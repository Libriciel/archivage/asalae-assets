# Asalae-assets

Asalae-assets permet d'importer facilement et efficacement les assets versionnés d'as@lae.
Le fichier binaire inclue permet de créer des liens symboliques vers le fichier versionné.
Si celui-ci est modifié, vous pourrez envoyer les modifications par git.

## Installation

Très simplement avec composer :

Commencez par éditer composer.json, afin d'ajouter l'exécution du script automatiquement lors d'un `composer update`
```json
{
  "scripts": {
    "post-update-cmd": "vendor/bin/asalae-assets-installer webroot/"
  }
}
```

Vous pouvez biensur exécuter la commande manuellement (après avoir installé le paquet).

Ensuite il faut installer le paquet :

```bash
composer config repositories.libriciel/asalae-assets git https://gitlab.libriciel.fr/libriciel/pole-archivage/asalae/asalae-assets.git
composer require libriciel/asalae-assets
```

