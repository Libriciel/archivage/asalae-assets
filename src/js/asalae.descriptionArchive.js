/**
 * Script de chargement de la page
 */
$(document).ready(
    function () {

        $('fieldset > legend').each(
            function () {
                $(this).closest('fieldset').addClass('expanded');
                var icon = 'fa fa-minus-square-o';
                if ($(this).closest('fieldset').hasClass('folder')) {
                    icon = 'fa fa-folder-open';
                } else if ($(this).closest('fieldset').hasClass('Contains')) {
                    icon = 'fa fa-folder-open';
                } else if ($(this).closest('fieldset').hasClass('Document')) {
                    icon = 'fa fa-file-o';
                    collapse(this, 'plier');
                } else if ($(this).closest('fieldset').hasClass('Document_v10')) {
                    icon = 'fa fa-file-o';
                    collapse(this, 'plier');
                } else if ($(this).closest('fieldset').hasClass('dataobjectreference')) {
                    icon = 'fa fa-file-o';
                    collapse(this, 'plier');
                } else if ($(this).closest('fieldset').hasClass('filesBloc')) {
                    icon = 'fa fa-tags';
                } else if ($(this).closest('fieldset').hasClass('formatIdentification')) {
                    icon = 'fa fa-info';
                } else if ($(this).closest('fieldset').hasClass('signingInformation')) {
                    icon = 'fa fa-info';
                    collapse(this,'plier');
                } else if ($(this).closest('fieldset').hasClass('RelatedData')) {
                    icon = 'fa fa-paperclip';
                } else if ($(this).closest('fieldset').hasClass('tag')) {
                    icon = 'fa fa-tags';
                } else if ($(this).closest('fieldset').hasClass('Appraisal')) {
                    icon = 'fa fa-calendar';
                } else if ($(this).closest('fieldset').hasClass('rules')) {
                    icon = 'fa fa-gavel';
                    collapse(this, 'plier');
                } else if ($(this).closest('fieldset').hasClass('HoldRule')) {
                    icon = 'fa fa-gavel';
                    collapse(this, 'plier');
                } else if ($(this).closest('fieldset').hasClass('LogBook')) {
                    icon = 'fa fa-book';
                    collapse(this, 'plier');
                } else if ($(this).closest('fieldset').hasClass('Tag')) {
                    icon = 'fa fa-flag-o';
                    collapse(this);
                } else if ($(this).closest('fieldset').hasClass('rules2')) {
                    icon = 'fa fa-gavel';
                    collapse(this, 'plier');
                } else if ($(this).closest('fieldset').hasClass('coverage')) {
                    icon = 'fa fa-shield';
                    collapse(this, 'plier');
                } else if ($(this).closest('fieldset').hasClass('agents')) {
                    icon = 'fa fa-user';
                    collapse(this, 'plier');
                } else if ($(this).closest('fieldset').hasClass('generique')) {
                    icon = 'fa fa-angle-right';
                    collapse(this, 'plier');
                } else if ($(this).closest('fieldset').hasClass('Agent')) {
                    icon = 'fa fa-user';
                    collapse(this, 'plier');
                } else if ($(this).closest('fieldset').hasClass('places')) {
                    icon = 'fa fa-map-marker';
                    collapse(this, 'plier');
                } else if ($(this).closest('fieldset').hasClass('entete')) {
                    icon = 'fa fa-window-maximize';
                } else if ($(this).closest('fieldset').hasClass('event')) {
                    icon = 'fa fa-bullhorn';
                    collapse(this, 'plier');
                } else if ($(this).closest('fieldset').hasClass('signature')) {
                    icon = 'fa fa-pencil-square-o';
                    collapse(this, 'plier');
                } else if ($(this).closest('fieldset').hasClass('gps')) {
                    icon = 'fa fa-map-marker';
                    collapse(this, 'plier');
                } else if ($(this).closest('fieldset').hasClass('Keyword')) {
                    icon = 'fa fa-flag-o';
                    collapse(this, 'plier');
                } else if ($(this).closest('fieldset').hasClass('Keyword_v10')) {
                    icon = 'fa fa-flag-o';
                    collapse(this, 'plier');
                } else if ($(this).closest('fieldset').hasClass('content')) {
                    icon = 'fa fa-ellipsis-h';
                } else if ($(this).closest('fieldset').hasClass('codeListClass')) {
                    icon = 'fa fa-info';
                    collapse(this, 'plier');
                } else if ($(this).closest('fieldset').hasClass('relatedObjectReference')) {
                    icon = 'fa fa-info';
                    collapse(this, 'plier');
                } else if ($(this).closest('fieldset').hasClass('child_relatedObjectReference')) {
                    icon = 'fa fa-cube';
                    collapse(this, 'plier');
                } else if ($(this).closest('fieldset').hasClass('childArchiveUnit')) {
                    icon = 'fa fa-folder';
                    collapse(this, 'plier');
                } else if ($(this).closest('fieldset').hasClass('deleted_childArchiveUnit')) {
                    icon = 'fa fa-folder';
                    collapse(this, 'plier');
                } else if ($(this).closest('fieldset').hasClass('deleted_childArchiveUnit1')) {
                    icon = 'fa fa-folder';
                } else if ($(this).closest('fieldset').hasClass('parentArchiveUnit')) {
                    icon = 'fa fa-folder-open';
                } else if ($(this).closest('fieldset').hasClass('deleted_parentArchiveUnit')) {
                    icon = 'fa fa-folder-open';
                } else if ($(this).closest('fieldset').hasClass('gestionClass')) {
                    icon = 'fa fa-gear';
                    collapse(this);
                } else if ($(this).closest('fieldset').hasClass('managementMetadata')) {
                    icon = 'fa fa-gear';
                    collapse(this);
                } else if ($(this).closest('fieldset').hasClass('metadonnees_descriptives')) {
                    icon = 'fa fa-list-ul';
                } else if ($(this).closest('fieldset').hasClass('meta_descriptive_10')) {
                    icon = 'fa fa-list-ul';
                } else if ($(this).closest('fieldset').hasClass('managementMetadata_Rules')) {
                    icon = 'fa fa-list-ul';
                    collapse(this);
                } else if ($(this).closest('fieldset').hasClass('descriptionClass')) {
                    icon = 'fa fa-list-ul';
                    collapse(this);
                } else if ($(this).parent('fieldset').hasClass('ContentDescriptive')) {
                    icon = 'fa fa-flag';
                } else if ($(this).parent('fieldset').hasClass('Organisation')) {
                    icon = 'fa fa-sitemap';
                    if (!$(this).parents('div').hasClass('acteur')) {
                        collapse(this, 'plier');
                    }
                } else if ($(this).closest('fieldset').hasClass('Address')) {
                    icon = 'fa fa-map-marker';
                    collapse(this, 'plier');
                } else if ($(this).closest('fieldset').hasClass('Contact')) {
                    icon = 'fa fa-envelope-o';
                    collapse(this, 'plier');
                } else if ($(this).closest('fieldset').hasClass('ContentDescription')) {
                    collapse(this, 'deplier');
                    icon = 'fa fa-info';
                } else if ($(this).closest('fieldset').hasClass('Communication')) {
                    icon = 'fa fa-bullhorn';
                    collapse(this, 'plier');
                } else if ($(this).closest('fieldset').hasClass('RelatedObjectReference')) {
                    icon = 'fa fa-link';
                } else if ($(this).closest('fieldset').hasClass('AccessRestriction')) {
                    icon = 'fa fa-unlock';
                } else if ($(this).closest('fieldset').hasClass('Integrity')) {
                    icon = 'fa fa-certificate';
                    $(this).attr('title', 'cliquer pour replier');
                    if ($(this).next().text() === '') {
                        $(this).next().hide();
                    }
                }
                $(this).prepend($('<i class="' + icon + ' icone-plier"></i>'));
            }
        ).on(
            'click',
            function () {
                collapse(this);
            }
        );

    //Lien footer (SEDA)
        $('#footer').find('a').on(
            'click',
            function () {
                window.open(this.href);
                return false;
            }
        );
    }
);