/**
 * Script de chargement de la page
 */
$(document).ready(
    function () {
    // noinspection JSCheckFunctionSignatures
        $('#arbonavigation')
            .bind(
                "loaded.jstree",
                function (event, data) {
                    data.instance.open_all();
                }
            )
            .bind(
                "select_node.jstree",
                function (e, data) {
                    $('#jstree').jstree('save_state');
                }
            )
            .on(
                "activate_node.jstree",
                function (e, data) {
                    var target = data.node.a_attr.href;
                    $('html, body').animate(
                        {
                            scrollTop: $(target).offset().top
                        },
                        600
                    );
                }
            )
            .on(
                'open_node.jstree',
                function (e, data) {
                //$('#' + data.node.id).find('i.jstree-icon.jstree-themeicon').chidren().addClass('fa-folder-o');
                    $('a i.jstree-icon.jstree-themeicon')
                        .addClass('fa').addClass('fa-folder-o').addClass('fa-folder-open');
                    $('#' + data.node.id).find('i.jstree-icon.jstree-themeicon').first()
                        //.removeClass('fa-folder-o')
                        .removeClass('fa-folder')
                        .addClass('fa-folder-open');
                }
            )
            .on(
                'close_node.jstree',
                function (e, data) {
                    $('#' + data.node.id).find('i.jstree-icon.jstree-themeicon').first()
                        .removeClass('fa-folder-open')
                        .addClass('fa-folder');

                }
            )
            .jstree();

        // draggable navigation
        $("#navigation")
            .draggable(
                {
                    cursor: "grabbing",
                    handle: "#headnavigation",
                    containment: "parent"
                }
            )
            .resizable(
                {
                    minHeight: 25,
                    minWidth: 40,
                    maxHeight: 600,
                    handles: 'w, sw, se, e, s',
                    containment: "parent"
                }
            );
        // FIXME effet de bord sur le legacy
        // $("#navigation")
        //     .resizable(
        //         {
        //             minWidth: 50,
        //             handles: 'e',
        //             containment: "parent",
        //             resize: function () {
        //                 adjustMainWidth();
        //             }
        //         });


        // $(window).on("resize", adjustMainWidth);
        // $(document).ready(adjustMainWidth);

        $('.backtotop').on(
            'click',
            function () {
                $('html, body').animate(
                    {
                        scrollTop: $('#main').offset().top
                    },
                    200
                );

            }
        );
    }
);

/**
 * plier/déplier le menu
 */
function togglenav() {
    const navigation = $('#navigation');
    const headnavigation = $('#headnavigation');
    const main = $('#main');
    const windowWidth = $(window).width();
    console.log('Largeur navigation :', navigation.outerWidth());

    if (navigation.outerWidth() > 60) {
        console.log('Réduction de la navigation');
        navigation.css('width', '50px');
        headnavigation.find('h4').hide();
        main.css(
            {
                'margin-left': '60px',
                'width': (windowWidth - 60) + 'px'
            }
        );
    } else {
        console.log('Augmentation de la navigation');
        navigation.css('width', '340px');
        headnavigation.find('h4').show();
        main.css(
            {
                'margin-left': '360px',
                'width': (windowWidth - 360) + 'px'
            }
        );

    }

}

function toggleMetaDescri () {
    const reductible_metaDescri = $('#reductible_metaDescri');
    var $fieldset = $('#reductible_metaDescri').closest('fieldset');

    if ($fieldset.hasClass('expanded')) {
        reductible_metaDescri.slideUp(500);
    } else {
        reductible_metaDescri.slideDown(500);
    }
}

function adjustMainWidth() {
    const navWidth = $("#navigation").outerWidth(); // Récupère la largeur de navigation
    const windowWidth = $(window).width(); // Récupère la largeur de la fenêtre
    const headnavigation = $('#headnavigation');
    main.style.marginLeft = (navWidth + 10)+ "px";
    main.style.width = (windowWidth - navWidth + 10) + "px";
    if (navWidth < 130) {
        headnavigation.find('h4').hide(); // Cache le titre h4
    } else {
        headnavigation.find('h4').show(); // Affiche le titre h4
    }
}
