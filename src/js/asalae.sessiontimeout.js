/* globals PHP, displayModalSessionTimeout */

/**
 * N'est pas consue pour faire partie de asalae-assets car dépendant de la
 * variable PHP
 */

var AsalaeSessionTimeout = {
    session_gc_maxlifetime: PHP.ini.session_gc_maxlifetime,
    savedOnbeforeunload: undefined,
    session_timeout: undefined,
    checkingConnection: false,
    iamConnected: true,
    chronoConnection: undefined,

    /**
     * Prépare la vérification de la connexion dans "duration" milisecondes
     * @param {number} duration
     */
    checkSessionTimeout: function (duration) {
        AsalaeSessionTimeout.session_timeout = setTimeout(
            function () {
                AsalaeSessionTimeout.checkingConnection = true; // Pour ignorer l'evenement "ajax_complete"
                $.ajax(
                    {
                        'url': PHP.urls.checkConnection,
                        'success': function (session) {
                            if (!session.connected || session.remaining <= 0) {
                                AsalaeSessionTimeout.iamConnected = false;
                                clearTimeout(AsalaeSessionTimeout.session_timeout);
                                clearTimeout(AsalaeSessionTimeout.chronoConnection);
                                $('html').trigger('session_timeout');
                            } else if (session.remaining > 0) {
                                clearTimeout(AsalaeSessionTimeout.session_timeout);
                                AsalaeSessionTimeout.checkSessionTimeout((session.remaining) * 1000 + 1000);
                                AsalaeSessionTimeout.launchChronoConnection((session.remaining) * 1000);
                            }
                        }
                    }
                );
            },
            duration
        );
    },

    debug: function () {
        $.ajax(
            {
                'url': PHP.urls.checkConnection,
                'success': function (session) {
                    console.log(session);
                }
            }
        );
    },

    /**
     * Callback appelé lors d'un appel ajax réussi de la modal de session timeout
     *
     * @param {object} content
     * @param {string} textStatus
     * @param {object} jqXHR
     */
    displayModalSessionTimeoutCallback: function (content, textStatus, jqXHR) {
        AsalaeSessionTimeout.checkingConnection = true; // Pour ignorer l'evenement "ajax_complete"
        if (jqXHR.getResponseHeader('X-Asalae-Success') === 'true') {
            window.onbeforeunload = AsalaeSessionTimeout.savedOnbeforeunload;
            clearTimeout(AsalaeSessionTimeout.session_timeout);
            AsalaeSessionTimeout.launchChronoConnection(AsalaeSessionTimeout.session_gc_maxlifetime * 1000);
            AsalaeSessionTimeout.checkSessionTimeout(AsalaeSessionTimeout.session_gc_maxlifetime * 1000 + 1000);
        }
    },

    /**
     * On lance le chrono de déconnexion
     * @param {number} duration
     */
    launchChronoConnection: function (duration) {
        clearTimeout(AsalaeSessionTimeout.chronoConnection);
        AsalaeSessionTimeout.chronoConnection = setTimeout(
            function () {
                AsalaeSessionTimeout.iamConnected = false;
            },
            duration
        );
    },

    /**
     * Permet d'initialiser le timer et les évenements
     */
    init: function () {
        // Debut timer
        AsalaeSessionTimeout.checkSessionTimeout(AsalaeSessionTimeout.session_gc_maxlifetime * 1000 + 1000);

        AsalaeSessionTimeout.launchChronoConnection(AsalaeSessionTimeout.session_gc_maxlifetime * 1000);

        // Si appel ajax, reset timer (si connecté)
        $('html').on(
            'ajax_complete',
            function () {
                if (!AsalaeSessionTimeout.checkingConnection && AsalaeSessionTimeout.iamConnected) {
                    clearTimeout(AsalaeSessionTimeout.session_timeout);
                    AsalaeSessionTimeout.checkSessionTimeout(AsalaeSessionTimeout.session_gc_maxlifetime * 1000 + 1000);
                }
                AsalaeSessionTimeout.checkingConnection = false;

            // Si timer dépassé, on affiche le formulaire de reconnection
            }
        ).on(
            'session_timeout',
            function () {
                    AsalaeSessionTimeout.savedOnbeforeunload = window.onbeforeunload;
                    AsalaeSessionTimeout.checkingConnection = true; // Pour ignorer l'evenement "ajax_complete"
                    displayModalSessionTimeout();
            }
        );
    }
};
AsalaeSessionTimeout.init();

$(
    function () {
        window.onbeforeunload = null;
    }
);
