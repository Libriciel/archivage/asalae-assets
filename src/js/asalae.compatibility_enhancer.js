if (typeof Number.isFinite === 'undefined') {
    // noinspection JSPrimitiveTypeWrapperUsage
    Number.isFinite = function (number) {
        return typeof number === 'number'
            && !isNaN(number)
            && number !== Number.infinity
            && number !== -Number.infinity
        ;
    }
}
if (typeof Math.log2 === 'undefined') {
    Math.log2 = function (x) {
        return Math.log(x) * Math.LOG2E;
    }
}
if (typeof (document.createElement('form')).reportValidity === 'undefined') {
    Element.prototype.reportValidity = function () {
        if (this.checkValidity && !this.checkValidity()) {
            var msg = "There is an error in the form";
            if (typeof __ === 'function') {
                msg = __(msg);
            }
            alert(msg);
        }
    };
}