/* globals $, Cookies */

/**
 * Permet de jongler entre les cookies mémorisés en bdd et ceux utilisé par le navigateur
 */
var AsalaeCookies = {
    cookies: {},
    defaultOpts: {path: '/', sameSite: 'Strict'}
};

/**
 * Initialise les cookies
 */
AsalaeCookies.init = function(cookies) {
    this.cookies = cookies;
};

/**
 * Getter d'un cookie
 * @param {string} name
 * @param {object} options
 * @return {string}
 */
AsalaeCookies.get = function (name, options = AsalaeCookies.defaultOpts) {
    var value = Cookies.get(name, options);
    if (value) {
        return value;
    }
    return this.cookies ? this.cookies[name] : null;
};

/**
 * Setter d'un cookie
 * @param {string} name
 * @param {string} value
 * @param {object} options
 */
AsalaeCookies.set = function (name, value, options = AsalaeCookies.defaultOpts) {
    Cookies.set(name, value, options);
    this.cookies[name] = ''+value;
}

/**
 * Détruit un cookie
 * @param {string} name
 * @param {object} options
 */
AsalaeCookies.unset = function (name, options = AsalaeCookies.defaultOpts) {
    Cookies.remove(name, options);
    if (typeof this.cookies[name] !== 'undefined') {
        delete this.cookies[name];
    }
}

/**
 * Vrais si un cookie existe
 * @param {string} name
 * @param {object} options
 * @returns {boolean}
 */
AsalaeCookies.exists = function (name, options = AsalaeCookies.defaultOpts) {
    var value = Cookies.get(name, options);
    return (typeof value !== 'undefined' && value !== '')
        || typeof this.cookies[name] !== 'undefined';
}
