/** global $, uuid, CRC32, moment */

// compatibilité ascendente avec le webpack encore (require)
if (typeof uuidv4 === 'undefined' && typeof uuid.v4 === 'function') {
    var uuidv4 = uuid.v4;
}
if (typeof crc32bstr === 'undefined' && typeof CRC32.bstr === 'function') {
    var crc32bstr = CRC32.bstr;
}


class ChunkyUploader {
    static MAX_ERRORS_ON_THE_SAME_CHUNK = 3;
    static MAX_SIMULTANEOUS_REQUESTS = 3;
    static CHUNK_SIZE = 2097152; // 2Mo
    static translations = {
        action: 'Action',
        confirm_delete: "Êtes-vous sûr de vouloir supprimer ce fichier ?",
        delete: "supprimer",
        dropbox_text: "Glissez-déposez vos fichiers ici",
        file: 'Fichier',
        pause: "pause",
        resume: "reprendre",
        retry: "relancer",
        select: "Sélectionner",
        select_all: "Sélectionner toutes les lignes",
        select_file: "Sélectionner ",
        size: ['octets', 'ko', 'Mo', 'Go', 'To', 'Po'],
        uploading_validation: "Un upload est en cours",
        progressBarCompleted: '<i class="fa fa-check" aria-hidden="true"></i> Terminé',
        progressBarInvalidated: '<i class="fa fa-times" aria-hidden="true"></i> Invalide'
    };
    static instances = [];

    constructor(input, sizeLimit = null) {
        this.input = input;
        this.url = input.attr('data-uploader');
        this.queue = [];
        this.sizeLimit = sizeLimit;
        this.uploading = false;
        setTimeout(
            () => {
                if (input.attr('data-table-generator') && window[input.attr('data-table-generator')]) {
                    this.tableGenerator = window[input.attr('data-table-generator')];
                } else {
                    this.tableGenerator = false;
                }
            },
            10
        );

        input.attr('data-chunky-id', ChunkyUploader.instances.length);
        ChunkyUploader.instances.push(this);
    }

    enqueue(chunkyFile)
    {
        this.queue.push(chunkyFile);
    }

    unqueue(chunkyFile)
    {
        for (let i = 0; i < this.queue.length; i++) {
            if (this.queue.uuid === chunkyFile.uuid) {
                this.queue.splice(i, 1);
                break;
            }
        }
    }

    init()
    {
        this.currentRequestCount = 0;
        this.stopped = false;
        this.interval = 0;
        this.currentFile = null;
        this.uploading = true;
    }

    run()
    {
        this.init();
        this.currentFile = this.queue.shift();
        this.input.trigger('upload-started.chunky');
        if (this.currentFile?.paused) {
            let pauseDuration = performance.now() - this.currentFile.pauseTime;
            this.currentFile.beginTime = this.currentFile.beginTime + pauseDuration;
            this.currentFile.progresses = this.currentFile.progresses.map((v) => v + pauseDuration);
        } else {
            this.currentFile.beginTime = performance.now();
        }

        this.interval = setInterval(() => this.updateProgresBar(), 500);
        this.sendNextChunk();
    }

    stop()
    {
        this.stopped = true;
        this.uploading = false;
        clearInterval(this.interval);
    }

    sendNextChunk()
    {
        let chunkyFile = this.currentFile;
        if (this.stopped) {
            return;
        }
        if (chunkyFile.succededChunks.length === chunkyFile.totalChunk) {
            this.stop();
            if (this.queue.length) {
                this.run();
            } else {
                this.uploading = false;
                this.input.trigger('upload-finished.chunky');
            }
            return;
        }
        let currentChunk = chunkyFile.remainingChunks.shift();
        if (currentChunk === undefined) {
            if (chunkyFile.failedChunks.length) {
                chunkyFile.remainingChunks = [...chunkyFile.failedChunks];
                currentChunk = chunkyFile.remainingChunks.shift();
            } else {
                return; // il reste des requetes en cours
            }
        }

        let chunkContent = chunkyFile.file.slice(
            currentChunk * ChunkyUploader.CHUNK_SIZE,
            (currentChunk + 1) * ChunkyUploader.CHUNK_SIZE
        );
        let reader = new FileReader();
        reader.onload = () => {
            let formData = new FormData();
            formData.append('filename', chunkyFile.file.name);
            formData.append('uuid', chunkyFile.uuid);
            formData.append('size', chunkyFile.file.size);
            formData.append('chunk[i]', currentChunk + 1);
            formData.append('chunk[total]', chunkyFile.totalChunk);
            formData.append('chunk[size]', reader.result.length);
            formData.append('chunk[hash]', (crc32bstr(reader.result)>>>0).toString(16));
            formData.append('file', chunkContent, 'chunk-'+currentChunk);
            let len = ('' + chunkyFile.totalChunk).length;

            this.currentRequestCount++;
            $.ajax(
                {
                    url: ("%s?f=%s&c=%0"+len+"d..%d").format(
                        this.url,
                        encodeURIComponent(chunkyFile.file.name),
                        currentChunk + 1,
                        chunkyFile.totalChunk
                    ),
                    method: 'POST',
                    data: formData,
                    cache: false,
                    processData: false,
                    contentType: false,
                    success: (content, textStatus, jqXHR) => {
                        if (content.indexOf('partial') !== -1 || content.indexOf('done') !== -1) {
                            this.chunkSuccess(currentChunk, jqXHR);
                        } else {
                            this.chunkError(currentChunk, jqXHR);
                        }
                    },
                    error: (jqXHR, textStatus, errorThrown) => {
                        this.chunkError(currentChunk, jqXHR);
                    },
                    complete: () => {
                        this.currentRequestCount--;
                        this.sendNextChunk(chunkyFile);
                    }
                }
            );
            if (this.currentRequestCount < ChunkyUploader.MAX_SIMULTANEOUS_REQUESTS) {
                this.sendNextChunk(chunkyFile);
            }
        };
        reader.readAsBinaryString(chunkContent);
    }

    chunkSuccess(currentChunk, jqXHR)
    {
        let chunkyFile = this.currentFile;
        chunkyFile.succededChunks.push(currentChunk);
        chunkyFile.progresses.push(performance.now());
        let percent = Math.ceil(chunkyFile.progresses.length / chunkyFile.totalChunk * 100);
        chunkyFile.tr.find('.progress-bar')
            .css('width', percent + '%')
            .attr('aria-valuenow', currentChunk)
            .text(percent + '%');

        let eventData = {
            uuid: chunkyFile.uuid,
            chunk: currentChunk,
            message: chunkyFile.tr.find('td.file-message')
        };
        this.input.trigger('chunk-success.chunky', eventData);
        if (percent === 100 && chunkyFile.invalidFile === false) {
            chunkyFile.tr.find('.progress-bar')
                .addClass('progress-bar-success')
                .html(ChunkyUploader.translations.progressBarCompleted);
            chunkyFile.tr.find('.uploading-message').remove();
            chunkyFile.tr.find('.action-pause').remove();
            chunkyFile.tr.find('.action-resume').remove();
            chunkyFile.tr.find('.action-retry').remove();

            let eventData = {
                uuid: chunkyFile.uuid,
                filename: chunkyFile.file.name,
                message: chunkyFile.tr.find('td.file-message'),
                tr: chunkyFile.tr,
                jqXHR: jqXHR
            };
            if (jqXHR.getResponseHeader('Chunky-Metadata')) {
                var urlDecoded = decodeURIComponent(jqXHR.getResponseHeader('Chunky-Metadata'));
                eventData.metadata = JSON.parse(urlDecoded);
            }

            if (this.tableGenerator) {
                eventData.tableGenerator = this.tableGenerator;
            }

            this.input.trigger('file-success.chunky', eventData);
        }
    }

    chunkError(currentChunk, jqXHR)
    {
        let chunkyFile = this.currentFile;
        chunkyFile.failedChunks.push(currentChunk);
        let chunkErrorCount = chunkyFile.failedChunks.filter((v) => v === currentChunk).length;
        if (chunkErrorCount >= ChunkyUploader.MAX_ERRORS_ON_THE_SAME_CHUNK) {
            this.stop();
            chunkyFile.tr.find('.progress-bar').addClass('progress-bar-danger');
        }
        // erreur de validation
        if (jqXHR.getResponseHeader('Chunky-Error')) {
            chunkyFile.invalidFile = true;
            chunkyFile.invalidMessage = decodeURIComponent(jqXHR.getResponseHeader('Chunky-Error'));
            let errorMessage = $('<p class="text-danger">').text(
                chunkyFile.invalidMessage
            );
            chunkyFile.tr.find('.uploading-message').empty().css('opacity', 1)
                .append(errorMessage);
            this.stop();
            chunkyFile.succededChunks.push(chunkyFile.failedChunks.pop());
            let progressBar = chunkyFile.tr.find('.progress-bar');
            progressBar.addClass('progress-bar-danger');
            progressBar.css('width', '100%');
            progressBar.attr('aria-valuenow', chunkyFile.totalChunk);
            progressBar.html(ChunkyUploader.translations.progressBarInvalidated);

            chunkyFile.tr.find('.action-retry').show();
            chunkyFile.tr.find('.action-pause').hide();
        }

        let eventData = {
            uuid: chunkyFile.uuid,
            filename: chunkyFile.file.name,
            chunk: currentChunk,
            message: chunkyFile.tr.find('td.file-message'),
            jqXHR: jqXHR
        };
        this.input.trigger('chunk-error.chunky', eventData);
    }

    updateProgresBar()
    {
        let chunkyFile = this.currentFile;
        if (chunkyFile.progresses.length === 0 || chunkyFile.invalidFile) {
            return;
        }

        let newTime = performance.now();
        let difftime = newTime - chunkyFile.beginTime;
        let seconds = difftime / 1000;
        let filteredProgresses = chunkyFile.progresses.filter((v) => v + 10000 > newTime);
        let secs = filteredProgresses.length ? (newTime - filteredProgresses[0]) / 1000 : 0;
        var rate = filteredProgresses.length / secs * ChunkyUploader.CHUNK_SIZE;
        var duration = moment().startOf('day').seconds(seconds).format('HH:mm:ss');
        let transferred = chunkyFile.succededChunks.length * ChunkyUploader.CHUNK_SIZE;
        let toUpload = chunkyFile.file.size - transferred;
        var estimation = toUpload / (transferred / seconds);
        var date = moment().startOf('day').seconds(Math.round(estimation));
        var s = ChunkyUploader.translations.size;
        var e = Math.floor(Math.log(rate) / Math.log(1024));
        var speed = Number.isFinite(e)
            ? (rate / Math.pow(1024, e)).toFixed(2) + " " + s[e] + "/s"
            : '0/s';
        var estimated;
        if (toUpload >= 0) {
            if (date.hour()) {
                estimated = date.hour()+'h '+date.minute()+'m';
            } else if (date.minute()) {
                estimated = date.minute()+'m '+date.second()+'s';
            } else if (date.second()) {
                estimated = date.second()+'s';
            } else {
                estimated = '';
            }
        }

        let statusMessage = chunkyFile.tr.find('.uploading-message').css('opacity', 1);
        statusMessage.find('.duration').text(duration);
        statusMessage.find('.speed').text(speed);
        if (estimated) {
            statusMessage.find('.estimated').text(estimated);
        } else {
            statusMessage.css('opacity', 0);
        }

        let eventData = {
            uuid: chunkyFile.uuid,
            filename: chunkyFile.file.name,
            duration: duration,
            speed: speed,
            estimated: estimated,
            transferred: transferred,
            remaining: toUpload,
        };
        this.input.trigger('update-progress.chunky', eventData);
    }

    generateDropbox(input)
    {
        var dropbox = $('<div class="dropbox dropbox-small">').attr('id', input.attr('id') + '-dropbox');
        dropbox.append($('<i class="fa fa-upload fa-3x text-primary">'));
        dropbox.append($('<p>').text(ChunkyUploader.translations.dropbox_text));
        var button = $('<button type="button" class="btn btn-success">');
        button.append($('<i class="fa fa-folder-open fa-space" aria-hidden="true">'));
        button.append('Parcourir...');
        button.on('click.chunky', () => input.trigger('click.chunky'));
        dropbox.append(button);
        input.after(dropbox);
        input.css({position: 'absolute', height: 0}).prop('tabindex', -1);

        dropbox
            .on('dragover.chunky', (event) => {
                event.preventDefault();
                dropbox.addClass('dragover')
            })
            .on('dragleave.chunky', () => dropbox.removeClass('dragover'))
            .on('drop.chunky', (event) => {
                event.preventDefault();
                event.stopPropagation();
                dropbox.removeClass('dragover');
                input.get(0).files = event.originalEvent.dataTransfer.files;
                input.trigger('change.chunky');
            })
        ;
        return dropbox;
    }

    generateTableUploads(multiple)
    {
        var table = $('<table class="table table-striped table-hover hide">')
        var tr = $('<tr>');
        var selector;
        if (multiple) {
            selector = $('<th class="table-checkbox">');
            let label = $('<label>');
            let checkAll = $('<input type="checkbox" class="checkall">');
            checkAll.on('change.chunky', () => {
                var checked = checkAll.prop('checked');
                table.find('.selection-checkbox').prop('checked', checked);
                this.input.trigger('check-file.chunky');
            });
            label.append($('<span class="sr-only">').text(ChunkyUploader.translations.select_all));
            label.append(checkAll);
            selector.append(label);
        } else {
            selector = $('<th>').text(ChunkyUploader.translations.select);
        }
        tr.append(selector);
        tr.append($('<th>').text(ChunkyUploader.translations.file));
        tr.append($('<th>').text(ChunkyUploader.translations.action));
        var thead = $('<thead>').append(tr);
        table.append(thead);
        var tbody = $('<tbody>');
        table.append(tbody);
        return table;
    }

    handleCheckEvent(input, table)
    {
        return () => {
            if (table.find('tbody input:checked').length === 0) {
                input.prop('required', input.get(0).hasAttribute('data-required'));
            } else {
                input.prop('required', false);
            }
            if (table.find('tbody tr').length === 0) {
                table.addClass('hide');
                input.trigger('upload-finished.chunky');
            }
        };
    }

    handleInputChange(input, table)
    {
        return () => {
            var files = input.get(0).files;
            if (!files.length) {
                return;
            }
            if (table.hasClass('hide')) {
                table.removeClass('hide');
            }
            input.val('');
            if (input.prop('required')) {
                input.prop('required', false);
                input.attr('data-required', '1');
            }
            for (let i = 0; i < files.length; i++) {
                let chunkyFile = new ChunkyFile(this, files[i]);
                let tr;
                if (!this.sizeLimit || this.sizeLimit > files[i].size) {
                    this.enqueue(chunkyFile);
                    tr = chunkyFile.generateTr();
                } else {
                    tr = chunkyFile.generateExceededSizeTr();
                }
                table.find('tbody').prepend(tr);
                this.setAutoTrColspan(table, tr);
                this.selectTrValue(table, tr);
            }
            if (this.uploading === false) {
                this.run();
            }
        };
    }

    selectTrValue(table, tr)
    {
        var multiple = this.input.get(0).hasAttribute('multiple');
        var input = tr.find('td.table-checkbox input');
        input.prop('checked', true);
        if (!multiple) {
            table.find('input[name="' + input.attr('name') + '"]')
                .not('[value="' + input.attr('value') + '"]')
                .prop('checked', false);
        }
    }

    setAutoTrColspan(table, tr)
    {
        let colspan = table.find('thead th:visible').length - 2;
        tr.find('td').eq(1).attr('colspan', colspan);
    }

    createTableFromPreviousValues(input, table, previousValue)
    {
        if (input.prop('required')) {
            input.prop('required', false);
        }
        if (!Array.isArray(previousValue)) {
            previousValue = [previousValue];
        }
        for (let i = 0; i < previousValue.length; i++) {
            let chunkyFile = new ChunkyFile(this, previousValue[i]);
            chunkyFile.uuid = previousValue[i].uuid ? previousValue[i].uuid : previousValue[i].id;
            let tr = chunkyFile.generateTr(false);
            tr.find('input.selection-checkbox').prop('checked', true);
            table.find('tbody').append(tr);
            table.removeClass('hide');
            this.setAutoTrColspan(table, tr);
            let progressBar = chunkyFile.tr.find('.progress-bar');
            progressBar.css('width', '100%');
            progressBar.attr('aria-valuenow', chunkyFile.totalChunk);
            progressBar.addClass('progress-bar-success');
            progressBar.html(ChunkyUploader.translations.progressBarCompleted);
            chunkyFile.tr.find('.uploading-message').remove();
            chunkyFile.tr.find('.action-pause').remove();
            chunkyFile.tr.find('.action-resume').remove();
            chunkyFile.tr.find('.action-retry').remove();

            setTimeout(
                () => {
                    let eventData = {
                        uuid: chunkyFile.uuid,
                        filename: chunkyFile.file.name,
                        message: chunkyFile.tr.find('td.file-message'),
                        tr: chunkyFile.tr
                    };
                    if (this.tableGenerator) {
                        eventData.tableGenerator = this.tableGenerator;
                    }
                    this.input.trigger('loaded-file.chunky', eventData);
                },
                10
            );
        }
        this.input.trigger('loaded.chunky');
    }

    wrapInput()
    {
        var uploader = this;
        var input = this.input;
        input.closest('.custom-file').find('.custom-file-label').remove();
        input.removeClass('custom-file-input');
        input.data('uploader', this);

        var dropbox = this.generateDropbox(input);
        var multiple = input.get(0).hasAttribute('multiple');
        var table = this.generateTableUploads(multiple);
        dropbox.after(table);
        if (input.prop('required')) {
            input.attr('data-required', '1');
            input.on('check-file.chunky', this.handleCheckEvent(input, table));
        }

        var s = ChunkyUploader.translations.size;
        var name = input.attr('name');
        input.on('change.chunky', this.handleInputChange(input, table));
        input.on('upload-started.chunky', () => {
            input.get(0).setCustomValidity(ChunkyUploader.translations.uploading_validation);
        });
        input.on('upload-finished.chunky', () => {
            input.get(0).setCustomValidity("");
        });

        let previousValue = JSON.parse(input.attr('data-value') || '""');
        if (previousValue) {
            this.createTableFromPreviousValues(input, table, previousValue);
        }
    }

    pauseAction(chunkyFile)
    {
        let uuid = chunkyFile.uuid;
        chunkyFile.paused = true;
        chunkyFile.pauseTime = performance.now();
        if (this.currentFile?.uuid === uuid) {
            this.stop();
            if (this.queue.length) {
                this.run();
            }
        } else {
            this.unqueue(chunkyFile);
        }
        chunkyFile.tr.find('.action-pause').hide();
        chunkyFile.tr.find('.action-resume').show();
    }

    resumeAction(chunkyFile)
    {
        this.queue.unshift(chunkyFile);
        if (this.stopped) {
            this.run();
        }
        chunkyFile.tr.find('.action-pause').show();
        chunkyFile.tr.find('.action-resume').hide();
    }

    retryAction(chunkyFile)
    {
        this.queue.push(chunkyFile);
        chunkyFile.invalidFile = false;
        chunkyFile.succededChunks = chunkyFile.succededChunks.filter(value => value !== 0);
        chunkyFile.remainingChunks.push(0);
        chunkyFile.failedChunks = [];
        if (this.stopped) {
            this.run();
        }
        chunkyFile.tr.find('.action-retry').hide();
        chunkyFile.tr.find('.action-pause').show();
        chunkyFile.tr.find('.action-resume').hide();
    }

    deleteAction(chunkyFile)
    {
        if (!confirm(ChunkyUploader.translations.confirm_delete)) {
            return;
        }
        chunkyFile.tr.find('button').removeAttr('data-toggle').tooltip('hide').tooltip('disable').disable();
        this.pauseAction(chunkyFile);
        this.unqueue(chunkyFile);
        if (!this.queue.length) {
            this.uploading = false;
            this.input.trigger('upload-finished.chunky');
        }
        let url = chunkyFile.file.deleteUrl
            ? chunkyFile.file.deleteUrl
            : ("%s?f=%s").format(
                this.url,
                encodeURIComponent(chunkyFile.file.name)
            );
        $.ajax(
            {
                url: url,
                method: 'DELETE',
                data: {uuid: chunkyFile.uuid},
                success: (content) => {
                    if (content.indexOf('deleted') === -1 ) {
                        return;
                    }
                    chunkyFile.tr.fadeOut(
                        400,
                        () => {
                            let tbody = chunkyFile.tr.closest('tbody');
                            chunkyFile.tr.remove();
                            this.input.trigger('check-file.chunky');
                        }
                    );
                }
            }
        );
    }
}

class ChunkyFile {
    constructor(uploader, file) {
        this.uploader = uploader;
        this.file = file;
        this.uuid = uuidv4();
        this.succededChunks = [];
        this.remainingChunks = [];
        this.failedChunks = [];
        this.beginTime = null;
        this.pauseTime = null;
        this.progresses = [];
        this.paused = false;
        this.invalidFile = false;
        this.invalidMessage = '';
        this.totalChunk = Math.ceil(file.size / ChunkyUploader.CHUNK_SIZE);
        for (let i = 0; i < this.totalChunk; i++) {
            this.remainingChunks.push(i);
        }
    }

    generateFileTd()
    {
        let fileTd = $('<td class="file-message">');
        var s = ChunkyUploader.translations.size;
        let e = Math.floor(Math.log(this.file.size) / Math.log(1024));
        let size = Number.isFinite(e)
            ? (this.file.size / Math.pow(1024, e)).toFixed(2) + " " + s[e]
            : '0'
        let chunkCount = Math.ceil(this.file.size / ChunkyUploader.CHUNK_SIZE);
        let progressBar = $('<div class="progress-bar" role="progressbar">')
            .attr('aria-valuenow', 0)
            .attr('aria-valuemin', 0)
            .attr('aria-valuemax', chunkCount)
            .css('width', 0)
        ;
        let progress = $('<div class="progress">').append(progressBar);
        fileTd.append(progress);
        fileTd.append(
            $('<div class="uploader-filername-container">')
                .append($('<span class="uploader-filername">').text(this.file.name))
                .append($('<span class="uploader-size float-right">').text(size))
        );
        let statusMessage = $('<div class="uploading-message">')
            .append($('<span class="duration">'))
            .append($('<span class="speed">'))
            .append($('<span class="estimated">'))
            .css('opacity', 0)
        ;
        fileTd.append(statusMessage);
        fileTd.css('width', '100%');
        return fileTd;
    }

    generateTr(newFile = true)
    {
        let name = this.uploader.input.attr('name');
        var multiple = this.uploader.input.get(0).hasAttribute('multiple');
        var required = this.uploader.input.get(0).hasAttribute('data-required');
        let tr = $('<tr>').attr('data-id', this.uuid);

        let selectionTd = $('<td class="table-checkbox">');
        let label = $('<label>');
        label.append($('<span class="sr-only">').text(ChunkyUploader.translations.select_file + this.file.name));
        if (multiple) {
            let checkbox = $('<input type="checkbox" class="selection-checkbox">')
                .attr('value' , this.uuid)
                .attr('name', name + '[]')
                .on('change.chunky', () => this.uploader.input.trigger('check-file.chunky'));
            label.append(checkbox);
        } else {
            label.append(
                $('<input type="radio" class="selection-radio">')
                    .attr('value' , this.uuid)
                    .attr('name', name)
                    .on('change.chunky', () => this.uploader.input.trigger('check-file.chunky'))
            );
        }
        selectionTd.append(label);
        tr.append(selectionTd);

        let fileTd = this.generateFileTd();
        tr.append(fileTd);

        let actionTd = $('<td class="action">');
        let retryBtn = $('<button type="button" class="btn btn-link action-retry" data-toggle="tooltip">')
            .attr('title', "Retenter la finalization du fichier")
            .append($('<i class="fa fa-redo">'))
            .append($('<span class="sr-only">').text(ChunkyUploader.translations.retry))
            .on('click.chunky', () => this.uploader.retryAction(this))
            .hide()
        ;
        actionTd.append(retryBtn);
        let pauseBtn = $('<button type="button" class="btn btn-link action-pause" data-toggle="tooltip">')
            .attr('title', "Mettre le transfert de fichier en pause")
            .append($('<i class="fa fa-pause">'))
            .append($('<span class="sr-only">').text(ChunkyUploader.translations.pause))
            .on('click.chunky', () => this.uploader.pauseAction(this))
        ;
        actionTd.append(pauseBtn);
        let resumeBtn = $('<button type="button" class="btn btn-link action-resume" data-toggle="tooltip">')
            .hide()
            .attr('title', "Continuer le transfert de fichier")
            .append($('<i class="fa fa-play">'))
            .append($('<span class="sr-only">').text(ChunkyUploader.translations.resume))
            .on('click.chunky', () => this.uploader.resumeAction(this))
            .hide()
        ;
        actionTd.append(resumeBtn);
        let deleteBtn = $('<button type="button" class="btn btn-link action-delete" data-toggle="tooltip">')
            .attr('title', "Supprimer le transfert de fichier")
            .append($('<i class="fa fa-trash text-danger">'))
            .append($('<span class="sr-only">').text(ChunkyUploader.translations.delete))
            .on('click.chunky', () => this.uploader.deleteAction(this))
        ;
        actionTd.append(deleteBtn);
        tr.append(actionTd);
        this.tr = tr;

        if (newFile) {
            let eventData = {
                uuid: this.uuid,
                filename: this.file.name,
                size: this.file.size,
                message: fileTd
            };
            this.uploader.input.trigger('new-file.chunky', eventData);
        }

        return tr;
    }

    generateExceededSizeTr()
    {
        let name = this.uploader.input.attr('name');
        var multiple = this.uploader.input.get(0).hasAttribute('multiple');
        var required = this.uploader.input.get(0).hasAttribute('data-required');
        let tr = $('<tr>').attr('data-id', this.uuid);

        let selectionTd = $('<td class="table-checkbox">');
        tr.append(selectionTd);

        let fileTd = this.generateFileTd();
        fileTd.find('.uploading-message').empty().css('opacity', 1).text(
            "Ce fichier dépasse la taille limite autorisée"
        );
        tr.append(fileTd);

        let actionTd = $('<td class="action">');
        let deleteBtn = $('<button type="button" class="btn btn-link action-delete" data-toggle="tooltip">')
            .attr('title', "Supprimer le transfert de fichier")
            .append($('<i class="fa fa-times-circle-o">'))
            .append($('<span class="sr-only">').text(ChunkyUploader.translations.delete))
        ;
        deleteBtn.on('click.chunky', () => tr.fadeOut(
            400,
            () => {
                deleteBtn.tooltip('dispose');
                let tbody = tr.closest('tbody');
                tr.remove();
                if (tbody.find('tr input:checked').length === 0) {
                    this.uploader.input.prop(
                        'required',
                        this.uploader.input.get(0).hasAttribute('data-required')
                    );
                }
                if (tbody.find('tr').length === 0) {
                    tr.closest('table').addClass('hide');
                    this.uploader.input.trigger('upload-finished.chunky');
                }
            }
        ));
        actionTd.append(deleteBtn);
        tr.append(actionTd);
        this.tr = tr;
        return tr;
    }
}

$(document).on('modal-loaded', function (event, modal) {
    modal.find('[data-uploader]').each(function() {
        var sizeLimit = $(this).attr('data-uploader-filesize-limit');
        var uploader = new ChunkyUploader($(this), sizeLimit);
        uploader.wrapInput();
    });
});
