/**
 * globals $, AsalaeGlobal, AsalaePopup
 */

/**
 * Script du moteur de recherche
 */
class AsalaeSearchEngine
{
    static instance;
    static lastInstance;

    /**
     * Constructeur
     * @param {string|jquery} searchbar
     * @param {string} current_url
     * @param {string} search_url
     * @param {object} table - liste des champs du popup sous forme: {"Nom": "name"}
     * @param {int} delay
     */
    constructor(searchbar, current_url, search_url, table, delay = 250) {
        searchbar = $(searchbar);
        this.results = null;
        this.hovering = null;
        this.searchbar = searchbar;
        this.current_url = current_url;
        this.search_url = search_url;
        this.table = table;
        this.delay = delay;
        this.basedelay = delay;
        var form = searchbar.closest('form');
        this.form = form;
        this.searchbarDiv = searchbar.closest('.form-group');
        this.ajaxRunning = false;
        this.prevval = '';
        this.ajaxSearch = null;
        this.searchbar.on('keyup change click', this.keyupHandler(this));
        var searchbutton = this.searchbarDiv.find('.input-group-addon.btn');
        searchbutton.on('click', function () {
            form.submit();
        });
        form.on('submit', function(e) {
            e.preventDefault();
            var val = encodeURI(searchbar.val());
            var url = current_url;
            if (val) {
                url += '?search='+val;
            }
            AsalaeGlobal.interceptedLinkToAjax(url);
            return false;
        });
        // Laisse le temps à la page de définir la taille de l'input
        setTimeout(() => this.initPopup(), 100);

        if (!AsalaeSearchEngine.instance) {
            AsalaeSearchEngine.instance = [];
        }
        let i = 0;
        do {
            this.instanceIndex = i;
            i++;
        } while (typeof AsalaeSearchEngine.instance[this.instanceIndex] !== 'undefined');
        AsalaeSearchEngine.instance[this.instanceIndex] = this;
        AsalaeSearchEngine.lastInstance = this;
        searchbar.attr('data-search-engine-uid', this.instanceIndex);
    }

    /**
     * Lorsque on modifi le contenu du champ de recherche
     * @param {object} searchEngine
     * @returns {function(): void}
     */
    keyupHandler(searchEngine)
    {
        return function () {
            var val = $(searchEngine.searchbar).val().trim();
            var delay = searchEngine.delay;
            if (searchEngine.prevval && val.length === 0) {
                searchEngine.searchbarDiv.find('.search-bar-results').remove();
                searchEngine.prevval = '';
            }
            if (val.length >= 3 && val !== searchEngine.prevval) {
                if (searchEngine.ajaxRunning) {
                    clearTimeout(searchEngine.ajaxRunning);
                    searchEngine.delay += searchEngine.basedelay;
                } else {
                    searchEngine.delay = searchEngine.basedelay;
                }
                if (searchEngine.ajaxSearch) {
                    if (searchEngine.ajaxSearch.readyState < 4) {
                        searchEngine.ajaxSearch.abort();
                    }
                    searchEngine.ajaxSearch = null;
                }
                searchEngine.ajaxRunning = setTimeout(function() {
                    searchEngine.prevval = val;
                    searchEngine.searchbarDiv.addClass('loading');
                    searchEngine.ajaxSearch = searchEngine.ajax();
                }, delay);
            }
        }
    }

    ajax()
    {
        return $.ajax({
            url: this.search_url,
            method: 'POST',
            headers: {
                Accept: 'application/json'
            },
            data: this.form.serialize(),
            success: this.ajaxSuccess(this),
            error: AsalaeGlobal.ajaxError,
            complete: () => {
                this.ajaxRunning = false;
                this.ajaxSearch = null;
                this.searchbarDiv.removeClass('loading');
            }
        });
    }

    ajaxSuccess(searchEngine)
    {
        return function (content) {
            searchEngine.searchbarDiv.find('.search-bar-results').remove();
            $(document).off('.hide-search').on('click.hide-search keyup.hide-search', function(e) {
                if ($(e.target).closest('.form-group').length === 0) {
                    searchEngine.searchbarDiv.find('.search-bar-results').remove();
                    $(document).off('click.hide-search');
                    searchEngine.prevval = '';
                }
            });
            if (content.length === 0) {
                searchEngine.searchbarDiv.append(
                    $('<div class="search-bar-results nav navbar"></div>').text(__("Aucun résultat"))
                );
                return;
            }
            var ul = $('<ul class="search-bar-results nav navbar"></ul>');
            for (var i = 0; i < content.length; i++) {
                var li = $('<li></li>');
                var table = $('<table>');
                var tbody = $('<tbody>');
                var first = true;
                for (var key in content[i].scores) {
                    var tr = $('<tr tabindex="0" role="button">')
                        .attr('data-id', content[i].id)
                        .attr('data-index', i);
                    if (first) {
                        first = false;
                        tr.append(
                            $('<th>').attr('rowspan', Object.keys(content[i].scores).length)
                                .text(content[i].result)
                        );
                    }
                    tr.append($('<th>').text(content[i].scores[key].field));
                    tr.append($('<td>').html(content[i].scores[key].str));
                    tr.on(
                        'click',
                        function() {
                            var id = $(this).attr('data-id');
                            AsalaeGlobal.interceptedLinkToAjax(
                                searchEngine.current_url+'?id='
                                +id+'&search='+encodeURI(searchEngine.searchbar.val())
                            );
                        }
                    );
                    tbody.append(tr);
                }
                li.on('mouseover, mousemove', searchEngine.hoverHandler(content[i].entity));
                li.on(
                    'mouseleave',
                    (function(li) {
                        return function (e) {
                            searchEngine.popup.form.remove();
                            searchEngine.popup.hide();
                        }
                    })(li.get(0))
                );
                table.append(tbody);
                li.append(table);
                ul.append(li);
            }
            searchEngine.searchbarDiv.append(ul);
        }
    }

    initPopup()
    {
        this.popup = new AsalaePopup();
        var table = $('<table class="table no-padding"></table>');
        var tbody = $('<tbody></tbody>');
        table.append(tbody);
        for (var key in this.table) {
            let tr = $('<tr></tr>');
            let th = $('<th></th>').text(key);
            let td = $('<td></td>').attr('data-path', this.table[key]);
            tr.append(th).append(td);
            tbody.append(tr);
        }
        this.popup.element()
            .append(table)
            .css({
                top: 0,
                left: 0,
                "max-width": $(this.searchbar).width(),
                "z-index": 101,
                padding: '0',
                overflow: "auto"
            });
    }

    hoverHandler(entity)
    {
        var searchEngine = this;
        return function (event) {
            var popup = searchEngine.popup.element();
            var popupTable = popup.find('tbody');
            var currentId = $(this).find('[data-id]').attr('data-id');
            if (!popupTable.is(':visible')) {
                searchEngine.popup.show();
            }
            popup.css('left', event.originalEvent.pageX - 150);
            popup.css('top', event.originalEvent.pageY + 50);
            if (searchEngine.hovering === currentId) {
                return;
            }
            searchEngine.hovering = currentId;
            for (var key in searchEngine.table) {
                let val = AsalaeSearchEngine.hashGet(entity, searchEngine.table[key]);
                let element = popupTable.find('td[data-path="'+searchEngine.table[key]+'"]');
                if (typeof val === 'string') {
                    element.text(val);
                } else {
                    element.html('');
                    element.append(val);
                }
            }
        }
    }

    static hashGet(data, path)
    {
        var keys = path.split('.');
        var actual = keys.shift();
        if (!actual || !data) {
            return data;
        }
        if (actual === '{n}') {
            let ul = $('<ul></ul>');
            for (var key in data) {
                ul.append($('<li></li>').text(this.hashGet(data[key], keys.join('.'))));
            }
            return ul;
        }
        return this.hashGet(data[actual], keys.join('.'));
    }
}
