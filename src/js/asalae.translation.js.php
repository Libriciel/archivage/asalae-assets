<?php
header('Content-type: application/javascript');

/**
 * En lien direct avec src/translations.php qui contient les
 * traductions spécifiques à l'application
 */
$dirLength = strlen(__DIR__) - strlen('/vendor/libriciel/asalae-assets/src/js');
$basedir = strpos(__DIR__, 'asalae-assets')
    ? substr(__DIR__, 0, $dirLength)
    : dirname(dirname(__DIR__));
require $basedir . '/vendor/autoload.php';
require $basedir . '/config/bootstrap.php';
$trads = include $basedir . '/src/translations.php';
$translations = '';
foreach ($trads as $key => $value) {
    $key = preg_replace('/\s\s+/', ' ', $key);
    $value = preg_replace('/\s\s+/', ' ', $value);
    $value = preg_replace('/\\\\n/', "\n", $value);
    $key = addcslashes($key, "\n\"\\");
    $value = addcslashes($value, "\n\"\\");
    $translations .= "case \"$key\": str = \"$value\"; break;\n        ";
}
$translations = trim($translations).PHP_EOL;
?>
'use strict';

/**
 * permet la traduction façon CakePHP
 *
 * Exemple :
 * //# src/Locale/fr/js.po
 * msgid "foo {0}"
 * msgstr "bar {0}"
 *
 * //# src/translations.php
 * return ["foo {0}" => __d('js', "foo {0}")];
 *
 * //# webroot/js/ascript.js
 * alert(__("foo {0}", "baz")) // affichera : "bar baz"
 *
 * @param str
 * @returns {string}
 */
function __(str) {
    switch (str) {
        <?=$translations?>
        default: console.warn("No translation for: '"+str+"'");
    }

    for (var _len = arguments.length, args = Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
        try {
            str = str.replace('{' + (_key -1) + '}', arguments[_key]);
        } catch (e) {}
    }
    return str;
}