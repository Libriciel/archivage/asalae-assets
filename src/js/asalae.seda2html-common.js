/**
 * plier/déplier un bloc
 * @param legend
 * @param action 'plier'|'deplier'
 */
function collapse(legend, action)
{
    //Init DOM node
    var $fieldset = $(legend).closest('fieldset'),
            $icon = $(legend).find('i.icone-plier'),
            opener = 'fa fa-plus-square-o',
            closer = 'fa fa-minus-square-o',
            classopen = 'expanded',
            classclosed = 'collapsed',
            minimizetitle = 'cliquer pour replier',
            maximizetitle = 'cliquer pour déplier';

    if ($fieldset.hasClass('folder')) {
        closer = 'fa fa-folder-open';
        opener = 'fa fa-folder';
    } else if ($fieldset.hasClass('Document')) {
        closer = 'fa fa-file-text-o';
        opener = 'fa fa-file-o';
    } else if ($fieldset.hasClass('Document_v10')) {
        closer = 'fa fa-file-text-o';
        opener = 'fa fa-file-o';
    } else if ($fieldset.hasClass('dataobjectreference')) {
        closer = 'fa fa-file-text-o';
        opener = 'fa fa-file-o';
    } else if ($fieldset.hasClass('formatIdentification')) {
        closer = 'fa fa-info';
        opener = 'fa fa-info';
    } else if ($fieldset.hasClass('signingInformation')) {
        closer = 'fa fa-info';
        opener = 'fa fa-info';
    } else if ($fieldset.hasClass('RelatedData')) {
        closer = 'fa fa-paperclip';
        opener = 'fa fa-paperclip';
    } else if ($fieldset.hasClass('Tag')) {
        closer = 'fa fa-flag';
        opener = 'fa fa-flag-o';
    } else if ($fieldset.hasClass('Appraisal')) {
        closer = 'fa fa-calendar';
        opener = 'fa fa-calendar';
    } else if ($fieldset.hasClass('rules')) {
        closer = 'fa fa-gavel';
        opener = 'fa fa-gavel';
    } else if ($fieldset.hasClass('rules2')) {
            closer = 'fa fa-gavel';
            opener = 'fa fa-gavel';
    } else if ($fieldset.hasClass('HoldRule')) {
        closer = 'fa fa-gavel';
        opener = 'fa fa-gavel';
    } else if ($fieldset.hasClass('LogBook')) {
        closer = 'fa fa-book';
        opener = 'fa fa-book';
    } else if ($fieldset.hasClass('Keyword')) {
        closer = 'fa fa-flag';
        opener = 'fa fa-flag-o';
    } else if ($fieldset.hasClass('Keyword_v10')) {
        closer = 'fa fa-flag';
        opener = 'fa fa-flag-o';
    } else if ($fieldset.hasClass('ContentDescriptive')) {
        closer = 'fa fa-flag';
        opener = 'fa fa-flag-o';
    } else if ($fieldset.hasClass('Organisation')) {
        closer = 'fa fa-sitemap';
        opener = 'fa fa-sitemap';
    } else if ($fieldset.hasClass('liste_integrity')) {
        closer = 'fa fa-tags';
        opener = 'fa fa-tags';
    } else if ($fieldset.hasClass('filesBloc')) {
        closer = 'fa fa-tags';
        opener = 'fa fa-tags';
    } else if ($fieldset.hasClass('TransferringAgency')) {
        closer = 'fa fa-sitemap';
        opener = 'fa fa-sitemap';
    } else if ($fieldset.hasClass('generique')) {
        closer = 'fa fa-angle-down';
        opener = 'fa fa-angle-right';
    } else if ($fieldset.hasClass('ArchivalAgency')) {
        closer = 'fa fa-sitemap';
        opener = 'fa fa-sitemap';
    } else if ($fieldset.hasClass('OriginatingAgency')) {
        closer = 'fa fa-sitemap';
        opener = 'fa fa-sitemap';
    } else if ($fieldset.hasClass('Address')) {
        closer = 'fa fa-map-marker';
        opener = 'fa fa-map-marker';
    } else if ($fieldset.hasClass('Contact')) {
        closer = 'fa fa-envelope';
        opener = 'fa fa-envelope-o';
    } else if ($fieldset.hasClass('ContentDescription')) {
        closer = 'fa fa-info';
        opener = 'fa fa-info';
    } else if ($fieldset.hasClass('relatedObjectReference')) {
        closer = 'fa fa-info';
        opener = 'fa fa-info';
    } else if ($fieldset.hasClass('codeListClass')) {
        closer = 'fa fa-info';
        opener = 'fa fa-info';
    } else if ($fieldset.hasClass('Communication')) {
        closer = 'fa fa-bullhorn';
        opener = 'fa fa-bullhorn';
    } else if ($fieldset.hasClass('CustodialHistory')) {
        closer = 'fa fa-clock-o';
        opener = 'fa fa-clock-o';
    } else if ($fieldset.hasClass('gestionClass')) {
            closer = 'fa fa-list-ul';
            opener = 'fa fa-list-ul';
    } else if ($fieldset.hasClass('managementMetadata')) {
        closer = 'fa fa-list-ul';
        opener = 'fa fa-list-ul';
    } else if ($fieldset.hasClass('managementMetadata_Rules')) {
        closer = 'fa fa-list-ul';
        opener = 'fa fa-list-ul';
    } else if ($fieldset.hasClass('descriptionClass')) {
        closer = 'fa fa-list-ul';
        opener = 'fa fa-list-ul';
    } else if ($fieldset.hasClass('content')) {
        closer = 'fa fa-ellipsis-h';
        opener = 'fa fa-ellipsis-h';
    } else if ($fieldset.hasClass('parentArchiveUnit')) {
        closer = 'fa fa-folder-open';
        opener = 'fa fa-folder';
} else if ($fieldset.hasClass('deleted_parentArchiveUnit')) {
        closer = 'fa fa-folder-open';
        opener = 'fa fa-folder';
    } else if ($fieldset.hasClass('childArchiveUnit')) {
        closer = 'fa fa-folder-open';
        opener = 'fa fa-folder';
    } else if ($fieldset.hasClass('deleted_childArchiveUnit')) {
        closer = 'fa fa-folder-open';
        opener = 'fa fa-folder';
    } else if ($fieldset.hasClass('deleted_childArchiveUnit1')) {
        closer = 'fa fa-folder-open';
        opener = 'fa fa-folder';
    } else if ($fieldset.hasClass('coverage')) {
        closer = 'fa fa-shield';
        opener = 'fa fa-shield';
    } else if ($fieldset.hasClass('agents')) {
        closer = 'fa fa-user';
        opener = 'fa fa-user';
    } else if ($fieldset.hasClass('Agent')) {
        closer = 'fa fa-user';
        opener = 'fa fa-user';
    } else if ($fieldset.hasClass('places')) {
        closer = 'fa fa-map-marker';
        opener = 'fa fa-map-marker';
    } else if ($fieldset.hasClass('event')) {
        closer = 'fa fa-bullhorn';
        opener = 'fa fa-bullhorn';
    } else if ($fieldset.hasClass('signature')) {
        closer = 'fa fa-pencil-square';
        opener = 'fa fa-pencil-square-o';
    } else if ($fieldset.hasClass('gps')) {
        closer = 'fa fa-map-marker';
        opener = 'fa fa-map-marker';
    } else if ($fieldset.hasClass('RelatedObjectReference')) {
        closer = 'fa fa-link';
        opener = 'fa fa-link';
    } else if ($fieldset.hasClass('AccessRestriction')) {
        closer = 'fa fa-unlock';
        opener = 'fa fa-lock';
    } else if ($fieldset.hasClass('meta_descriptive_10')) {
        closer = 'fa fa-list-ul';
        opener = 'fa fa-list-ul';
    } else if ($fieldset.hasClass('metadonnees_descriptives')) {
        closer = 'fa fa-list-ul';
        opener = 'fa fa-list-ul';
    } else if ($fieldset.hasClass('Integrity')) {
        closer = 'fa fa-certificate';
        opener = 'fa fa-certificate';
    }

    if (action === undefined && $fieldset.hasClass(classopen) || action === 'plier') {
        //Etat: déplié; Action: plier;
        $fieldset
                .removeClass(classopen)
                .addClass(classclosed);
        $icon
                .removeClass(closer)
                .addClass(opener);

        $(legend).attr('title', maximizetitle);
        //Animation slide
        $(legend).next().slideUp();
    } else if (action === undefined && $fieldset.hasClass(classclosed) || action === 'deplier') {
        //Etat: plié; Action: déplier;
        $fieldset
                .removeClass(classclosed)
                .addClass(classopen);
        $icon
                .removeClass(opener)
                .addClass(closer);

        $(legend).attr('title', minimizetitle);
        //Animation slide
        if ($(legend).next().text() !== '') {
            $(legend).next().slideDown();
        }
    }
}

/**
 * tout plier/déplier
 * @param {boolean} visible
 */
function expandAll(visible)
{
    var action = visible ? 'deplier' : 'plier';
    $('#main').find('fieldset > legend').each(
        function () {
            collapse(this, action);
        }
    );
    $('#files').find('fieldset > legend').each(
        function () {
            collapse(this, action);
        }
    );
}

function expandFiles(visible)
{
    var action = visible ? 'deplier' : 'plier';
    $('#files').find('fieldset > legend').each(
        function() {
            collapse(this,action);
        }
    );
}

function expandArchiveObjects(visible)
{
    var action = visible ? 'deplier' : 'plier';
    $('#archObj').find('fieldset > legend').each(
        function() {
            collapse(this,action);
        }
    )
}

function invertDataName()
{
    $('label, legend').each(
        function () {
            var value = $(this).find('span.text').text(),
            title = $(this).attr('data-name');
            if (title) {
                $(this).attr('data-name', value).find('span.text').text(title);
            }
        }
    );
}

$(function () {
    $('.depliage_fieldset').on(
        'click',
        function (event) {
            var target_uid = $(this).attr('href');
            collapse($(target_uid), 'deplier');
        }
    );

    $('[tabindex]').on('keypress', function (event) {
        if ([13, 32].includes(event.originalEvent.keyCode)) {
            event.preventDefault();
            $(this).click();
        }
    });
});
