/*
 * "%s %s %s".format("Format", "For", "Javascript");
 * Based on PHPJS sprintf.
 *
 * @author: Nijikokun <nijikokun@gmail.com>
 * @copyright: 2011 (c) Nijikokun
 */
String.prototype.format=function(){var k=arguments,q=0,o=function(b,a,f,c){f||(f=" ");a=b.length>=a?"":Array(1+a-b.length>>>0).join(f);return c?b+a:a+b},m=function(b,a,f,c,g,d){var e=c-b.length;e>0&&(b=f||!g?o(b,c,d,f):b.slice(0,a.length)+o("",e,"0",!0)+b.slice(a.length));return b},n=function(b,a,f,c,g,d,e){b>>>=0;f=f&&b&&{2:"0b",8:"0",16:"0x"}[a]||"";b=f+o(b.toString(a),d||0,"0",!1);return m(b,f,c,g,e)};return this.replace(/%%|%(\d+\$)?([-+\'#0 ]*)(\*\d+\$|\*|\d+)?(\.(\*\d+\$|\*|\d+))?([scboxXuidfegEG])/g,
    function(b,a,f,c,g,d,e){var i,j;if(b=="%%")return"%";var h=!1;j="";var l=g=!1;i=" ";for(var r=f.length,p=0;f&&p<r;p++)switch(f.charAt(p)){case " ":j=" ";break;case "+":j="+";break;case "-":h=!0;break;case "'":i=f.charAt(p+1);break;case "0":g=!0;break;case "#":l=!0}c=c?c=="*"?+k[q++]:c.charAt(0)=="*"?+k[c.slice(1,-1)]:+c:0;c<0&&(c=-c,h=!0);if(!isFinite(c))throw Error("format: (min)width must be finite");a=a?k[a.slice(0,-1)]:k[q++];d=d?d=="*"?+k[q++]:d.charAt(0)=="*"?+k[d.slice(1,-1)]:+d:"fFeE".indexOf(e)>
    -1?6:e=="d"?0:void 0;switch(e){case "s":return e=String(a),d!=null&&(e=e.slice(0,d)),m(e,"",h,c,g,i);case "c":return e=String.fromCharCode(+a),d!=null&&(e=e.slice(0,d)),m(e,"",h,c,g,void 0);case "b":return n(a,2,l,h,c,d,g);case "o":return n(a,8,l,h,c,d,g);case "x":return n(a,16,l,h,c,d,g);case "X":return n(a,16,l,h,c,d,g).toUpperCase();case "u":return n(a,10,l,h,c,d,g);case "i":case "d":return i=+a|0,b=i<0?"-":j,a=b+o(String(Math.abs(i)),d,"0",!1),m(a,b,h,c,g);case "e":case "E":case "f":case "F":case "g":case "G":return i=
        +a,b=i<0?"-":j,j=["toExponential","toFixed","toPrecision"]["efg".indexOf(e.toLowerCase())],e=["toString","toUpperCase"]["eEfFgG".indexOf(e)%2],a=b+Math.abs(i)[j](d),m(a,b,h,c,g)[e]();default:return b}})};