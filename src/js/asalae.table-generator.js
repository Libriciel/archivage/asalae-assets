/* global moment, jQuery, Cookies, AsalaePopup */

/**
 * Ensemble de classes javascript pour manipuler/générer des tableaux de
 * résultats.
 */

if (typeof __ === 'undefined') {
    var __ = function (str) {
        return str;
    };
}

/**
 * Permet de manipuler un tableau à partir d'un export de donnée CakePHP
 * transformé en json et de quelques paramètres
 * (nom des colonnes, les liens, les titres...)
 *
 * @param {Element|jQuery} table
 * @returns {TableGenerator}
 */
class TableGenerator {
    constructor(table)
    {
        this.table = table;
        this.data = {};
        this._params = null;
        this._prevParams = null;
        this._rawParams = null;
        this._configId = null;
        this._prevView = null;
        this._configChanged = false;
        this._cookieLess = false;
        this._memory = {
            view: null,
            activeLine: null,
            favorites: [],
            max_per_page: null,
            columns: []
        };
        this.view = 'default';
        this.cookies = {
            base: 'table-generator-config-table-'+$(table).attr('id')
        };
        this.cookies.active = this.cookies.base + '-tr-active';
        this.cookies.accepted = {
            "default": this.cookies.base + '-tr-accepted-default',
            "div": this.cookies.base + '-tr-accepted-div',
            "column": this.cookies.base + '-tr-accepted-column'
        };
        this.cookies.view = this.cookies.base + '-view';
        this.cookies.maxPerPage = this.cookies.base + '-max-per-page';
        this.elements = {
            draggable: {id: 'draggable-'+$(this.table).attr('id')}
        };
        this.messages = {
            favorite: "Add/remove from favorites",
            confirmSwitchView: "Do you want to save your change before switch the view?",
            confirmChangeMaxPerPage: "We need to refresh the page to apply your changes. Do you want to refresh it now ?",
            selectCheckbox: "Select",
            selectAllCheckboxes: "Select all"
        };
        this.max_per_page = 0;
        this._ids = [];
        if (typeof TableGenerator.instance === 'undefined') {
            TableGenerator.instance = [];
        }
        let i = 0;
        do {
            this.instanceIndex = i;
            i++;
        } while (typeof TableGenerator.instance[this.instanceIndex] !== 'undefined');
        TableGenerator.instance[this.instanceIndex] = this;
        TableGenerator.lastInstance = this;
        $(table).attr('data-table-uid', this.instanceIndex);
        $(table).data('table-generator', this);
    }

    /**
     * Génère le contenu d'un tableau (efface le precédent)
     *
     * @param {object} data
     * @param {object} params
     * @returns {TableGenerator}
     */
    generateAll(data = this.data, params = this._viewParams())
    {
        data = typeof params.tbody.order !== 'undefined'
            ? TableGenerator.evalSortArray(data, params.tbody.order)
            : data;

        TableGenerator.appendActions(data, this.actions);
        $(this.table).find('thead').remove();
        $(this.table).prepend(this._generateThead(params));

        $(this.table).find('tbody').remove();
        if (typeof data !== 'undefined') {
            $(this.table).append(this._generateTbody(data, params));
        }

        this._toggleClassActiveCookie();
        $(window).trigger('resize');
        $(this.table).trigger('change');
        $(this.table).find('td > div').trigger('loaded');

        return this;
    };

    /**
     * Reconstruit le tbody du tableau
     *
     * @param {object} data
     * @param {object} params
     * @returns {TableGenerator}
     */
    generateTbody(data = this.data, params = this._viewParams())
    {
        data = typeof params.tbody.order !== 'undefined'
            ? TableGenerator.evalSortArray(data, params.tbody.order)
            : data;

        $(this.table).find('tbody').remove();
        if (typeof data !== 'undefined') {
            $(this.table).append(this._generateTbody(data, params));
        }

        this._toggleClassActiveCookie();
        $(window).trigger('resize');
        $(this.table).trigger('change');

        return this;
    }

    /**
     * Permet de retirer une ligne de data par identifiant
     * Utile lors d'une suppression
     *
     * @param {string} id
     * @param {string|null} path
     * @returns {TableGenerator}
     */
    removeDataId(id, path = null)
    {
        var data = this.data;

        if (path === null) {
            path = this._params.tbody.identifier;
        }

        for (var key in data) {
            try {
                if (TableGenerator._evalData(path, data[key])+'' === id+'') {
                    if (data[key].uid) {
                        this.cancelUploadWhenDeleted(data[key].uid);
                    }
                    this.data.splice(key, 1);
                    this.table.trigger('deleted_data.table', id);
                    break;
                }
            } catch (e) {
            }
        }

        return this;
    }

    /**
     * cancel() le file de l'uploader (permet de l'uploader à nouveau)
     * @param {string} uniqueIdentifier
     */
    cancelUploadWhenDeleted(uniqueIdentifier)
    {
        if (!this.uploader
            || !this.uploader.files?.[0]?.uniqueIdentifier
        ) {
            return;
        }
        var files = this.uploader.files;
        for (var i = 0; i < files.length; i++) {
            var file = files[i];
            if (file.uniqueIdentifier === uniqueIdentifier) {
                file.cancel();
                return;
            }
        }
    }

    /**
     * Permet d'obtenir les données d'une ligne à partir d'un identifiant
     *
     * @param {string} id
     * @param {string|null} path
     * @returns {object}
     */
    getDataId(id, path = null)
    {
        var data = this.data;

        if (path === null) {
            path = this._params.tbody.identifier;
        }

        for (var key in data) {
            try {
                if (TableGenerator._evalData(path, data[key])+'' === id+'') {
                    return data[key];
                }
            } catch (e) {
            }
        }
    }

    /**
     * Permet de remplacer les données d'une ligne à partir d'un identifiant
     *
     * @param {string} id
     * @param {object} newData
     * @param {string|null} path
     * @returns {TableGenerator}
     */
    replaceDataId(id, newData, path = null)
    {
        var data = this.data;

        if (path === null) {
            path = this._params.tbody.identifier;
        }

        for (var key in data) {
            try {
                if (TableGenerator._evalData(path, data[key]) === id) {
                    var old_data = typeof data[key] === 'object'
                        ? $.extend({}, data[key], true)
                        : data[key];
                    data[key] = newData;
                    this.table.trigger(
                        'replaced_data.table',
                        {
                            old_data: old_data,
                            new_data: newData,
                        }
                    );
                }
            } catch (e) {
            }
        }

        return this;
    }

    /**
     * Permet d'obtenir la liste des cookies et leurs valeurs
     *
     * @returns {object}
     */
    exportCookies(toString = false)
    {
        var results = this._recursiveExportCookies(this.cookies);
        return toString ? JSON.stringify(results) : results;
    }

    /**
     * Fait la lecture de la liste de cookies de façon recursive
     *
     * @param {object} cookies
     * @returns {object} cookies
     */
    _recursiveExportCookies(cookies)
    {
        var results = {},
            value;
        for (let key in cookies) {
            if (typeof cookies[key] === 'string') {
                value = TableGenerator.cookie(cookies[key]);
                if (typeof value !== 'undefined') {
                    results[cookies[key]] = value;
                }
            } else {
                Object.assign(results, this._recursiveExportCookies(cookies[key]));
            }
        }
        return results;
    }

    /**
     * Importe les cookies
     *
     * @param {object|string} cookies
     * @returns {TableGenerator}
     */
    importCookies(cookies)
    {
        if (typeof cookies === 'string') {
            cookies = JSON.parse(cookies);
        }
        if (typeof cookies === 'object' && Object.keys(cookies).length) {
            for (let key in cookies) {
                TableGenerator.cookie(key, cookies[key]);
            }
        }
        return this;
    }

    /**
     * Setter de params
     *
     * @param {object} params
     * @returns {TableGenerator}
     */
    params(params)
    {
        this._rawParams = JSON.parse(JSON.stringify(params));
        // on place les thead d'une seule ligne dans un array si besoin
        if (typeof params === 'object'
            && typeof params.thead === 'object'
            && Array.isArray(params.thead)
            && typeof params.thead[0] === 'object'
            && !Array.isArray(params.thead[0])
        ) {
            params.thead = [params.thead];
        }
        this._params = {
            tbody: {},
            thead:{
                "default": [],
                column: [],
                div: []
            }};
        this._params.tbody = params.tbody;

        for (let tr in params.thead) {
            var viewDiv = new TableHead(
                'view-div',
                {
                    target: null,
                    'data-view': 'div',
                    label: this._params.tbody['view-div-label'],
                    thead: []
                }
            );
            this._params.thead.default[tr] = [];
            this._params.thead.column[tr] = [];
            this._params.thead.div[tr] = [viewDiv];

            for (let th in params.thead[tr]) {
                params.thead[tr][th].initialDisplay = !!params.thead[tr][th].display;
                switch (params.thead[tr][th]['data-view']) {
                    case 'default':
                        this._params.thead.default[tr].push(params.thead[tr][th]);
                        // no-break FALLTHROUGH
                    case 'div':
                        this._params.thead.div[0][0].thead.push(params.thead[tr][th]);
                        break;
                    case 'column':
                        this._params.thead.default[tr].push(params.thead[tr][th]);
                        this._params.thead.column[tr].push(params.thead[tr][th]);
                        this._params.thead.div[tr].push(params.thead[tr][th]);
                }
            }

            // ajoute un bouton filter sur viewDiv
            viewDiv.filter = {};
            for (let i in viewDiv.thead) {
                if (viewDiv.thead[i].filter === null) {
                    continue;
                }
                for (var field in viewDiv.thead[i].filter) {
                    viewDiv.filter[field] = $(viewDiv.thead[i].filter[field]);
                    // ajoute un label et un attribut for
                    let id = viewDiv.filter[field].find('input[id], select[id], textarea[id]').first().attr('id');
                    let label = viewDiv.thead[i].label;
                    if (typeof label === 'object' && label.label) {
                        label = $('<div>').html(label.label).text();
                    }
                    viewDiv.filter[field].prepend($('<label>').text(label).attr('for', id));

                    // si c'est un select, on s'assure qu'il ai une option vide
                    let select = viewDiv.filter[field].find('select');
                    if (select.length > 0 && select.find('option[value=""]').length === 0) {
                        select.prepend('<option value=""></option>');
                        select.val('');
                    }
                }
            }
        }
        return this;
    }

    /**
     * Remet la configuration du tableau par défaut
     */
    reset()
    {
        this._memory = {
            view: null,
            activeLine: null,
            favorites: [],
            max_per_page: null,
            columns: []
        };
        this.params(this._rawParams);
        this.generateAll();
        this._configChanged = true;
    }

    /**
     * Permet d'obtenir les paramètres actif de la vue
     *
     * @returns {object}
     */
    _viewParams()
    {
        return {
            tbody: this._params.tbody,
            thead: this._params.thead[this.view]
        };
    }

    /**
     * Applique la configuration en cookie
     *
     * @returns {TableGenerator}
     */
    applyCookies()
    {
        var acceptedCookie = TableGenerator.cookie(this.cookies.accepted[this.view]),
            accepted = acceptedCookie ? JSON.parse(acceptedCookie) : null,
            view = TableGenerator.cookie(this.cookies.view),
            maxPerPage = TableGenerator.cookie(this.cookies.maxPerPage),
            activeLine = TableGenerator.cookie(this.cookies.active),
            thead,
            visible,
            id;

        this._memory.activeLine = null;
        this._memory.view = null;
        this._memory.max_per_page = null;
        this._memory.columns = null;
        this._memory.favorites = [];

        if (activeLine) {
            let dataId = activeLine.match(/\[data\-id="([^"]+)"\]/);
            if (dataId) {
                this._memory.activeLine = dataId[1];
            }
        }
        if (view) {
            this.view = view;
            this._memory.view = view;
        }

        if (maxPerPage) {
            this.max_per_page = maxPerPage;
            this._memory.max_per_page = maxPerPage;
        }

        var cookies = Object.keys({...Cookies.get(), ...TableGenerator.cookies})
            .filter(key => Cookies.get(key) !== "0");
        var baseCookieName = 'table-favorite-'+this.table.attr('id')+'-';
        var baseCookieNameLen = baseCookieName.length;
        for (var i = 0; i < cookies.length; i++) {
            if (cookies[i].substr(0, baseCookieNameLen) === baseCookieName) {
                this._memory.favorites.push(cookies[i].substr(baseCookieNameLen));
            }
        }

        if (accepted && accepted.length) {
            this._memory.columns = [];
            for (let tr in this._viewParams().thead) {
                var activeThead = this.view !== 'div'
                    ? this._viewParams().thead[tr]
                    : this._viewParams().thead[tr][0].thead;

                thead = [];
                visible = false;

                // On ajoute en premier dans l'ordre mémorisé les colonnes choisies
                for (let key in accepted) {
                    for (let th in activeThead) {
                        id = this.elements.draggable.id+'-'+activeThead[th]['_id'];
                        if (id === accepted[key]) {
                            if (this._memory.columns.indexOf(activeThead[th]['_id']) === -1) {
                                this._memory.columns.push(activeThead[th]['_id']);
                            }
                            activeThead[th].display = true;
                            thead.push(activeThead[th]);
                            break;
                        }
                    }
                }

                // On ajoute ensuite en champs cachés les elements restant
                for (let th in activeThead) {
                    id = this.elements.draggable.id+'-'+activeThead[th]['_id'];
                    visible = false;
                    for (let key in accepted) {
                        if (id === accepted[key]) {
                            visible = true;
                            break;
                        }
                    }
                    if (!visible) {
                        activeThead[th].display = false;
                        thead.push(activeThead[th]);
                    }
                }

                // on fini par les actions si elles existent
                for (let th in activeThead) {
                    if (activeThead[th]['_id'] === 'actions') {
                        for (let i = 0; i < thead.length; i++) {
                            if (thead[i]['_id'] === 'actions') {
                                thead.splice(i, 1);
                                break;
                            }
                        }
                        activeThead[th].display = true;
                        thead.push(activeThead[th]);
                        break;
                    }
                }

                if (this.view !== 'div') {
                    this._params.thead[this.view][tr] = thead;
                } else {
                    this._params.thead.div[tr][0].thead = thead;
                }
            }
        }

        return this;
    }

    /**
     * Supprime tous les cookies liés à la table courante
     *
     * @param {object} cookies - default this.cookies
     * @returns {TableGenerator}
     */
    deleteCookies(cookies = this.cookies)
    {
        for (var key in cookies) {
            if (typeof cookies[key] === 'string') {
                TableGenerator.cookie(cookies[key], null);
            } else {
                this.deleteCookies(cookies[key]);
            }
        }

        return this;
    }

    /**
     * Rempli la zone de configuration du tableau de résultats
     *
     * @param {string} id
     * @returns {TableGenerator}
     */
    insertConfiguration(id)
    {
        this._configId = id;
        this._dropZone($(id+' div.showed div.drop-zone'));
        this._dropZone($(id+' div.hidded div.drop-zone'));
        this._btnSwitchViews();
        this._btnAcceptChange();
        this._btnCancelChange();
        this._btnApplyChange();
        this._prevParams = this._viewParams();
        this.fillDropZone();
        this._selectMaxResultsPerPage();

        return this;
    }

    /**
     * Select du nombre de résultats par pages
     *
     * @returns {TableGenerator}
     */
    _selectMaxResultsPerPage()
    {
        var elements = $(this._configId+' .nb-max-results'),
            that = this;

        this.initial_max_per_page = $(elements).val();
        if (this.max_per_page) {
            $(elements).val(this.max_per_page);
        } else {
            this.max_per_page = this.initial_max_per_page;
        }

        $(elements).change(
            function () {
                that.max_per_page = $(this).val();
            }
        );

        return this;
    }

    /**
     * Boutons permettant de changer de "rendu" du tableau
     *
     * @returns {TableGenerator}
     */
    _btnSwitchViews()
    {
        var elements = $(this._configId+' .switch-table-view'),
            that = this;

        $(elements).off('.clickEnter').clickEnter(
            function (event) {
                if ($(this).hasClass('active')) {
                    return;
                }
                $(elements).removeClass('active');
                $(this).addClass('active');

                if (that._configChanged && confirm(that.messages.confirmSwitchView)) {
                    that._applyChange();
                }
                that._configChanged = false;

                if (!that._prevView) {
                    that._prevView = that.view;
                }
                that.view = $(this).attr('data-view');

                $(that._configId+' div.drop-zone').parent()
                .css(
                    'min-height',
                    function () {
                        return $(this).height()+'px';
                    }
                );

                let animationSpeed = 200;
                $(that._configId+' div.drop-zone').slideUp(
                    animationSpeed,
                    'swing',
                    function () {
                        that.fillDropZone();
                        setTimeout(
                            function () {
                                $(that._configId+' div.drop-zone').slideDown(animationSpeed);
                            },
                            10
                        );
                    }
                );
            }
        );

        return this;
    }

    /**
     * Rempli les drop zones avec le paramétrage actuel
     *
     * @param {object} params
     * @returns {TableGenerator}
     */
    fillDropZone(params = this._viewParams())
    {
        $(this._configId+' div.drop-zone div.draggable-element').remove();

        for (var tr in params.thead) {
            if (this.view === 'div') {
                for (let th in params.thead[tr][0].thead) {
                    let dropZone = params.thead[tr][0].thead[th].display
                        ? 'div.showed'
                        : 'div.hidded';

                    $(this._configId+' '+dropZone+' div.drop-zone').append(
                        this._getDraggableElement(
                            params.thead[tr][0].thead[th]
                        )
                    );
                }
                continue;
            }

            for (let th in params.thead[tr]) {
                let dropZone = params.thead[tr][th].display
                    ? 'div.showed'
                    : 'div.hidded';
                if (params.thead[tr][th]._id === 'actions') {
                    continue;
                }

                $(this._configId+' '+dropZone+' div.drop-zone').append(
                    this._getDraggableElement(
                        params.thead[tr][th]
                    )
                );
            }
        }

        return this;
    }

    /**
     * Bouton Valider
     *
     * @returns {TableGenerator}
     */
    _btnAcceptChange()
    {
        var that = this;
        $(this._configId+' button.accept')
            .off('.clickEnter')
            .clickEnter(
                function () {
                    that._applyChange();
                    if (that.max_per_page !== that.initial_max_per_page
                        && confirm(that.messages.confirmChangeMaxPerPage)
                    ) {
                        location.reload();
                    }
                }
            );
        return this;
    }

    /**
     * Bouton Annuler
     *
     * @returns {TableGenerator}
     */
    _btnCancelChange()
    {
        var that = this,
            accepted = [];

        $(this._configId+' button.cancel')
            .off('.clickEnter')
            .clickEnter(
                function () {
                    let maxPerPage = that._memory.max_per_page;
                    that._prevParams = that._params;
                    if (that._prevView) {
                        that.view = that._prevView;
                    }

                    $(that._configId+' div.showed div.drop-zone div.draggable-element').remove();
                    $(that._configId+' div.hidded div.drop-zone div.draggable-element').remove();
                    that.insertConfiguration(that._configId);
                    that._configChanged = false;
                    if (maxPerPage) {
                        $(that._configId+' .nb-max-results').val(maxPerPage);
                    } else {
                        let v = $(that._configId+' .nb-max-results option').first();
                        $(that._configId+' .nb-max-results')
                        .val(typeof v.val() !== 'undefined' ? v.val() : v.text());
                    }
                }
            );

        return this;
    }

    /**
     * Bouton Appliquer
     *
     * @returns {TableGenerator}
     */
    _btnApplyChange()
    {
        var that = this;
        $(this._configId+' button.apply')
            .off('.clickEnter')
            .clickEnter(
                function () {
                    that._applyChange();
                }
            );
        return this;
    }

    /**
     * Applique les modifications de la configuration du tableau
     *
     * @returns {TableGenerator}
     */
    _applyChange()
    {
        var accepted = [];
        var columns = [];
        $(this._configId+' div.showed div.drop-zone div.draggable-element')
            .each(
                function () {
                    accepted.push($(this).attr('id'));
                    columns.push($(this).attr('data-field'));
                }
            );

        this._prevMemory = JSON.stringify(this._memory);
        this._memory.columns = columns;
        if (!this._cookieLess) {
            TableGenerator.cookie(this.cookies.accepted[this.view], JSON.stringify(accepted));
            TableGenerator.cookie(this.cookies.view, this.view);
            TableGenerator.cookie(this.cookies.maxPerPage, this.max_per_page);
            this.applyCookies();
        } else {
            this.loadMemory(this._memory);
        }
        this.generateAll();
        this._configChanged = false;
        this._prevParams = this._params;
        this._prevView = null;
        if (JSON.stringify(this._memory) !== this._prevMemory) {
            this.table.trigger('configuration_changed');
        }

        return this;
    }

    /**
     * Applique les evenements pour transformer un element en drop zone
     *
     * @param {jQuery|Element} element
     * @returns {TableGenerator}
     */
    _dropZone(element)
    {
        var that = this;

        $(element).sortable();
        $(element).droppable(
            {
                drop: function (event, ui) {
                    that._configChanged = true;
                    if (event.target !== $(event.originalEvent.target).parent().get(0)) {
                        $(event.target).append(
                            $(event.originalEvent.target)
                            .removeAttr('style')
                            .clone()
                        );
                        $(event.originalEvent.target).remove();
                    }
                }
            }
        );
        $(element).disableSelection();

        return this;
    }

    /**
     * Retourne un element draggable
     *
     * @param {string} info
     * @returns {jQuery}
     */
    _getDraggableElement(info)
    {
        var element = $('<div></div>'),
            label = info.label;
        if (typeof label === 'object' && label.label) {
            label = label.label;
        }
        try {
            if ($(label).length >= 1) {
                if (label.label) {
                    label = $(label.label).text().trim();
                } else {
                    label = $(label).children().remove().end().text();
                }
            }
        } catch (e) {
        }

        element.addClass('draggable-element')
            .addClass('btn')
            .addClass('btn-info')
            .addClass(info._id)
            .attr('id', this.elements.draggable.id+'-'+info._id)
            .attr('role', 'button')
            .attr('data-field', info._id)
            .attr('tabindex', 0)
            .append(label)
            .dblclick(
                function () {
                    if ($(this).parent().parent().hasClass('showed')) {
                        $(this).parent().parent().parent().find('.hidded .drop-zone')
                        .append(this);
                    } else {
                        $(this).parent().parent().parent().find('.showed .drop-zone')
                        .append(this);
                    }
                }
            )
            .on(
                'keyup',
                function (event) {
                    switch (event.originalEvent.keyCode) {
                        case 13: // Enter
                            $(this).trigger('dblclick');
                            break;
                        case 37: // gauche
                            TableGenerator._moveDraggableElement($(this), 'left');
                            break;
                        case 38: // haut
                            break;
                        case 39: // droite
                            TableGenerator._moveDraggableElement($(this), 'right');
                            break;
                        case 40: // bas
                            break;
                    }
                }
            );

        if (typeof info.class !== 'undefined' && info.class !== null) {
            element.addClass(info.class);
        }

        return element;
    }

    /**
     * Modifi l'ordre d'affichage d'un element dans son parent
     * @param element
     * @param direction
     */
    static _moveDraggableElement(element, direction)
    {
        var index = element.index(),
            parent = element.parent(),
            children = parent.children(),
            count = children.length
        ;
        if ((index === 0 && direction === 'left')
            || (index === count -1 && direction === 'right')
        ) {
            return;
        }
        if (direction === 'left') {
            element.insertBefore(children.eq(index -1));
        } else {
            element.insertAfter(children.eq(index +1));
        }
        element.focus();
    }

    /**
     * Permet d'ordonner un tableau (data) de façon dynamique selon str
     *
     * @param {object} data
     * @param {string} str Chemin sur lequel ranger le tableau ex: Model.name
     * @returns {object} data ordonné
     */
    static evalSortArray(data, str)
    {
        data.sort(
            function (a, b) {
                var a2, b2;
                try {
                    a2 = TableGenerator._evalData(str, a);
                } catch (e) {
                    a2 = -Infinity;
                }
                try {
                    b2 = TableGenerator._evalData(str, b);
                } catch (e) {
                    b2 = -Infinity;
                }

                return a2 > b2;
            }
        );

        return data;
    }

    /**
     * Ajoute à un json de data, les actions selon params
     *
     * @param {object} data
     * @param {object} params
     * @returns {object} data complété
     */
    static appendActions(data, params)
    {
        // Pour chaque lignes dans data (tr)
        for (let i = 0; i < data.length; i++) {
            let subactions = {};
            data[i].actions = {};

            // Pour chaque actions
            for (let key in params) {
                let appendLabel = false;
                data[i].actions[key] = typeof params[key].href === 'string'
                    ? $('<a></a>')
                    : $('<button></button>');

                // Pour chaque paramètre d'action
                for (let k in params[key]) {
                    switch (k) {
                        case 'label':
                            data[i].actions[key].append(
                                TableGenerator._sprintf(
                                    params[key][k],
                                    params[key]['params'],
                                    data[i]
                                )
                            );
                            break;
                        case 'params':
                            break;
                        case 'confirm':
                            data[i].actions[key].click(
                                (function (data) {
                                    return function (event) {
                                        if (!confirm(
                                            TableGenerator._sprintf(
                                                params[key][k],
                                                params[key]['params'],
                                                data
                                            )
                                        )
                                        ) {
                                            return false;
                                        }
                                        if (params[key]['data-callback']) {
                                            eval(
                                                TableGenerator._sprintf(
                                                    params[key]['data-callback'],
                                                    params[key]['params'],
                                                    data
                                                )
                                            );
                                        }
                                    }
                                })(data[i])
                            );
                            break;
                        case 'display':
                        case 'displayEval':
                            let display = true;
                            if (typeof params[key]['displayEval'] !== 'undefined') {
                                try {
                                    display = eval(params[key][k].replace(/{index}/g, i));
                                } catch (e) {
                                    display = false;
                                }
                            }
                            if (typeof params[key]['display'] !== 'undefined') {
                                display = display && params[key]['display'];
                            }
                            data[i].actions[key].toggle(display);
                            data[i].actions[key].prop('disabled', !display);
                            break;
                        case 'title':
                            let text = $('<span></span>').html(
                                TableGenerator._sprintf(
                                    params[key][k],
                                    params[key]['params'],
                                    data[i]
                                )
                            ).text();
                            data[i].actions[key].attr(k, text);
                            break;
                        case 'data-action':
                            subactions[key] = data[i].actions[key];
                            appendLabel = params[key][k];
                            break;
                        default:
                            data[i].actions[key].attr(
                                k,
                                TableGenerator._sprintf(
                                    params[key][k],
                                    params[key]['params'],
                                    data[i]
                                )
                            );
                    }
                }
                if (appendLabel) {
                    data[i].actions[key].append(' '+appendLabel);
                }
            }

            // placard à balais
            let ul = $('<ul class="dropdown-menu"></ul>');
            let hideSubactions = true;
            for (let k in subactions) {
                data[i].actions[k] = null;
                if (typeof data[i].actions.subactions === 'undefined') {
                    data[i].actions.subactions = $('<div class="subactions"></div>')
                        .append(
                            $('<a class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false" href="#" aria-label="actions" title="actions"></a>')
                                .append($('<i class="fa fa-ellipsis-v" aria-hidden="true"></i>'))
                        )
                        .append(ul);
                }
                data[i].actions[k] = null;
                ul.append($('<li></li>').append(subactions[k]));
                if (!$(subactions[k]).prop('disabled')) {
                    hideSubactions = false;
                }
            }
            if (subactions && data[i].actions.subactions && hideSubactions) {
                data[i].actions.subactions.hide();
            }
        }

        return data;
    }

    /**
     * Modifi une chaine de caractère en lui remplacant les {0}, {1} ...
     * par ce qui est défini dans params
     *
     * @param {string} str chaine à modifier
     * @param {Array} params array de chemin vers la donnée (dans data)
     * @param {object} data données brut
     * @returns {string}
     */
    static _sprintf(str, params, data)
    {
        for (let i = 0; i < params.length; i++) {
            try {
                let evaluated = TableGenerator._evalData(params[i], data);
                if (typeof evaluated === 'object') {
                    evaluated = JSON.stringify(evaluated);
                }
                evaluated = $('<div>').text(evaluated).html();
                str = str.replace(new RegExp('\\{'+i+'}', 'g'), evaluated);
            } catch (e) {
            }
        }
        return str;
    }

    /**
     * Echape une chaine tel addslashes de php
     *
     * @param {string} str
     * @returns {String}
     */
    static addslashesJson(str)
    {
        var a = {};
        a[str] = 1;
        return JSON.stringify(a).slice(2,-4);
    }

    /**
     * Génère l'entête du tableau
     *
     * @param {object} params
     * @returns {jQuery} Element thead
     */
    _generateThead(params = this._viewParams())
    {
        var thead = $('<thead></thead>'),
            tr,
            th,
            label;

        // Boucle sur les tr du thead
        for (let i in params.thead) {
            tr = $('<tr></tr>');

            if (typeof params.tbody.checkbox !== 'undefined' && params.tbody.checkbox) {
                tr.append(
                    $('<th class="table-checkbox"></th>')
                        .attr('scope', 'col')
                        .append(
                            $('<input>').attr('id', 'table-'+this.instanceIndex+'-checkall')
                                .addClass('checkall')
                                .attr('data-table-uid', this.instanceIndex)
                                .attr('onclick', 'TableHelper.checkall(this)')
                                .attr('type', 'checkbox')
                                .on('change.tableGenerator', function() {
                                    $(this).closest('table').find('> tbody > tr')
                                        .toggleClass('checked', $(this).prop('checked'));
                                })
                        )
                        .append('<label class="sr-only" for="table-'+this.instanceIndex+'-checkall">'+this.messages.selectAllCheckboxes+'</label>')
                );
            }
            if (typeof params.tbody.favorites !== 'undefined' && params.tbody.favorites) {
                let fav = params.tbody.favorites;
                tr.append(
                    $('<th class="table-favorite"></th>')
                        .attr('scope', 'col')
                        .append(fav === true ? '' : fav)
                );
            }

            // Boucle sur les th du head
            for (let j in params.thead[i]) {
                let label = params.thead[i][j].label;
                let sort;
                let filter;
                if (typeof label === 'object') {
                    sort = label.sort;
                    filter = label.filter;
                    label = label.label;
                }
                if (params.thead[i][j].filter) {
                    let name = [];
                    for (let key in params.thead[i][j].filter) {
                        $(params.thead[i][j].filter[key]).find('[name]').each(
                            function () {
                                name.push($(this).attr('name'));
                            }
                        );
                    }
                    try {
                        if (!$(label).is('span.field')) {
                            label = $('<span class="field">').append(label);
                        }
                    } catch (e) {
                        label = $('<span class="field">').append(label);
                    }

                    $(label).append(' ');
                    filter = $('<button type="button" class="btn btn-link btn-filter"></button>')
                        .attr('data-fields', name.join(';'))
                        .append($('<span class="sr-only"></span>').text(__("Filtrer")))
                        .append($('<i class="fa fa-filter" aria-hidden="true"></i>'))
                        .on('click.filter.table', this._filterClick(params.thead[i][j]));
                }

                th = $('<'+params.thead[i][j].type+'></'+params.thead[i][j].type+'>')
                    .toggle(params.thead[i][j].display)
                    .attr('scope', 'col')
                    .append(label)
                    .append(filter)
                    .append(sort);

                if (params.thead[i][j].colspan > 1) {
                    th.attr('colspan', params.thead[i][j].colspan);
                }
                if (params.thead[i][j]._id) {
                    th.attr('data-fieldname', params.thead[i][j]._id);
                }

                TableGenerator._addTdOptions(th, params.thead[i][j]);

                tr.append(th);
            }

            thead.append(tr);
        }

        return thead;
    };

    /**
     * Click sur le bouton filtrer
     * @private
     */
    _filterClick(params)
    {
        var that = this;
        return function () {
            var th = $(this).closest('th').css({position: 'relative'});
            var modal = th.closest('.modal');
            var target;
            var css;
            if (!modal.length) {
                modal = null;
                target = th;
            } else {
                target = modal;
            }
            if (target.closest('thead').find('.filter-popup').length) {
                target.closest('thead').find('.filter-popup').remove();
            } else {
                var form = $('<form class="filter-popup"></form>');
                let closeBtn = $('<button type="button" class="btn btn-link close">×</button>')
                    .attr('title', __("Fermer"))
                    .attr('aria-label', __("Fermer"))
                    .click(
                        function () {
                            $(this).closest('form').effect(
                                'drop',
                                {},
                                400,
                                function () {
                                    $(this).remove();
                                }
                            );
                        }
                    );
                form.append(closeBtn);

                for (let fieldname in params.filter) {
                    form.append(params.filter[fieldname]);
                }
                let submitIcon = $('<i class="fa fa-filter" aria-hidden="true"></i>');
                let submitSpan = $('<span> </span>').text(__("Filtrer"));
                let submitBtn = $('<button type="submit" class="btn btn-default btn-small"></button>')
                    .append(submitIcon)
                    .append(submitSpan)
                    .click(
                        function () {
                            $(this).closest('form').effect(
                                'drop',
                                {},
                                400,
                                function () {
                                    $(this).remove();
                                }
                            );
                        }
                    );
                form.append(submitBtn)
                target.prepend(form);

                if (modal) {
                    form.css({position: 'fixed'});
                    var offset = th.offset();
                    var top = offset.top - form.height() - 30;
                    var left = offset.left;
                    if (top + form.outerHeight() > $(window).height()) {
                        top = $(window).height() - form.outerHeight();
                    }
                    if (top < 0) {
                        top = 0;
                    }
                    if (left + form.outerWidth() > $(window).width()) {
                        left = $(window).width() - form.outerWidth();
                    }
                    if (left < 0) {
                        left = 0;
                    }
                    css = {
                        top: top,
                        left: left,
                        "z-index": 1100
                    };
                } else {
                    css = {
                        position: 'absolute',
                        bottom: 30,
                        'z-index': 1100
                    };
                }

                form.css(css);
                if (form.offset().top < 0 && css.bottom) {
                    css.bottom += form.offset().top;
                    form.css(css);
                }
                form.find('input, select, textarea').first().focus();

                $(that.table).trigger('created.form', {form: form, th: th});
            }
            if (params.filterCallback) {
                (eval('('+params.filterCallback+')')).call(this, params);
            }
        };
    }

    /**
     * Génère le corp du tableau
     *
     * @param {object} data
     * @param {object} params
     * @returns {jQuery} Element tbody
     */
    _generateTbody(data = this.data, params = this._viewParams())
    {
        var tr,
            tbody = $('<tbody></tbody>'),
            theadInfo = this._theadInfo(params);

        // On boucle sur chaque résultat (tr)
        this._ids = [];
        for (let i = 0; i < data.length; i++) {
            tr = this._generateTr(i, theadInfo, data, params);
            tbody.append(tr);
        }

        return tbody;
    };

    /**
     * Génère une ligne du tbody
     *
     * @param {number} index pour récupérer les données dans data
     * @param {object} theadInfo
     * @param {object} data
     * @param {object} params
     * @returns {jQuery} Element tr
     */
    _generateTr(index = 0, theadInfo = this._theadInfo(this._viewParams()), data = this.data, params = this._viewParams())
    {
        var cssClass,
            tr = $('<tr></tr>'),
            content,
            td,
            id = 'eval-failed-'+this.instanceIndex,
            favorite;

        try {
            cssClass = eval(params.tbody.classEval.replace(/{index}/g, index));
            tr.addClass(cssClass);
        } catch (e) {
        }
        try {
            id = TableGenerator._evalData(params.tbody.identifier, data[index]);
        } catch (e) {
        }
        var i = 0;
        var base_id = id;
        while (this._ids.indexOf(id) !== -1) {
            id = base_id + '_' + i;
            i++;
        }
        this._ids.push(id);

        // On boucle sur chaque colonne (td)
        for (let j = 0; j < theadInfo.length; j++) {
            try {
                content = TableGenerator._evalData(theadInfo[j].target, data[index]);
            } catch (e) {
                content = null;
            }
            if (typeof content === 'string'
                && theadInfo[j]
                && (typeof theadInfo[j].escape === 'undefined'
                    || theadInfo[j].escape === true)
            ) {
                content = $('<div>').text(content).html();
            }

            if ((typeof content === 'object' && !content instanceof jQuery)
                || theadInfo[j].thead !== null
            ) {
                content = this._parseContent(data[index], theadInfo[j]);
            }

            if (typeof theadInfo[j].callback !== 'undefined' && theadInfo[j].callback !== null) {
                if (typeof theadInfo[j].callback === 'string') {
                    theadInfo[j].callback = eval(theadInfo[j].callback);
                }
                if (typeof theadInfo[j].callback === 'function') {
                    content = theadInfo[j].callback(content, data[index]);
                }
            }

            td = $('<td></td>')
                    .addClass(TableGenerator._targetToClass(theadInfo[j].target))
                    .attr('data-fieldname', theadInfo[j]._id)
                    .toggle(theadInfo[j].display)
                    .append(content);

            if (theadInfo[j].colspan > 1) {
                td.attr('colspan', theadInfo[j].colspan);
            }

            if (typeof theadInfo[j].class !== 'undefined') {
                td.addClass(theadInfo[j].class);
            }

            td = TableGenerator._addTdOptions(td, theadInfo[j]);

            if (typeof theadInfo[j].titleEval !== 'undefined') {
                // Permet de parser les html entities (affichage correct du title après un h())
                let tmp = document.createElement('div');
                tmp.innerHTML = TableGenerator._evalData(theadInfo[j].titleEval, data[index]);
                td.attr('title', tmp.textContent);
            }

            tr.append(td);
        }
        tr.attr('id', 'table-'+this.instanceIndex+'-tr-'+id).attr('data-id', id);

        if (typeof params.tbody.favorites !== 'undefined' && params.tbody.favorites) {
            favorite = $('<button title="'+this.messages.favorite+'" aria-label="'+this.messages.favorite+'" type="button" class="btn-link"></button>')
                .append('<i aria-hidden="true" class="fa fa-star-o favorites-star"></i>');
            favorite.click(TableGenerator._favoriteHandler);
            TableGenerator._initFavorite(favorite, tr, this.table);
            tr.prepend(
                $('<td></td>')
                    .addClass('td-favorite')
                    .append(favorite)
            );
        }
        if (typeof params.tbody.checkbox !== 'undefined' && params.tbody.checkbox) {
            var input = $('<input>')
                .attr('id', 'table-'+this.instanceIndex+'-checkbox'+id)
                .attr('name', 'data[table]['+this.instanceIndex+'][]')
                .attr('type', 'checkbox')
                .addClass('table-'+this.instanceIndex+'-checkbox')
                .val(id)
                .on('change.tableGenerator', function() {
                    $(this).closest('tr').toggleClass('checked', $(this).prop('checked'));
                });
            var label = $('<label class="sr-only"></label>')
                .attr('for', 'table-'+this.instanceIndex+'-checkbox'+id)
                .append(this.messages.selectCheckbox+' '+id);
            var tdCheckbox = $('<td></td>').addClass('td-checkbox');
            if (params.tbody.checkbox === true || eval(params.tbody.checkbox.replace(/{index}/g, index), data)) {
                tdCheckbox.append(input).append(label);
            }
            tr.prepend(tdCheckbox);
        }

        this._trActiveCookie(tr);

        return tr;
    };

    /**
     * Permet d'obtenir le nom du cookie pour le favori d'une ligne d'un tableau
     * @param {jQuery} element
     * @param {jQuery} tr
     * @param {jQuery} table
     * @returns {string}
     */
    static _getFavoriteCookieName(element, tr, table)
    {
        return 'table-favorite-'+table.attr('id')+'-'+tr.attr('data-id');
    }

    /**
     * Action du clic sur l'etoile des favoris
     */
    static _favoriteHandler()
    {
        var fa = $(this).find('i.fa, i.fas');
        var generator = fa.closest('table').data('table-generator');
        var trId = fa.closest('tr').attr('data-id');
        generator._prevMemory = JSON.stringify(generator._memory);
        if (!Array.isArray(generator._memory.favorites)) {
            generator._memory.favorites = [];
        }

        fa.toggleClass('fa-star').toggleClass('fa-star-o');
        if (fa.hasClass('fa-star')) {
            generator._memory.favorites.push(trId)
        } else {
            var index = generator._memory.favorites?.indexOf(trId);
            if (index >= 0) {
                generator._memory.favorites.splice(index, 1);
            }
        }
        if (!generator._cookieLess) {
            var cookieName = TableGenerator._getFavoriteCookieName(
                fa,
                fa.closest('tr'),
                fa.closest('table')
            );
            if (fa.hasClass('fa-star')) {
                TableGenerator.cookie(
                    cookieName,
                    '1',
                    10 * 365 /* jours */
                );
            } else {
                TableGenerator.cookie(
                    cookieName,
                    '0',
                    1 /* jours */
                );
            }
        }

        if (JSON.stringify(generator._memory) !== generator._prevMemory) {
            generator.table.trigger('configuration_changed');
        }
    }

    /**
     * Affiche en favori si dans les cookies
     * @param {jQuery} element
     * @param {jQuery} tr
     * @param {jQuery} table
     */
    static _initFavorite(element, tr, table)
    {
        var cookieName = TableGenerator._getFavoriteCookieName(element, tr, table);
        if (table.data('table-generator')._memory.favorites?.indexOf(tr.attr('data-id')) >= 0) {
            element.find('i.fa, i.fas').addClass('fa-star').removeClass('fa-star-o');
        }
    }

    /**
     * Ajoute à un element jQuery les attributs données par options
     *
     * @param {jQuery} element
     * @param {object} options
     * @returns {jQuery}
     */
    static _addTdOptions(element, options)
    {
        var blacklist = [
            '_id',
            'label',
            'link',
            'type',
            'display',
            'target',
            'thead',
            'callback',
            'colspan',
            'titleEval',
            'style',
            'filter',
            'order'
        ];
        for (let key in options) {
            if (blacklist.indexOf(key) < 0) {
                element.attr(key, options[key]);
            }
        }

        // Merge les styles
        if (typeof options.style !== 'undefined' && options.style) {
            let style = element.attr('style') ? element.attr('style') : '';
            element.attr('style', style + options.style);
        }

        return element;
    }

    /**
     * Ajoute les sous elements de façon récursive
     *
     * @param {jQuery|Element} subElement
     * @param {object} info
     * @param {Array|object} data
     * @returns {jQuery|Element}
     */
    _subElements(subElement, info, data)
    {
        var subSubElement,
            field;

        if (Array.isArray(data)) {
            for (let i in data) {
                subSubElement = $('<div></div>').addClass('td-group-sub');
                subSubElement = this._subElements(subSubElement, info, data[i]);
                subElement.append(subSubElement);
            }
            return subElement;
        }

        for (let i in info.thead) {
            subSubElement = $('<div></div>').addClass('td-group-sub-sub');

            try {
                field = TableGenerator._evalData(info.thead[i].target, data);
            } catch (e) {
                field = undefined;
            }

            if (typeof field === 'object' && field !== null && !(field instanceof jQuery)) {
                field = this._parseContent(data, info.thead[i]);
            }

            if (typeof field === 'string'
                && info.thead[i]
                && (typeof info.thead[i].escape === 'undefined'
                    || info.thead[i].escape === true)
            ) {
                field = $('<div>').text(field).html();
            }

            if (typeof info.thead[i].callback !== 'undefined' && info.thead[i].callback !== null) {
                if (typeof info.thead[i].callback === 'string') {
                    info.thead[i].callback = eval(info.thead[i].callback);
                }
                if (typeof info.thead[i].callback === 'function') {
                    field = info.thead[i].callback(field, data);
                }
            }

            if (!field) {
                continue;
            }

            var display = true;
            if (info.thead[i].displayEval) {
                try {
                    display = TableGenerator._evalData(info.thead[i].displayEval, data);
                } catch (e) {
                    display = true;
                }
            }
            if (info.thead[i].display === false
                || info.thead[i].display === 'none'
                || !display
            ) {
                subSubElement.hide();
            }
            if (info.thead[i].class) {
                subSubElement.addClass(info.thead[i].class);
            }

            if (info.thead[i].label) {
                let label = info.thead[i].label;
                if (label.label) {
                    label = $(label.label).text().trim();
                }
                subSubElement.append(
                    $('<span></span>').addClass('td-group-fieldname')
                        .append(label)
                );
            }

            subSubElement.append(
                $('<div></div>')
                .addClass('td-group-fieldvalue')
                .append(field)
            );

            subElement.append(subSubElement);
        }

        return subElement;
    }

    /**
     * Permet de créer le contenu d'une cellule de façon récursive
     *
     * @param {object} data
     * @param {object} info theadInfo
     * @returns {jQuery} Element (div du td)
     */
    _parseContent(data, info)
    {
        var element = $('<div></div>').addClass('td-group').attr('tabindex', -1);

        try {
            element = this._subElements(
                element,
                info,
                !info.target ? data : TableGenerator._evalData(info.target, data)
            );
        } catch (e) {
        }

        return element;
    }

    /**
     * Fait une evaluation compatible avec la syntaxe suivante :
     * data['ma-var'] comme pour data.maVar
     *
     * @param {string} str
     * @param {object} data
     * @returns {string|number|Array|object|null|undefined|bool} résultat de l'eval
     */
    static _evalData(str, data)
    {
        str = str.split(".").join('"]["');
        return eval(str.indexOf('[') === 0 ? 'data'+str : 'data["'+str+'"]');
    }

    /**
     * Simplifie le thead pour construire le tbody
     *
     * @param {object} params
     * @returns {array}
     */
    _theadInfo(params = this._viewParams())
    {
        var thead = [];

        // Boucle sur les tr du thead
        for (let i in params.thead) {
            // Boucle sur les th du head
            for (let j in params.thead[i]) {
                thead.push(params.thead[i][j]);
            }
        }

        return thead;
    };

    /**
     * Transforme une cible en nom de classe css
     * ex: Monmodel[0].field -> Monmodel-0-field
     *
     * @param {string} target
     * @returns {string|null}
     */
    static _targetToClass(target)
    {
        if (typeof target === 'undefined' || !target) {
            return null;
        }
        return target.replace('.', '-').replace('[', '-').replace(']', '');
    }

    /**
     * Lors du clic sur une ligne, elle gagne/perd la classe active.
     * Cette information est conservé en cookie
     *
     * @param {jQuery|Element} tr
     * @returns {TableGenerator}
     */
    _trActiveCookie(tr)
    {
        var that = this;
        $(tr).click(
            function () {
                if ($(this).hasClass('do-not-change')) {
                    $(this).removeClass('do-not-change');
                    return;
                }
                that._prevMemory = JSON.stringify(that._memory);
                if ($(this).hasClass('active')) {
                    that._memory.activeLine = null;
                    if (!that._cookieLess) {
                        TableGenerator.removeCookie(that.cookies.active);
                    }
                    $(this).parent().find('tr').removeClass('active');
                } else {
                    $(this).parent().find('tr').removeClass('active');
                    $(this).addClass('active');
                    that._memory.activeLine = $(tr).attr('data-id');
                    if (!that._cookieLess) {
                        TableGenerator.cookie(
                            that.cookies.active,
                            'table#'+$(tr).parent().parent().attr('id')+' tr[data-id="'+$(tr).attr('data-id')+'"]'
                        );
                    }
                }
                if (JSON.stringify(that._memory) !== that._prevMemory) {
                    that.table.trigger('configuration_changed');
                }
            }
        );
        $(tr).find('a, button.btn-link').click(
            function (event) {
                that._prevMemory = JSON.stringify(that._memory);
                $(tr).closest('table').find('tbody tr').removeClass('active');
                $(tr).addClass('active').addClass('do-not-change');
                setTimeout(
                    function () {
                        $(tr).removeClass('do-not-change');
                    },
                    100
                );
                that._memory.activeLine = $(tr).attr('data-id');
                if (!that._cookieLess) {
                    TableGenerator.cookie(
                        that.cookies.active,
                        'table#' + $(tr).parent().parent().attr('id') + ' tr[data-id="' + $(tr).attr('data-id') + '"]'
                    );
                }
                if (JSON.stringify(that._memory) !== that._prevMemory) {
                    that.table.trigger('configuration_changed');
                }
            }
        );
        return this;
    }

    /**
     * Ajoute la classe active sur une ligne si elle est désignée active en cookie
     * @returns {TableGenerator}
     */
    _toggleClassActiveCookie()
    {
        if (!this._memory.activeLine) {
            return;
        }
        var active = this.table.find('tr[data-id="'+this._memory.activeLine+'"]').first();
        $(active).addClass('active');
        return this;
    }

    /**
     * Désactive la gestion par cookies
     */
    setCookieLess()
    {
        this._cookieLess = true;
    }

    /**
     * Charge le data des cookies
     * @param data
     */
    static loadCookies(data)
    {
        TableGenerator.cookies = data;
    }

    /**
     * Getter/setter de cookies
     * Si disponnible, utilisera les cookies en mémoire dans TableGenerator.cookies
     * @param key
     * @param value
     * @param options
     */
    static cookie(key, value, options = {path: "/", SameSite: "Strict"})
    {
        if (value !== undefined && typeof TableGenerator.cookies[key] === 'undefined') {
            TableGenerator.cookies[key] = Cookies.get(key, options);
        }
        if (value === undefined) {
            return TableGenerator.cookies[key];
        } else {
            TableGenerator.cookies[key] = value;
            return Cookies.set(key, value, options);
        }
    }

    /**
     * Supprime un cookie
     * @param key
     * @param options
     * @return {boolean|*}
     */
    static removeCookie(key, options = {path: "/", SameSite: "Strict"})
    {
        if (TableGenerator.cookies[key]) {
            delete TableGenerator.cookies[key];
        }
        Cookies.set(key, 'null', options);
        return true;
    }

    /**
     * Incrémente le paginateur
     */
    incrementPaginator(paginator = null)
    {
        if (!paginator) {
            paginator = this.table.siblings().find('.pagination-counters');
        }
        var min = paginator.find('.start');
        var max = paginator.find('.end');
        var count = paginator.find('.count');
        min.html(Math.max(1, parseInt(min.html(), 10)));
        max.html(parseInt(max.html(), 10) +1);
        count.html(parseInt(count.html(), 10) +1);
    }

    /**
     * Décrémente le paginateur
     */
    decrementPaginator(paginator = null)
    {
        if (!paginator) {
            paginator = this.table.siblings().find('.pagination-counters');
        }
        var start = paginator.find('.start');
        var max = paginator.find('.end');
        var count = paginator.find('.count');
        count.html(Math.max(0, parseInt(count.html(), 10) -1));
        start.html(Math.min(parseInt(start.html(), 10), parseInt(count.html(), 10)));
        max.html(Math.max(0, parseInt(max.html(), 10) -1));
    }

    /**
     * Charge les données de l'utilisateurs
     * @param data
     */
    loadMemory(data)
    {
        this._memory = {
            view: data.view,
            activeLine: data.activeLine,
            favorites: data.favorites,
            max_per_page: data.max_per_page,
            columns: data.columns
        };

        if (!data.columns || data.columns.length === 0) {
            this.params(this._rawParams);
            return;
        }

        this._loadMemoryColumns();
    }

    /**
     * Partie colonnes configurés des données utilisateurs
     * @private
     */
    _loadMemoryColumns()
    {
        var params = this._viewParams();
        for (let tr in params.thead) {
            let thead = [];
            let activeThead = this.view !== 'div'
                ? params.thead[tr]
                : params.thead[tr][0].thead;

            // On ajoute en premier dans l'ordre mémorisé les colonnes choisies
            for (let i = 0; i < this._memory.columns.length; i++) {
                for (let th in activeThead) {
                    if (activeThead[th]['_id'] === this._memory.columns[i]) {
                        activeThead[th].display = true;
                        thead.push(activeThead[th]);
                        break;
                    }
                }
            }

            // On ajoute ensuite en champs cachés les elements restant
            for (let th in activeThead) {
                if (this._memory.columns.indexOf(activeThead[th]['_id']) === -1) {
                    activeThead[th].display = false;
                    thead.push(activeThead[th]);
                }
            }

            // on fini par les actions si elles existent
            for (let th in activeThead) {
                if (activeThead[th]['_id'] === 'actions') {
                    for (let i = 0; i < thead.length; i++) {
                        if (thead[i]['_id'] === 'actions') {
                            thead.splice(i, 1);
                            break;
                        }
                    }
                    activeThead[th].display = true;
                    thead.push(activeThead[th]);
                    break;
                }
            }

            if (this.view !== 'div') {
                this._params.thead[this.view][tr] = thead;
            } else {
                this._params.thead.div[tr][0].thead = thead;
            }
        }
    }

    exportMemory()
    {
        return this._memory;
    }
}
TableGenerator.instance = [];
TableGenerator.cookies = typeof PHP !== 'undefined' && PHP.cookies ? PHP.cookies : {};

/**
 * Aide à la construction d'un object adapté pour l'entête de TableGenerator
 *
 * @param {string} label
 * @param {object} params
 * @returns {TableHead}
 */
class TableHead {
    constructor(label, params)
    {
        this._id = label.replace(/[\W]/g, '_');
        this.label = label;

        for (let key in params) {
            this[key] = params[key];
        }

        this.title   = typeof this.title === 'undefined' ? '' : this.title;
        this.link    = typeof this.link === 'undefined' ? false : this.link;
        this.type    = typeof this.type === 'undefined' ? 'th' : this.type;
        this.display = typeof this.display === 'undefined' ? true : this.display;
        this.displayEval = typeof this.display === 'undefined' ? true : this.displayEval;
        this.colspan = typeof this.colspan === 'undefined' ? 1 : this.colspan;
        this.target  = typeof this.target === 'undefined' ? label : this.target;
        this.thead   = typeof this.thead === 'undefined' ? null : this.thead;
        this['data-view'] =
            typeof this['data-view'] === 'undefined' ? 'default' : this['data-view'];
        this.filter   = typeof this.filter === 'undefined' ? null : this.filter;
    }
}

/**
 * Ensemble de callbacks pour formater les champs pour le TableGenerator
 */
class TableHelper {
    /**
     * Permet de modifier la locale de la lib momentjs
     *
     * @param {string} str
     */
    static locale(str)
    {
        moment.locale(str);
    }

    /**
     * Permet de formater une date selon "format"
     *
     * @param {string} format ex: 'Y-M-D h:m:s'
     * @returns {Function}
     */
    static date(format = 'Y-M-D')
    {
        return function (value) {
            if (typeof value === 'undefined' || !value) {
                return null;
            }

            return moment(new Date(value)).format(format);
        };
    }

    /**
     * Transforme une valeur boolean en mot
     * @param {string} yes
     * @param {string} no
     * @returns {Function}
     */
    static boolean(yes = 'yes', no = 'no')
    {
        return function (value) {
            if (typeof value === 'undefined' || value === null) {
                return null;
            }

            return value ? yes : no;
        };
    }

    /**
     * Conversion de bytes
     *
     * @param {string} bytes
     * @returns {String}
     */
    static readableBytes(bytes)
    {
        if (!AsalaeGlobal.is_numeric(bytes)) {
            return bytes;
        }
        var s = ['bytes', 'kB', 'MB', 'GB', 'TB', 'PB'],
            e = Math.floor(Math.log(parseFloat(bytes)) / Math.log(1024));
        return Number.isFinite(e)
            ? (bytes / Math.pow(1024, e)).toFixed(2) + " " + s[e]
            : bytes;
    }

    /**
     * Permet de cocher/décocher toutes les cases d'un tableau
     * @param {Element} element
     */
    static checkall(element)
    {
        $('table[data-table-uid="'+$(element).attr('data-table-uid')+'"]')
            .find('tbody .td-checkbox input')
            .prop('checked', $(element).prop('checked'));
    }

    /**
     * Ajoute des balises <wbr> après chaques underscore.
     * Permet le retour à la ligne des très long nom qui ont des underscores
     * à la place des espaces
     * @param {string} char
     * @param {int} maxlength
     * @return {void | string | *}
     */
    static wordBreak(char = '_', maxlength = 40)
    {
        return function (value) {
            if (typeof value === 'undefined' || value === null) {
                return null;
            }
            var regex = new RegExp(char, 'g');
            var m;
            var i = 0;
            value = value.replace(regex, char+'<wbr/>');
            while ((m = value.match(new RegExp('([^ <>'+char+']{'+(maxlength+1)+'})[^ <>'+char+']'))) && i < 1000) {
                if (m[1].length === (maxlength +1)) {
                    var str = m[1].substr(0, maxlength);
                    value = value.replace(m[1], str+'<wbr/>'+m[1].substr(maxlength))
                } else {
                    value = value.replace(m[1], m[1]+'<wbr/>');
                }
                i++;
            }
            return value;
        }
    }

    /**
     * Transforme un array
     * @param {Array} value
     * @returns {Function}
     */
    static array(value)
    {
        if (typeof value === 'string') {
            try {
                value = JSON.parse(value);
            } catch (e) {
                return null;
            }
        }
        if (typeof value !== 'object' || !Array.isArray(value)) {
            return null;
        }
        var ul = $('<ul></ul>');
        for (let i = 0; i < value.length; i++) {
            ul.append($('<li></li>').append(value[i]));
        }
        return ul;
    }

    /**
     * Donne un ul de field
     */
    static ul(field)
    {
        return function (value, context) {
            if (Array.isArray(value)) {
                let ul = $('<ul></ul>');
                for (let i = 0; i < value.length; i++) {
                    if (value[i][field]) {
                        ul.append($('<li></li>').text($('<i>').html(value[i][field]).text()));
                    }
                }
                return ul;
            }
            return null;
        }
    }

    /**
     * Donne une icone à partir d'un nom de fichier
     * @param {string}       field - champ du nom de fichier
     * @param {string|false} directory - champ qui défini si le filename est un dossier
     * @returns {function(*, *): (*|jQuery|HTMLElement|Mixed|Window.jQuery)}
     */
    static filenameIcon(field = 'filename', directory = false)
    {
        return function (value, context)
        {
            var div = $('<div class="prev-ext"></div>');
            var icon = $('<i class="fa" aria-hidden="true"></i>');
            var span = $('<span></span>');
            div.append(icon).append(span);
            if (context.directory) {
                icon.addClass('fa-folder-o');
                return div;
            }
            var ext = context.filename.substr(context.filename.lastIndexOf('.') +1);
            if (ext.length > 4) {
                icon.addClass('fa-file-o');
                return div;
            }
            switch (ext.toLowerCase()) {
                case 'zip':
                case 'gz':
                case 'tar':
                    icon.addClass(' fa-file-archive-o');
                    break;
                case 'wav':
                case 'mp3':
                case 'aac':
                    icon.addClass(' fa-file-audio-o');
                    break;
                case 'jpg':
                case 'png':
                    icon.addClass(' fa-file-image-o');
                    break;
                case 'avi':
                case 'mp4':
                case 'mov':
                case 'wmv':
                case 'mkv':
                    icon.addClass(' fa-file-movie-o');
                    break;
                case 'pdf':
                    icon.addClass('fa-file-pdf-o');
                    break;
                case 'tsr':
                    icon.addClass('fa-file-o');
                    span.addClass('fa').addClass('fa-clock-o');
                    break;
                default:
                    icon.addClass('fa-file-o');
                    span.append(ext);
            }
            return div;
        }
    }

    /**
     * Popup d'affichage des détails des fichiers
     * @param filename
     * @param context
     * @returns {*|jQuery|HTMLElement|Mixed|jQuery}
     */
    static filenameToPopup(filename, context)
    {
        var uid = "filename-popup-"+(Date.now() * 10000 + Math.random() * 10000).toString(16);
        var div = $('<div>').attr('id', uid);
        var popup = new AsalaePopup();
        $('body').append(popup.element());
        popup.element()
            .css({
                display: "none",
                "max-width": 600,
                "max-height": 400,
                "z-index": 10000,
                overflow: "auto"
            })
            .attr('data-depand-id', uid)
            .append($('<div>').text(TableHelper.wordBreak()(context['mime'])))
            .append($('<div>').text(TableHelper.readableBytes(context['size'])));
        div.html(TableHelper.wordBreak()($('<div>').html(context['name']).text()));

        $(document).on(
            'change.'+uid,
            function () {
                if ($('#'+ uid).length === 0) {
                    $('form[data-depand-id="'+uid+'"]').remove();
                    $(document).off('change.'+uid);
                }
            }
        );
        div.on('mouseover mousemove', function(e) {
            popup.element()
                .css({
                    position: 'fixed',
                    top: e.originalEvent.clientY + 10,
                    left: e.originalEvent.clientX + 10
                })
                .show();
            if (!popup.element().is(':visible')) {
                popup.show();
            }
        });
        div.on('mouseleave', function() {
            if (popup.element().is(':visible')) {
                popup.element().hide();
            }
        });
        return div;
    }
}

/**
 * Callback générique lorsqu'on a ajouté un enregistrement, modifier ou supprimer
 */
class TableGenericAction
{
    /**
     * Permet de trouver le nom du modèle
     * @param {TableGenerator} table
     * @return {string}
     */
    static getModelName(table)
    {
        var model = $(table.table).attr('data-for'),
            match
        ;
        if (match = model.match(/^Generated(.*)Table$/)) {
            model = match[1];
        }
        return model;
    }

    /**
     * Callback générique afterAdd
     * @param {TableGenerator} table
     * @param {string|null} model   est défini automatiquement à partir du nom
     *                              d'objet javascript si null
     * @return {Function}
     */
    static afterAdd(table, model = null)
    {
        if (model === null) {
            model = TableGenericAction.getModelName(table);
        }
        return function (content, textStatus, jqXHR) {
            var step = jqXHR.getResponseHeader('X-Asalae-Step');
            if (jqXHR.getResponseHeader('X-Asalae-Success') === 'true'
                && step
                && step.match(/^[\w\-.'" (),\[\]]+$/)
            ) {
                eval(step);
            } else if (jqXHR.getResponseHeader('X-Asalae-Success') === 'true') {
                table.data.push($.extend(true, {[model]: content}, content));
                TableGenerator.appendActions(table.data, table.actions);
                table.generateAll();
                table.incrementPaginator();
                var idField;
                if (table && table._params && table._params.tbody) {
                    idField = table._params.tbody.identifier;
                } else {
                    idField = 'id';
                }
                var tr = table.table.find('tr[data-id="'+content[idField]+'"]').first();
                if (tr.length) {
                    $(table.table).trigger('created.tablejs', tr);
                }
            } else {
                AsalaeGlobal.colorTabsInError();
            }
        }
    }

    /**
     * Callback générique afterEdit
     * @param {TableGenerator} table
     * @param {string|null} model   est défini automatiquement à partir du nom
     *                              d'objet javascript si null
     * @return {Function}
     */
    static afterEdit(table, model = null)
    {
        if (model === null) {
            model = TableGenericAction.getModelName(table);
        }
        return function (content, textStatus, jqXHR) {
            if (jqXHR.getResponseHeader('X-Asalae-Success') === 'true') {
                var data = table.getDataId(content.id);
                if (!data) {
                    console.warn('Unable to edit: content.id was not found');
                    return;
                }
                table.replaceDataId(content.id, $.extend(true, {[model]: content}, content));
                TableGenerator.appendActions(table.data, table.actions);
                table.generateAll();
                var idField;
                if (table && table._params && table._params.tbody) {
                    idField = table._params.tbody.identifier;
                } else {
                    idField = 'id';
                }
                var tr = table.table.find('tr[data-id="'+content[idField]+'"]').first();
                if (tr.length) {
                    $(table.table).trigger('modified.tablejs', tr);
                }
            } else {
                AsalaeGlobal.colorTabsInError();
            }
        }
    }

    /**
     * Action générique du suppression
     * @param table
     * @param url
     * @return {Function}
     */
    static deleteAction(table, url)
    {
        return function (id) {
            var useConfirm = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : true;
            if (useConfirm && !confirm(__("Êtes-vous sûr de vouloir supprimer cet enregistrement ?"))) {
                return;
            }
            var tr = $(table.table).find('tr[data-id="'+id+'"]');
            tr.find('button').addClass('deleting').addClass('gray').addClass('disabled').disable();
            $('html').addClass('ajax-loading');
            $(table.table).trigger('deleting.tablejs', tr);
            $.ajax(
                {
                    url: url.replace(/\/$/, '') + '/' + id,
                    method: 'DELETE',
                    headers: {
                        Accept: 'application/json'
                    },
                    success: function (message) {
                        if (message && message.report === 'done') {
                            table.removeDataId(id);
                            tr.fadeOut(
                                400,
                                function () {
                                    $(this).remove();
                                    $(table.table).trigger('deleted.tablejs', tr);
                                    table.decrementPaginator();
                                }
                            );
                        } else if (message && message.report) {
                            return this.error(message.report);
                        } else {
                            this.error(message);
                        }
                    },
                    error: function (e) {
                        if (typeof AsalaeGlobal === 'object') {
                            return AsalaeGlobal.ajaxError(e);
                        }
                        tr.addClass('danger');
                    },
                    complete: function () {
                        $('html').removeClass('ajax-loading');
                    }
                }
            );
        }
    }
}

if (typeof module !== 'undefined') {
    module.exports = {
        TableGenerator: TableGenerator,
        TableGenericAction: TableGenericAction,
        TableHead: TableHead,
        TableHelper: TableHelper
    };
}
