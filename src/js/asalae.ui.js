/**
 * Même principe que draggable de jquery ui, mais fonctionne sur éléments
 * imbriqués
 * @param options
 * @returns {$}
 */
$.fn.draggableCustom = function (options = {}) {
    if (typeof options !== 'object') {
        options = {};
    }
    options.handle = typeof options.handle !== 'string'
        ? '.handle'
        : options.handle
    ;
    this.each(
        function () {
            var container = $(this);
            $(this).find(options.handle).each(
                function () {
                    var handle = $(this);
                    handle.css('cursor', 'move');
                    handle.on(
                        'mousedown.draggableCustom',
                        function (event) {
                            container.addClass('grabbing')
                            .data('pageX', event.pageX)
                            .data('pageY', event.pageY);
                        }
                    );
                    $(window).on(
                        'mousemove.draggableCustom',
                        function (event) {
                            if (container.hasClass('grabbing')) {
                                var diffX = container.data('pageX') - event.pageX,
                                diffY = container.data('pageY') - event.pageY,
                                left = parseInt(container.css('left'), 10) - diffX,
                                top = parseInt(container.css('top'), 10) - diffY
                                ;
                                container.css(
                                    {
                                        left: parseInt(container.css('left'), 10) - diffX,
                                        top: parseInt(container.css('top'), 10) - diffY,
                                        right: (parseInt(container.css('left'), 10) - diffX) * -1,
                                        bottom: (parseInt(container.css('top'), 10) - diffY) * -1
                                    }
                                )
                                .data('pageX', event.pageX)
                                .data('pageY', event.pageY);
                            // Assure le non dépassement du cadre du body
                                let position = container.offset(),
                                parentPosition = container.parent().offset(),
                                x1 = position.left - parentPosition.left,
                                x2 = x1 + container.actual('outerWidth'),
                                y1 = position.top - parentPosition.top,
                                y2 = y1 + container.actual('outerHeight'),
                                body = $('body'),
                                bodyX = body.actual('outerWidth'),
                                bodyY = body.actual('outerHeight');
                                if (x1 > bodyX) {
                                    container.css(
                                        {
                                            left: '-='+(x2 - bodyX),
                                            right: '+='+(x2 - bodyX)
                                        }
                                    );
                                }
                                if (x2 < 0) {
                                    container.css(
                                        {
                                            left: '-='+x1,
                                            right: '+='+x1
                                        }
                                    );
                                }
                                if (y1 > bodyY) {
                                    container.css(
                                        {
                                            top: '-='+(y2 - bodyY),
                                            bottom: '+='+(y2 - bodyY)
                                        }
                                    );
                                }
                                if (y2 < 0) {
                                    container.css(
                                        {
                                            top: '-='+y1,
                                            bottom: '+='+y1
                                        }
                                    );
                                }

                            // Retire les sélections potentiellement occasionnées par
                            // le déplacement de la modale
                                document.getSelection().removeAllRanges();
                            }
                        }
                    );
                    $(window).on('mouseup.draggableCustom', AsalaeGlobal.grabHandlers.mouseup);
                }
            );
        }
    );
    return this;
};