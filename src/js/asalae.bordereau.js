/**
 * Script de chargement de la page
 */
$(document).ready(
    function () {
        $('.Organisation')
            .removeClass('collapsed')
            .addClass('expanded');

        $('fieldset > legend').each(
            function () {
                $(this).closest('fieldset').addClass('expanded');
                var icon = 'fa fa-minus-square-o';
                if ($(this).closest('fieldset').hasClass('folder')) {
                    icon = 'fa fa-folder-open';
                } else if ($(this).closest('fieldset').hasClass('Contains')) {
                    icon = 'fa fa-folder-open';
                } else if ($(this).closest('fieldset').hasClass('Document_v10')) {
                    icon = 'fa fa-file-o';
                    collapse(this, 'plier');
                } else if ($(this).closest('fieldset').hasClass('RelatedData')) {
                    icon = 'fa fa-paperclip';
                } else if ($(this).closest('fieldset').hasClass('entete')) {
                    icon = 'fa fa-window-maximize';
                } else if ($(this).closest('fieldset').hasClass('tag')) {
                    icon = 'fa fa-tags';
                } else if ($(this).closest('fieldset').hasClass('liste_integrity')) {
                    icon = 'fa fa-tags';
                } else if ($(this).closest('fieldset').hasClass('Appraisal')) {
                    icon = 'fa fa-calendar';
                } else if ($(this).closest('fieldset').hasClass('rules')) {
                    icon = 'fa fa-gavel';
                    collapse(this, 'plier');
                } else if ($(this).closest('fieldset').hasClass('Keyword_v10')) {
                    icon = 'fa fa-flag-o';
                    collapse(this, 'plier');
                } else if ($(this).closest('fieldset').hasClass('metadonnees_descriptives')) {
                    icon = 'fa fa-list-ul';
                } else if ($(this).closest('fieldset').hasClass('descriptionClass')) {
                    icon = 'fa fa-list-ul';
                    collapse(this, 'plier');
                } else if ($(this).parent('fieldset').hasClass('ContentDescriptive')) {
                    icon = 'fa fa-flag';
                    collapse(this, 'plier');
                } else if ($(this).parent('fieldset').hasClass('TransferringAgency')) {
                    icon = 'fa fa-sitemap';
                } else if ($(this).parent('fieldset').hasClass('ArchivalAgency')) {
                    icon = 'fa fa-sitemap';
                } else if ($(this).parent('fieldset').hasClass('OriginatingAgency')) {
                    icon = 'fa fa-sitemap';
                } else if ($(this).parent('fieldset').hasClass('Organisation')) {
                    icon = 'fa fa-sitemap';
                    if (!$(this).parents('div').hasClass('acteur')) {
                        collapse(this, 'plier');
                    }
                } else if ($(this).closest('fieldset').hasClass('Address')) {
                    icon = 'fa fa-map-marker';
                    collapse(this, 'plier');
                } else if ($(this).closest('fieldset').hasClass('Contact')) {
                    icon = 'fa fa-envelope-o';
                    collapse(this, 'plier');
                } else if ($(this).closest('fieldset').hasClass('ContentDescription')) {
                    icon = 'fa fa-info';
                    if ($(this).parents('fieldset').hasClass('ArchiveObject')) {
                        collapse(this, 'plier');
                    }
                } else if ($(this).closest('fieldset').hasClass('Communication')) {
                    icon = 'fa fa-bullhorn';
                    collapse(this, 'plier');
                } else if ($(this).closest('fieldset').hasClass('CustodialHistory')) {
                    icon = 'fa fa-clock-o';
                } else if ($(this).closest('fieldset').hasClass('RelatedObjectReference')) {
                    icon = 'fa fa-link';
                } else if ($(this).closest('fieldset').hasClass('AccessRestriction')) {
                    icon = 'fa fa-unlock';
                } else if ($(this).closest('fieldset').hasClass('Integrity')) {
                    icon = 'fa fa-certificate';
                    collapse(this, 'plier');
                    $(this).attr('title', 'cliquer pour replier');
                    if ($(this).next().text() === '') {
                        $(this).next().hide();
                    }
                } else {
                    //collapse(this, 'plier');
                    //$(this).css('color', 'red');
                }
                $(this).prepend($('<i class="' + icon + ' icone-plier"></i>'));
            }
        ).on(
            'click',
            function () {
                collapse(this);
            }
        );

    //Lien footer (SEDA)
        $('#footer').find('a').on(
            'click',
            function () {
                window.open(this.href);
                return false;
            }
        );
    }
);
