/* global jQuery, $ */

if (typeof __ === 'undefined') {
    var __ = function (str) {
        return str;
    };
}

/**
 * Notifications
 */
class AsalaeNotifications
{
    constructor(config)
    {
        this.session = null;
        this.data = null;
        this.count = 0;
        this.user_id = 0;
        this.token = config.token;
        this.element = config.element;
        this.elementUl = config.elementUl;
        this.favicon = new Favico(
            {
                type : 'rectangle',
                animation:'none',
                bgColor : '#f05922',
                fontFamily : 'Arial'
            }
        );
        this.ajax = {
            deleteNotification: config.ajax.deleteNotification
        };
        // Arrete l'animation de la cloche
        $('#container-number-notification').hover(
            function () {
                $(this).find('i.fa-bell').removeClass('faa-shake');
            }
        ).click(
            function (event) {
                $(config.element).children().each(
                    function () {
                        $(this).stop(true, true).slideUp(
                            400,
                            function () {
                                $(this).remove();
                            }
                        );
                    }
                )
            }
        );
    }

    connect(user_id, url)
    {
        var that = this;
        try {
            AsalaeWebsocket.connect(
                url,
                function (session) {
                    $('#container-number-notification')
                        .removeClass('danger')
                        .attr('title', "");
                    that.session = session;
                    that.user_id = user_id;
                    session.subscribe(
                        'user_'+user_id+'_'+that.token,
                        function (topic, data) {
                            that.updateCount(1);
                            if (that.count <= 20) {
                                that.append(data.message);
                            }
                            if (that.count === 20) {
                                alert("L'affichage des notifications a été désactivé afin de préserver les performances de votre navigateur. Afin de voir les notifications suivantes, il vous faudra lire les notifications actuelles et recharger la page.")
                            }
                        }
                    );
                    session.subscribe(
                        'close_user_'+user_id+'_'+that.token,
                        function (topic, data) {
                            if (data.target === undefined) {
                                data = data.message;
                            }
                            $(data.target+'.alert-dismissible').stop(true, true).slideToggle(
                                400,
                                function () {
                                    $(data.target).remove();
                                }
                            );
                            var count = $(data.targetLi+'.navbar-link').stop(true, true).slideToggle(
                                400,
                                function () {
                                    $(data.targetLi).remove();
                                }
                            ).length;
                            that.updateCount(-count);
                        }
                    );
                    session.subscribe(
                        'ping',
                        function (topic, data) {
                            session.publish('pong', {
                                category: 'pong',
                                message: user_id
                            });
                        }
                    );
                    $(that.element).trigger('connected.autobahn');
                },
                function () {
                    $('#container-number-notification')
                        .addClass('danger')
                        .attr('title', __("Déconnecté"));
                },
                {'skipSubprotocolCheck': true}
            );
        } catch (e) {
            console.error(e);
        }
        return this;
    }

    updateCount(amount)
    {
        var containerNumberNotification = $('#container-number-notification'),
            notificationBell = containerNumberNotification.find('i.fa-bell'),
            numberNotification = $('#number-notification');
        if (amount === 0 || (this.count === 0 && amount < 0)) {
            notificationBell.removeClass("faa-shake");
            return;
        } else if (amount > 0 && !containerNumberNotification.hasClass('active')) {
            notificationBell.addClass("faa-shake");
        }
        this.count += amount;
        this.favicon.badge(this.count);
        numberNotification.text(this.count).addClass("faa-horizontal");
        setTimeout(
            function () {
                numberNotification.removeClass('faa-horizontal');
            },
            1000
        );

        return this;
    }

    insert(data, deleteByAjax = true)
    {
        $(this.element).empty();

        this.updateCount(data.length);

        for (var key in data) {
            this.append(data[key], deleteByAjax);
        }

        return this;
    }

    static filterExecutable(element)
    {
        $(element).each(
            function () {
                if ($(this).prop("tagName") === 'SCRIPT') {
                    $(this).remove();
                    return;
                }
                var attrs = $(this).attr();
                for (var key in attrs) {
                    if (key.substr(0, 2) === 'on' || attrs[key].indexOf('javascript:') !== -1) {
                        $(this).removeAttr(key);
                    }
                }
                var children = $(this).children();
                for (var i = 0; i < children.length; i++) {
                    AsalaeNotifications.filterExecutable(children[i]);
                }
            }
        );
        return element;
    }

    append(data, deleteByAjax = true)
    {
        var div, button, that = this, action, li, timer, timeMessage, elapsed;
        var message = AsalaeNotifications.filterExecutable($('<div></div>').append(data.text));
        var created = typeof data.created === 'object' ? data.created.date : data.created;
        elapsed = Math.round(Math.abs(new Date(created) - new Date()) / 1000);
        if (elapsed < 60) {
            timeMessage = __("à l'instant");
        } else if (elapsed < 3600) {
            timeMessage = __("depuis {0} minute(s)", Math.round(elapsed / 60));
        } else if (elapsed < 86400) {
            timeMessage = __("depuis {0} heure(s)", Math.floor(elapsed / 3600));
        } else if (elapsed >= 86400) {
            timeMessage = __("depuis {0} jour(s)", Math.floor(elapsed / 86400));
        }

        div = $('<div></div>')
            .addClass('alert')
            .addClass('alert-dismissible')
            .addClass('alert-notification')
            .addClass(data.css_class)
            .attr('id', 'notification-'+data.id)
            .attr('role', 'alert');

        button = $('<button></button>')
            .addClass('close')
            .attr('type', 'button')
            .attr('aria-label', 'Close')
            .append($('<span></span>').append('&times;'));

        timer = $('<span>')
            .addClass('notification-timer')
            .attr('data-created', created)
            .text(timeMessage);

        div.append(button)
            .append(message.html())
            .append(timer.clone());

        message.append(timer);

        li = $('<li></li>')
            .addClass('navbar-link')
            .addClass('alert')
            .addClass('notification')
            .addClass('alert-notification')
            .addClass(data.css_class)
            .attr('id', 'notification-li-'+data.id)
            .append(message);

        action = function (event) {
            if (event.target.tagName === 'A') {
                return;
            }
            event.stopPropagation();
            // Evite les clics succéssifs
            div.unbind('click');
            li.unbind('click');
            // On envoi l'evenement de cloture aux autres fenètres
            if (that.session !== null) {
                that.session.publish(
                    'close_user_'+that.user_id+'_'+that.token,
                    {
                        category: 'close_user_'+that.user_id+'_'+that.token,
                        message: {
                            target: '#notification-' + data.id,
                            targetLi: '#notification-li-' + data.id
                        }
                    }
                );
            } else {
                $(div).stop(true, true).slideToggle(
                    400,
                    function () {
                        $(div).remove();
                    }
                );
                $(li).stop(true, true).slideToggle(
                    400,
                    function () {
                        $(li).remove();
                    }
                );
                that.updateCount(-1);
            }
            // Ajax pour suppression en base
            if (deleteByAjax && !isNaN(Number(data.id))) {
                $.ajax(
                    {
                        method: 'DELETE',
                        url: that.ajax.deleteNotification+'/'+data.id,
                        error: function () {
                            console.error("Notification delete failed");
                        }
                    }
                );
            }
        };
        div.click(action);
        li.click(action);

        setTimeout(
            function () {
                div.stop(true, true).slideUp(
                    400,
                    function () {
                        $(this).remove();
                    }
                );
            },
            5000
        );

        $(this.element).append(div);
        $(this.elementUl).append(li);

        return this;
    }
}

setInterval(
    function() {
        $('.notification-timer[data-created]').each(
            function() {
                var timeMessage = '';
                var elapsed = Math.round(Math.abs(new Date($(this).attr('data-created')) - new Date()) / 1000);
                if (elapsed < 60) {
                    timeMessage = __("à l'instant");
                } else if (elapsed < 3600) {
                    timeMessage = __("depuis {0} minute(s)", Math.round(elapsed / 60));
                } else if (elapsed < 86400) {
                    timeMessage = __("depuis {0} heure(s)", Math.floor(elapsed / 3600));
                } else if (elapsed >= 86400) {
                    timeMessage = __("depuis {0} jour(s)", Math.floor(elapsed / 86400));
                }
                $(this).text(timeMessage);
            }
        )
    },
    30000
);

/**
 * Namespace foure-tout pour les fonctions globales
 */
var AsalaeGlobal = {};

/**
 * Complexité du mot de passe
 *
 * @param {string} password
 * @returns {number} score de complexitée
 */
AsalaeGlobal.passwordScore = function (password) {
    var mult, chars = 0;
    if (/[a-z]/.test(password)) {
        chars += 26;
    }
    if (/[A-Z]/.test(password)) {
        chars += 26;
    }
    if (/[0-9]/.test(password)) {
        chars += 10;
    }
    if (/[ !#$*%]/.test(password)) {
        chars += 8;
    }
    if (/[^a-zA-Z0-9 !#$*%]/.test(password)) {
        chars += 20;
    }

    return password.length * Math.log2(chars);
};

/**
 * Initialise la pagination javascript
 *
 * @param {string|jQuery} section
 */
AsalaeGlobal.paginationAjax = function (section) {
    $(section).find('ul.pagination a').click(
        function (event) {
            event.preventDefault();
            if ($(this).parent().hasClass('disabled')
                || !$(this).attr('href')
                || $(this).attr('href').indexOf('javascript') === 0
            ) {
                return;
            }
            $(this).disable().closest('.pagination').find('a').disable().parent().addClass('disabled');
            AsalaeGlobal.updatePaginationAjax(section, $(this).attr('href'));
        }
    );
};

var updatingPaginationAjax = 0,
    queuedPaginationAjax = 0;
/**
 * Met à jour la section liée à la pagination
 *
 * @param {string|jQuery} section
 * @param {string} url
 */
AsalaeGlobal.updatePaginationAjax = function (section, url = window.location.href) {
    var initialHeight = $(section).height(),
        block = $('<div></div>').addClass('transition-container')
            .css('min-height', initialHeight),
        loading = $('<div class="text-center loading-div"><i aria-hidden="true" class="fa fa-4x fa-spinner faa-spin animated"></i></div>'),
        tables = $(section).find('table').map(
            function () {
                return $(this).attr('data-table-uid');
            }
        ).toArray(),
        tablesObj = tables.length ? tables.map((table) => TableGenerator.instance[table]) : null
    ;

    // Empêche le déclenchement trop rapide des mises à jour (max 1 mise à jour par seconde)
    // Si une mise à jour est commandée, les précédentes sont annulées au profit de la nouvelle
    if (updatingPaginationAjax) {
        clearTimeout(queuedPaginationAjax);
        queuedPaginationAjax = setTimeout(
            function () {
                AsalaeGlobal.updatePaginationAjax(section, url);
            },
            1000
        );
        return;
    }
    updatingPaginationAjax = true;

    $(section).after(block).css('min-height', initialHeight);
    block.append($(section));
    AsalaeLoading.start();
    $.ajax(
        {
            url: url,
            headers: {
                "X-Asalae-Update": 'section'
            },
            success: function (content) {
                var id = $(section).attr('id'),
                    html = $(content).hide();
                // renommage pour éviter les conflits d'ids
                section = $(section).attr('id', id+'-transition');
                section.find('[id]')
                    .each(
                        function () {
                            $(this).attr('id', $(this).attr('id')+'-transition');
                        }
                    );

                // animation de transition
                $(section).after(html).stop(true, true).fadeOut(
                    400,
                    function () {
                        $(this).remove();
                    }
                );
                html.not('script').stop(true, true).fadeIn(
                    400,
                    function () {
                        $(window).trigger('resize');
                    }
                );
                // on remet la pagination ajax sur le nouveau bloc
                setTimeout(
                    function () {
                        AsalaeGlobal.paginationAjax('#' + id);
                        AsalaeGlobal.initContentScope();

                        // on propage l'evenement
                        $(window).trigger('resize');
                        window.history.pushState({}, '', url);
                        $(window).trigger('history.change', url);
                    },
                    0
                );
            },
            error: function (jqXHR, textStatus, errorThrown) {
                if (jqXHR.status === 404 && url.indexOf('page=')) {
                    AsalaeGlobal.updatePaginationAjax(
                        section,
                        url.replace(/page=[0-9]+&?/, '').replace(/[?&]+$/, '')
                    );
                } else {
                    alert('Une erreur a eu lieu');
                }
            },
            complete: function () {
                loading.remove();
                AsalaeLoading.stop();
                setTimeout(
                    function () {
                        block.after(block.children()).remove();
                        updatingPaginationAjax = false;
                    },
                    400
                );
            }
        }
    );
}

/**
 * Permet d'obtenir la valeur d'une variable passée en GET
 *
 * @param {string} name
 * @param {string} url
 * @returns {string|null} valeur du param
 */
AsalaeGlobal.getParameterByName = function (name, url = window.location.href) {
    var regex = new RegExp("[?&]" + name.replace(/[\[\]]/g, "\\$&") + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) {
        return null;
    }
    if (!results[2]) {
        return '';
    }
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}

/**
 * Permet de décoder une chaine encodé par la fonction PHP urlencode()
 *
 * @param {string} str url à décoder
 * @returns {string} url décodé
 */
AsalaeGlobal.urldecode = function (str) {
    return decodeURIComponent(
        (str + '')
            .replace(/%(?![\da-f]{2})/gi, '%25')
            .replace(/\+/g, '%20')
    )
}

/**
 * Supprime les filtres actifs (boutton Supprimer les filtres)
 */
AsalaeGlobal.confirmRemoveFilters = function () {
    $('form.ModalFilters').empty().submit();
}

/**
 * Sauvegarde les filtres actifs
 */
AsalaeGlobal.saveFilters = function () {
    var modal = $(this).closest('.modal'),
        link = $(this).attr('data-link'),
        form = link && link.length ? $(link) : modal.find('form'),
        formData = AsalaeFilter.getFiltersData(form);
    if (formData === "") {
        let select = modal.find('select[name=filter]');
        select.prop('required', true);
        form.get(0).reportValidity();
        select.prop('required', false);
    } else if (formData) {
        let uid = modal.attr('data-uid'),
            saveModal = $('.filter-save-modal[data-uid='+uid+']');
        saveModal.modal('show');
        setTimeout(
            function () {
                saveModal.find('input').first().focus();
            },
            500
        );
    }
}

/**
 * Lance la suppression de toutes les notifications
 */
AsalaeGlobal.deleteAllNotifications = function (event, element) {
    event.stopPropagation();
    if (element) {
        fetch($(element).data('url'), {method: 'DELETE'}).catch((e) => console.error(e));
    } else {
        $('ul#notifications-ul-container li.notification').click();
    }
}

/**
 * Redirige vers la page indiqué dans l'input crée lors du clic sur les "..."
 */
AsalaeGlobal.paginationSelectInput = function () {
    $('.pagination li.ellipsis button').off('click').click(
        function () {
            var max = parseInt($(this).closest('.pagination').find('li:not(.next) a').last().text().replace(/\s/, ''), 10),
                url = $($(this).closest('.pagination').find('li:not(.active):not(.prev):not(.ellipsis) a:not([href=""])').get(1)); // Note: le 0 n'a pas de page=1
            $(this).css({position: 'relative'}).append(
                $('<input/>')
                    .css({position: 'absolute', top: '-30px', left: '0', width: '3em'})
                    .attr('min', 1)
                    .attr('max', max)
                    .attr('step', 1)
                    .attr('type', 'text')
                    .attr('id', 'pagination-select-input')
                    .bind(
                        'blur keydown',
                        function (event) {
                            var pageNum = url.text().trim(),
                                value = parseInt($(this).val(), 10);

                            if (event.type === 'blur'
                                || event.keyCode === 13
                                || event.keyCode === 10
                            ) {
                                if (!AsalaeGlobal.is_numeric($(this).val()) === false
                                    && value >= 1
                                    && value <= max
                                ) {
                                    url.attr('href', url.attr('href').replace('page='+pageNum, 'page='+$(this).val())).click();
                                }
                                $('input#pagination-select-input').remove();
                            }
                        }
                    )
            );
            $('#pagination-select-input').focus();
        }
    );
}

/**
 * Colore en rouge les onglets qui comportent une erreur
 * et selectionne le premier onglet en erreur
 */
AsalaeGlobal.colorTabsInError = function () {
    var firstErrors = [];
    $('div.ui-tabs-panel:has(.has-error, :invalid)').each(
        function () {
            var targetId = $(this).attr('id'),
                tab = $('a.ui-tabs-anchor[href="#'+targetId+'"]').first(),
                firstErrorId;

            tab.parent().addClass('bg-danger');
            firstErrorId = $(tab).closest('ul')
                .find('> li.bg-danger > a.ui-tabs-anchor')
                .first()
                .attr('id');

            if (firstErrors.indexOf(firstErrorId) === -1) {
                firstErrors.push(firstErrorId);
            }
        }
    );
    for (let i=0; i<firstErrors.length; i++) {
        $('#'+firstErrors[i]).click();
    }
}

/**
 * Attribue des événements lors d'un appel ajax
 * @type window.XMLHttpRequest|Window.XMLHttpRequest
 */
var OldXHR = window.XMLHttpRequest;
AsalaeGlobal.NewXHR = function () {
    var RealXHR = new OldXHR();
    RealXHR.addEventListener(
        "readystatechange",
        function () {
            var eventName;
            switch (RealXHR.readyState) {
                case 1:
                    eventName = 'ajax_connection';
                    break;
                case 2:
                    eventName = 'ajax_send';
                    break;
                case 3:
                    eventName = 'ajax_processing';
                    break;
                case 4:
                    eventName = 'ajax_complete';
                    break;
            }
            $('html').trigger(eventName);
        },
        false
    );
    return RealXHR;
}
window.XMLHttpRequest = AsalaeGlobal.NewXHR;

/**
 * Affiche une animation lors de l'attente d'une réponse ajax
 * @param {jQuery} modal
 */
AsalaeGlobal.addWaitingAjaxResponse = function (modal) {
    $(modal).fadeTo(400, 0.8).css('cursor', 'wait', 'important')
        .find('button.cancel, button.accept').css('cursor', 'wait', 'important').disable();
}

/**
 * Retire l'animation d'attente de réponse ajax
 * @param {jQuery} modal
 */
AsalaeGlobal.removeWaitingAjaxResponse = function (modal) {
    $(modal).stop(true, true).fadeTo(400, 1).css('cursor', '')
        .find('button.cancel, button.accept').css('cursor', '').enable();
}

/**
 * Vérifi qu'une chaine ne contiens que des chiffres
 * @param {string|number} str
 * @returns {boolean}
 */
AsalaeGlobal.is_numeric = function (str) {
    return /^\d+$/.test(''+str);
}

var miniWindowCount = 0;
/**
 * Permet la réduction des modals
 */
AsalaeGlobal.minifiableModal = function () {
    var minifierIcon = $('<i aria-hidden="true" class="fa fa-window-minimize float-right soften"></i>'),
        maximizerIcon = $('<i aria-hidden="true" class="fa fa-window-maximize float-right soften"></i>'),
        h4 = $(this).find('div.modal-header > h4'),
        title = h4.text(),
        modal = $(this),
        num,
        _minifyModal,
        windowLabel = modal.attr('data-window-label'),
        _maximizeModal,
        _removeMaximized;
    modal.attr("data-backdrop", "static");

    _removeMaximized = function () {
        modal.find('.maximized-modal.modal-dialog').removeClass('maximized-modal')
            .find('>.modal-content').css('height', '')
            .closest('.modal').css('overflow', 'hidden')
            .find('iframe').css('height', '');
    };
    _maximizeModal = function () {
        var dialog = $(this).closest('.modal-dialog'),
            content = dialog.find('>.modal-content'),
            iframe = content.find('iframe');
        dialog.toggleClass('maximized-modal');
        if (dialog.hasClass('maximized-modal')) {
            content.attr('data-width', content.css('width'))
                .attr('data-height', content.css('height'))
                .css('width', '')
                .css('height', '')
                .height($(window).height() -50)
                .closest('.modal').css('overflow', 'hidden');
            iframe.css('height', $(window).height() -50);
            modal.find('button.close').off('click', _removeMaximized).click(_removeMaximized);
        } else {
            content.css('width', content.attr('data-width'))
                .css('height', content.attr('data-height'));
            _removeMaximized();
        }
    };
    _minifyModal = function () {
        var modalClone = modal.clone(),
            restore = $('<i aria-hidden="true" class="fa fa-window-restore float-right slide-icon"></i>'),
            destroy = $('<i aria-hidden="true" class="fa fa-times float-right slide-icon"></i>'),
            label = title.length > 18 ? title.substr(0, 15)+'...' : title,
            h4 = $('<h4></h4>').append(label).append(destroy).append(restore).attr('title', title),
            miniWindow = $('<div class="mini-window"></div>').append(h4),
            top = 110,
            tops = [],
            num;
        if (!(num = minifierIcon.attr('data-num'))) {
            miniWindowCount++;
            num = miniWindowCount;
        }
        restore.append(num);

        modal.css('overflow', 'hidden');// Evite l'affichage du scroll durant l'animation

        destroy.click(
            function () {
                miniWindow.toggle(
                    'puff',
                    function () {
                        $(this).remove();
                    }
                )
            }
        )

        // On calcul la position vertical de la vignette par rapport aux autres
        $('.mini-window').each(
            function () {
                tops.push(parseInt($(this).css('top'), 10));
            }
        );
        tops.sort();
        for (let i = 0; i < tops.length; i++) {
            if ((top >= tops[i] && top < tops[i] + 40) || (top < tops[i] && top + 40 > tops[i])) {
                top = tops[i] + 40;
            }
        }

        miniWindow.css(
            {
                position: 'fixed', // Important pour "Chrome"
                left: 0,
                top: top+'px'
            }
        ).draggableCustom({handle: 'h4, .bottom-handle'});

        restore.click(
            function () {
                miniWindow.toggle('puff');
                if (modal.hasClass('in')) {
                    _minifyModal();
                    modal.stop(true, true).fadeOut(
                        200,
                        function () {
                            $(this).stop(true, true).fadeIn(200);
                        }
                    );
                }

                modal.find('.modal-body').replaceWith(modalClone.find('.modal-body'));
                modal.modal('show');
                minifierIcon.attr('data-num', num);
                _removeMaximized();
                setTimeout(
                    function () {
                        miniWindow.remove();
                    },
                    500
                );
            }
        );

        _removeMaximized();
        $('html > body').append(miniWindow);
    };

    minifierIcon.click(
        function () {
            if (windowLabel) {
                (function () {
                    var label = eval(windowLabel);
                    if (label) {
                        title = label;
                    }
                }).call(modal);
            }
            _minifyModal();
            modal.toggle('drop');
            modal.modal('hide');
            $(this).removeAttr('data-num');
        }
    );


    maximizerIcon.click(_maximizeModal);
    h4.dblclick(_maximizeModal);

    h4.find('i.fa-window-minimize, i.fa-window-maximize').remove();
    h4.append(maximizerIcon).append(minifierIcon);
}


/**
 * Permet de charger les filtres de recherche par ajax
 * @param event
 */
AsalaeGlobal.modalFiltersFormHandler = function (event) {
    var form = $(this).get(0);
    event.preventDefault();
    if (form.checkValidity()) {
        let clone = $(this).clone();
        clone.find('input.configuration-filter').remove();
        let url = $(this).attr('action') + '?' + clone.serialize();
        $(this).closest('.modal').modal('hide');
        $('html').addClass('ajax-loading');
        setTimeout(
            function () {
                AsalaeGlobal.interceptedLinkToAjax(url);
            },
            500
        );
    } else {
        form.reportValidity();
    }

}

/**
 * Assignation dynamique de la taille des table td
 */
AsalaeGlobal.smartTdSize = function () {
    $('.smart-td-size').each(
        function () {
            var columnsCount = 0,
                maxWidth = $(this).parent().width(),
                widths = [],
                max,
                trs = $(this).find('> thead > tr, > tbody > tr, > tr');

            if (trs.length === 0) {
                return;
            }
            // On réinitialise le tableau
            trs.find('> td:visible, > th:visible').css('max-width', '').removeClass('overflow');

            // On compte le nombre max de colonnes
            trs.each(
                function () {
                    var tdCount = 0;
                    $(this).find('> td:visible, > th:visible').each(
                        function () {
                            var colspan = $(this).attr('colspan');
                            if (!colspan) {
                                colspan = 1;
                            }
                            tdCount += parseInt(colspan, 10);
                        }
                    );
                    columnsCount = columnsCount < tdCount ? tdCount : columnsCount;
                }
            );

            // On quantifi la taille maximum de chaques colonnes
            trs.each(
                function () {
                    var i = 0;
                    $(this).find('> td:visible, > th:visible').each(
                        function () {
                            var colspan = $(this).attr('colspan'),
                                width = $(this).outerWidth();
                            if (!colspan) {
                                colspan = 1;
                            }
                            width = width / colspan;
                            for (let col = 0; col < colspan; ++col) {
                                if (widths[i + col]) {
                                    widths[i + col] = Math.max(widths[i + col], width);
                                } else {
                                    widths[i + col] = width;
                                }
                            }
                            i += colspan;
                        }
                    );
                }
            );
            if (widths.length === 0) {
                return;
            }

            // On cherche le max-width idéal pour chaques colonnes
            max = Math.max.apply(null, widths);
            while (Math.floor(
                widths.reduce(
                    function (a, b) {
                        return a + b;
                    },
                    0
                )
            ) > maxWidth && isFinite(max)) {
                max--;
                for (let i = 0; i < widths[i]; i++) {
                    if (widths[i] > max) {
                        widths[i] = max;
                    }
                }
            }

            // On destribu la taille maximum de chaques colonnes
            trs.each(
                function () {
                    var i = 0;
                    $(this).find('> td:visible, > th:visible').each(
                        function () {
                            var colspan = $(this).attr('colspan'),
                                width = 0,
                                divScrollable = $('<div></div>');
                            if (!colspan) {
                                colspan = 1;
                            }
                            for (let col = 0; col < colspan; ++col) {
                                width += widths[i + col];
                            }
                            i += colspan;
                            if ($(this).hasClass('action') && $(this).outerWidth() > width) {
                                $(this).addClass('overflow');
                            }
                            if ($(this).outerWidth() > width) {
                                $(this).css('max-width', width);
                                if (!$(this).find('> div').length) {
                                    $(this).contents().appendTo(divScrollable);
                                    $(this).append(divScrollable);
                                }
                            }
                        }
                    );
                }
            );
        }
    );
}

AsalaeGlobal.replacePageContent = function (content, push = '') {
    var pageEmpty = true,
        mainContent = $('.main-content');
    mainContent.find('*').off();
    mainContent.empty();
    $(content).each(
        function () {
            var id = $(this).attr('id');
            if (id === 'layout-content') {
                pageEmpty = false;
                $(this).find('*[id]').each(
                    function () {
                        var id = $(this).attr('id'),
                            target = $('#'+id);
                        if (Object.keys(target).length > 0) {
                            target.replaceWith($(this));
                        } else {
                            mainContent.append($(this));
                        }
                    }
                );
            } else if (id === 'template-content') {
                pageEmpty = false;
                $(this).find('.modal[id]').each(
                    function () {
                        $('.modal-list #'+$(this).attr('id')).remove();
                    }
                );
                $('.main-content').append($(this).html());
            } else if (id === 'head-content') {
                $('html > head > title').text($(this).find('title').html());
            }
        }
    );
    if (pageEmpty) {
        mainContent.append(content);
    }
    setTimeout(AsalaeGlobal.initContentScope, 0);
    if (push) {
        window.history.pushState({}, '', push);
        $(window).trigger('history.change', push);
    }
}

AsalaeGlobal.currentUrl = ''+window.location;

/**
 * Empeche la page de se recharger completement lors d'un clic sur <a>
 * @param {string} href
 * @param {boolean} push effectue un window.history.pushState
 * @param {function} callback fonction à appeler une fois le refresh effectué
 */
AsalaeGlobal.interceptedLinkToAjax = function (href, push = true, callback = undefined) {
    $('.modal').modal('hide');
    $(window).one(
        'history.change',
        function () {
            $('.modal-backdrop').remove();
            $('body').removeClass('modal-open');
            window.onbeforeunload = null;
        }
    );
    $('.main-content').stop(true, true).fadeOut();
    AsalaeGlobal.currentUrl = href;
    AsalaeLoading.ajax(
        {
            url: href,
            type: 'GET',
            headers: {"No-Layout": true},
            success: function (content, textStatus, jqXHR) {
                AsalaeGlobal.replacePageContent(content, push ? href : '');
            },
            error: function (jqXHR, textStatus, errorThrown) {
                if (jqXHR.status === 401
                    && typeof AsalaeSessionTimeout !== 'undefined'
                    && typeof AsalaeSessionTimeout.session_timeout !== 'undefined'
                ) {
                    clearTimeout(AsalaeSessionTimeout.session_timeout);
                    AsalaeSessionTimeout.checkSessionTimeout(0);
                    return;
                }
                if (jqXHR.responseText) {
                    var body = $(jqXHR.responseText).find('body');
                    if (body.length < 1) {
                        let bodyPos = jqXHR.responseText.indexOf('<body'),
                            bodyClose = jqXHR.responseText.indexOf('>', bodyPos + 1),
                            bodyEnd = jqXHR.responseText.indexOf('</body');
                        if (bodyPos !== -1) {
                            body = $(jqXHR.responseText.substring(bodyClose + 1, bodyEnd));
                        } else {
                            AsalaeGlobal.replacePageContent(jqXHR.responseText, push ? href : '');
                            return;
                        }
                    }
                    $('body').html(body);
                }
                if (push) {
                    window.history.pushState({}, '', href);
                    $(window).trigger('history.change', href);
                }
            },
            complete: function () {
                $('.main-content').stop(true, true).fadeIn();
                if (callback !== undefined) {
                    callback();
                }
            }
        }
    );
}

/**
 * Ajoute ?renew=true au lien de téléchargement si on fait CTRL + click sur un lien
 */
$(document).on('click.link', 'a', function(event, target) {
    if (($(this).attr('download') || $(this).attr('data-renewable')) && event.ctrlKey) {
        var oldHref = $(this).attr('href');
        var href = oldHref;
        href += href.indexOf('?') !== -1 ? '&renew=true' : '?renew=true';
        var link = $(this).attr('href', href);
        setTimeout(
            function() {
                link.attr('href', oldHref);
            },
            10
        );
    }
});

AsalaeGlobal.interceptedLinkhandler = function (event) {
    var href = $(this).attr('href');
    if (!href || event.isDefaultPrevented()) {
        return;
    }
    if (href[0] !== '#'
        && href.indexOf('javascript:') !== 0
        && ['no-ajax', 'section-ajax'].indexOf($(this).attr('data-mode')) === -1
        && !$(this).attr('target')
    ) {
        event.preventDefault();
        var beforeEvent = $.Event('before.intercept');
        $(this).trigger(beforeEvent);

        if (beforeEvent.isImmediatePropagationStopped()) {
            return;
        }
        AsalaeGlobal.interceptedLinkToAjax(href);
        $(this).trigger('after.intercept');
    }
};

AsalaeGlobal.grabHandlers = {
    mousedown: function (event) {
        $(this).addClass('grabbing')
            .data('clientX', event.clientX)
            .data('clientY', event.clientY)
            .data('scrollX', $(this).parent().scrollLeft())
            .data('scrollY', $(this).parent().scrollTop());
    },
    mouseup: function () {
        $('.grabbing').removeClass('grabbing');
    },
    mousemove: function (event) {
        if ($(this).hasClass('grabbing')) {
            var diffX = event.clientX - $(this).data('clientX'),
                diffY = event.clientY - $(this).data('clientY');
            $(this).parent()
                .scrollLeft($(this).data('scrollX') - diffX)
                .scrollTop($(this).data('scrollY') - diffY);
        }
    }
};

/**
 * Permet un affichage en mode "fixed" et respectant les règles d'accéssibilité
 * @param {jQuery} element
 * @param {string} label aria-label
 * @return {jQuery} element
 */
AsalaeGlobal.chosen = function (element, label) {
    var chosenOpts = {width: ''};
    if ($(element).find('option[value=""]').length) {
        chosenOpts.allow_single_deselect = true;
    }
    element.chosen(chosenOpts)
        .attr('tabindex', '-1')
        .on(
            'chosen:showing_dropdown',
            function () {
                var container = $(this).parent().find('.chosen-container'),
                    modal = container.closest('.modal-dialog'),
                    drop = container.find('.chosen-drop'),
                    offset = container.offset(),
                    modalOffset = modal.offset(),
                    width = container.parent().width();
                if (modal.length > 0) {
                    drop.css(
                        {
                            top: offset.top + container.height() - modalOffset.top,
                            left: offset.left - modalOffset.left
                        }
                    );
                } else {
                    drop.css(
                        {
                            top: offset.top + container.height(),
                            left: offset.left,
                            width: container.width()
                        }
                    );
                }
                // Corrige le décalage de 10px entre le 1er appel et le 2e
                if (container.parent().width() > width) {
                    drop.css('width', container.parent().width());
                }
            }
        )
        .on(
            'chosen:hiding_dropdown',
            function () {
                var container = $(this).parent().find('.chosen-container'),
                    modal = container.closest('.modal-dialog'),
                    drop = container.find('.chosen-drop');
                drop.attr('style', '');
            }
        )
        .parent()
        .find('select').show().css(
        {
            opacity: 0,
            position: 'absolute',
            "z-index": -1
        }
    )
        .addClass('chosenified')
        .on(
            'invalid',
            function (event) {
                $(this).parent().find('.chosen-container').addClass('invalid').one(
                    'click',
                    function () {
                        $(this).removeClass('invalid');
                    }
                );
            }
        )
        .parent()
        .find('input.chosen-search-input')
        .attr('aria-label', label);
    return element;
}

/**
 * Affichage des erreurs lié a une entité
 * @param {object} msg
 * @returns {string}
 */
AsalaeGlobal.entityErrors = function (msg) {
    if (typeof msg !== 'object') {
        return '';
    } else if (msg.errors) {
        return AsalaeGlobal.entityErrors(msg.errors);
    }
    var str = '';
    for (var field in msg) {
        for (var e in msg[field]) {
            str += ' - '+field+': '+msg[field][e]+"\n";
        }
    }
    return str;
};

/**
 * gestion d'erreur générique
 */
AsalaeGlobal.ajaxError = function (e) {
    if (typeof e === 'undefined') {
        console.error('unknown error');
        console.trace();
        return;
    }
    if (e && typeof e === 'object' && e.responseJSON && typeof e.responseJSON.message === 'string') {
        var line = e.responseJSON.file
            ? "\n" + e.responseJSON.file + ':' + e.responseJSON.line
            : '';
        if (e.responseJSON.message === '') {
            e.responseJSON.message = __("Une erreur inattendue a eu lieu");
        }
        alert(
            __("Erreur {0}: {1}", e.responseJSON.code, line)
            + "\n\n" + __("message: {0}", e.responseJSON.message)
        );
        if (PHP.debug) {
            console.error(e.responseJSON.local_trace);
            console.warn(e.responseJSON.trace);
        }
    } else if (e.status === 401
        && typeof AsalaeSessionTimeout !== 'undefined'
        && typeof AsalaeSessionTimeout.session_timeout !== 'undefined'
    ) {
        displayModalSessionTimeout();
    } else if (!e || typeof e !== 'object' || e.statusText !== 'abort') {
        console.error(e);
    }
}

/**
 * Equivalent du AsalaeGlobal.chosen en select2
 * @param {jQuery} element
 * @param {object} options à passer à select2
 * @return {jQuery} element
 */
AsalaeGlobal.select2 = function (element, options = {}) {
    if (!(element instanceof jQuery)) {
        element = $(element);
    }
    if (typeof options.allowClear === 'undefined') {
        options.allowClear = element.is(':required') === false;
    }
    if (typeof options.placeholder === 'undefined') {
        options.placeholder = element.find('option[value=""]').first().html() || '';
    }
    if (typeof options.width === 'undefined') {
        options.width = '100%';
    }
    var parent = element.closest('.modal');
    if (parent.length === 0) {
        parent = $('body');
    }
    if ($(element).attr('multiple')) {
        element.select2(
            $.extend(
                {},
                {
                    multiple: true,
                    dropdownParent: parent,
                    templateSelection: function (tag, container) {
                        var element = $(tag.element);
                        if (element.attr('locked')) {
                            $(container).addClass('locked-tag');
                            tag.locked = true;
                            return tag.text;
                        }
                        return tag.text;
                    }
                },
                options
            )
        ).on(
            'select2:unselecting',
            function (e) {
                if ($(e.params.args.data.element).attr('locked')) {
                    e.preventDefault();
                }
            }
        );
    } else {
        element.select2(
            $.extend(
                {},
                options,
                {
                    dropdownParent: parent,
                    width: '100%'
                }
            )
        );
    }
    element.siblings('.select2')
        .first()
        .find('.select2-search__field')
        .attr('aria-label', 'search field');
    return element;
}

/**
 * Ajoute la fonction h() à javascript
 * @param str
 * @return {string}
 */
AsalaeGlobal.h = function (str) {
    return String(str).replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;');
}

/**
 * Scroll vers le bas
 * @param {Element} element
 * @return {string}
 */
AsalaeGlobal.scrollBottom = function (element) {
    var scrollable = AsalaeGlobal.getScrollParent(element);
    if (scrollable.tagName === 'BODY') {
        $('html, body').animate({scrollTop: scrollable.scrollHeight}, 'fast');
    } else {
        $(scrollable).animate({scrollTop: scrollable.scrollHeight}, 'fast');
    }
}

/**
 * Scroll vers le haut
 * @param {Element} element
 * @return {string}
 */
AsalaeGlobal.scrollTop = function (element) {
    var scrollable = AsalaeGlobal.getScrollParent(element);
    $(scrollable).animate({scrollTop: 0}, 'fast');
    if (scrollable.tagName === 'BODY') {
        scrollable = $('html, body');
    }
    $(scrollable).animate({scrollTop: 0}, 'fast');
}

/**
 * Ajoute la fonction h() à javascript
 * @param {Element} node
 * @return {string}
 */
AsalaeGlobal.getScrollParent = function (node) {
    if (node instanceof $) {
        node = node.get(0);
    }
    if (node && node.tagName === 'BODY') {
        return node;
    }
    if (!node) {
        return null;
    }
    if (node.scrollHeight > node.clientHeight && $(node).css('overflow-y') === 'auto') {
        return node;
    } else {
        return AsalaeGlobal.getScrollParent(node.parentNode);
    }
}

/**
 * effectu une animation sur un champ de formulaire par exemple afin de montrer
 * qu'il a été modifié
 * @param node
 * @param color
 */
AsalaeGlobal.highlight = function (node, color = 'success') {
    switch (color) {
        case 'green':
        case 'success':
            color = '#5cb85c';
            break;
        case 'orange':
        case 'warning':
            color = '#ffb648';
            break;
        case 'red':
        case 'danger':
            color = '#ff3e3e';
            break;
        case 'blue':
        case 'info':
            color = '#3eddff';
            break;
    }

    var parent = $(node).stop(true, true).shake({duration: 1000, distance: 2}).parent();
    var initialBg = parent.css('background-color');
    parent.css('background-color', color)
        .stop(true, true)
        .animate(
            {'background-color': 'transparent'},
            function () {
                parent.css('background-color', initialBg);
            }
        );
};

/**
 * Color un instant une zone
 * @param node
 * @param color
 */
AsalaeGlobal.blink = function (node, color = '#5cb85c') {
    node = $(node);
    node.each(
        function () {
            var currentNode = $(this);
            if (currentNode.attr('data-running-animation')) {
                return;
            }
            var initialBg = currentNode.css('background-color');
            currentNode.css('background-color', color)
                .attr('data-running-animation', 'true')
                .attr('data-initial-bg', initialBg)
                .stop(true, true)
                .animate(
                    {'background-color': 'transparent'},
                    function () {
                        currentNode.css('background-color', initialBg)
                            .removeAttr('data-running-animation')
                            .removeAttr('data-initial-bg');
                    }
                );
        }
    );
    // correction auto des animations bloquées
    setTimeout(
        function () {
            $('[data-running-animation]').each(
                function () {
                    $(this).css('background-color', $(this).attr('data-initial-bg'))
                        .removeAttr('data-running-animation')
                        .removeAttr('data-initial-bg');
                }
            );
        },
        500
    );
};

/**
 * Donne une chaine avec une taille limitée
 * @param {string} str
 * @param {bool} isFilename
 * @param {int} maxlength
 * @param {string} replace
 * @return {string}
 */
AsalaeGlobal.shortString = function (str, isFilename = false, maxlength = 30, replace = '~') {
    if (str.length < maxlength) {
        return str;
    }
    if (isFilename) {
        if (str.indexOf('/') !== -1) {
            str = str.substr(str.lastIndexOf('/') +1);
            if (str.length <= maxlength) {
                return str;
            }
        }
        let endString;
        if (str.indexOf('.') !== -1) {
            let ext = str.substr(str.lastIndexOf('.') +1);
            endString = replace+'.'+ext;
        } else {
            endString = replace;
        }
        let cutLength = maxlength - endString.length;
        return str.substr(0, cutLength) + endString;
    } else {
        return str.substr(0, maxlength -1) + replace;
    }
}

AsalaeGlobal.forceEncode = function (str, char) {
    let charCode = char.charCodeAt(0).toString(16).toUpperCase();
    return str.replace(new RegExp(char, 'g'), '%' + charCode);
}

/**
 * Fait un encodeURIComponent mais remplace le % par escapeChar
 * (contourne la protection d'apache sur le / ou le ?)
 * @param {string} str
 * @param {string} escapeChar
 * @return {string}
 */
AsalaeGlobal.safeUrlEncode = function (str, escapeChar = '~') {
    let encodedStr = encodeURIComponent(str)
        .replace(/[!~*'()]/g, (c) => '%' + c.charCodeAt(0).toString(16).toUpperCase());
    let escapeCharCode = escapeChar.charCodeAt(0).toString(16).toUpperCase();
    encodedStr = encodedStr.replace(new RegExp(escapeChar, 'g'), '%' + escapeCharCode);
    return encodedStr.replace(/%/g, escapeChar);
}

/**
 * Fait un decodeURIComponent mais remplace le escapeChar par %
 * @param {string} str
 * @param {string} escapeChar
 * @return {string}
 */
AsalaeGlobal.safeUrlDecode = function (str, escapeChar = '~') {
    let decodedStr = str.replace(new RegExp(escapeChar, 'g'), '%');
    return decodeURIComponent(decodedStr);
}

/**
 *
 * @return {any|jQuery|HTMLElement}
 */
AsalaeGlobal.extractDebug = function () {
    var output = $('<div></div>');
    $('script').each(
        function () {
            var inner = $(this).html();
            var div = $('<div class="cake-debug-output"></div>');
            var pointer = 0;
            var begin = inner.indexOf('<pre class="cake-error">');
            var end = 0;
            while (begin !== -1) {
                div = $('<div class="cake-debug-output inner-script"></div>');
                pointer = begin;
                end = inner.indexOf('</pre>', pointer);
                if (!end) {
                    break;
                }
                pointer = end;
                div.append(inner.substr(begin, end - begin));
                output.append(div);
                console.warn('error exported from script', this);
                begin = inner.indexOf('<pre class="cake-error">', pointer);
            }
        }
    );
    return output.children();
};

/**
 * Déplacement des liens dans le menu utilisateur
 */
class CustomMenu {
    /**
     * Ouvre le menu utilisateur au survol du bouton pendant le drag
     * @param event
     */
    static dropdownToggleOnDragoverHandler(event)
    {
        var dropdown = $(this).find('.dropdown-toggle'),
            target = dropdown.attr('data-target');
        if (target) {
            $(target).removeClass('open');
        } else {
            $(this).removeClass('open');
        }
        $(this).addClass('dragover');
        dropdown.dropdown('toggle');
    }

    /**
     * Ferme le menu utilisateur en quittant le survol
     */
    static dragleaveHandler()
    {
        $(this).removeClass('dragover');
    }

    /**
     * Drag sur le menu utilisateur place le lien selon hauteur de la souris
     * @param event
     */
    static dragoverHandler(event)
    {
        var offset = $(this).offset(),
            height = $(this).height();
        if (!CustomMenu.navbarDragging) {
            return;
        }
        if (event.pageY < offset.top + (height / 2)) {
            CustomMenu.navbarDragging.insertBefore($(this).parent());
        } else {
            CustomMenu.navbarDragging.insertAfter($(this).parent());
        }
        if (CustomMenu.navbarDraggingBackup) {
            CustomMenu.navbarDraggingBackup.hide();
        }
    }

    /**
     * Déplace un lien existant du menu utilisateur
     */
    static dragstartHandler()
    {
        var target = $(this).parent(),
            clone = target.clone();
        CustomMenu.navbarDragging = clone.addClass('active');
        if (CustomMenu.navbarDragging.find('.fa-times').length === 0) {
            CustomMenu.navbarDragging.append(CustomMenu.getCloseBtn());
        }
        if ($('#user-menu').find(this).length) {
            CustomMenu.navbarDraggingBackup = target;
        }
    }

    /**
     * Lorsque on relache le drag
     *  - dans la zone d'insertion: suppression du backup et ajout du bouton supprimer
     *  - hors de la zone: restauration via backup
     * @param event
     */
    static dragendHandler(event)
    {
        if (!CustomMenu.navbarDragging) {
            return;
        }
        // NOTE: suite à un dragstart, les events "mouse" ne sont plus triggered
        // "dragend" est triggered, on ne peut pas utiliser target ni pageX-pageY (différences selon navigateur)
        // l'event "mousemove" qui suit sur firefox (sous windows uniquement) n'est pas utilisable
        // les informations qu'il contient semblent venir du moment du "dragstart"
        var i = 0;
        $(document).on(
            'mousemove.usermenu',
            function (event) {
                i++;
                if (i === 1) {
                    return;
                }
                var userMenu = $('#user-menu'),
                    menu = userMenu.find('.dropdown-menu'),
                    offset = menu.offset(),
                    width = menu.width(),
                    height = menu.height();
                if (!CustomMenu.navbarDragging) {
                    $(document).off('mousemove.usermenu');
                    return;
                }
                // Si la souris est à l'interieur du menu lors de l'evenement...
                if (offset
                    && event.pageX > offset.left
                    && event.pageX < offset.left + width
                    && event.pageY > offset.top
                    && event.pageY < offset.top + height
                ) {
                    CustomMenu.navbarDragging.removeClass('active');
                    CustomMenu.navbarDragging.find('.navbar-link:not(.disable-drag)')
                        .off('.usermenu')
                        .on('dragover.usermenu dragenter.usermenu', CustomMenu.dragoverHandler)
                        .on('dragstart.usermenu', CustomMenu.dragstartHandler)
                        .on('dragend.usermenu', CustomMenu.dragendHandler);
                    if (CustomMenu.navbarDraggingBackup) {
                        CustomMenu.navbarDraggingBackup.remove();
                    }
                    setTimeout(
                        function () {
                            $('#user-menu').trigger('user-menu.changed');
                        },
                        1
                    );
                } else {
                    CustomMenu.navbarDragging.remove();
                    if (CustomMenu.navbarDraggingBackup) {
                        CustomMenu.navbarDraggingBackup.show();
                    }
                }
                CustomMenu.navbarDragging = undefined;
                CustomMenu.navbarDraggingBackup = undefined;
                $(document).off('mousemove.usermenu');
            }
        );
    }

    /**
     * Croix de suppression d'un lien du menu utilisateur
     * @returns {jQuery}
     */
    static getCloseBtn()
    {
        var btn = $('<i aria-hidden="true" class="fa fa-times close"></i>');
        return btn.click(
            function (event) {
                event.stopPropagation();
                $(this).closest('li').remove();
                $('#user-menu').trigger('user-menu.changed');
            }
        )
    }
}
CustomMenu.navbarDragging = undefined;
CustomMenu.navbarDraggingBackup = undefined;

var AsalaeLoading = {
    started: false,
    checkinterval: null,
    initialized: false,
    running: []
};

/**
 * Démarre l'animation de chargement
 */
AsalaeLoading.start = function () {
    if (AsalaeLoading.started) {
        return;
    }
    AsalaeLoading.started = true;
    $('html').addClass('ajax-loading');
    $('#loading-screen').stop(true, true).fadeIn(100);
    // Résout un bug sur ie
    setTimeout(
        function () {
            $('#loading-screen').removeClass('paused');
        },
        10
    );
    AsalaeLoading.checkinterval = setInterval(
        function () {
            if (!AsalaeLoading.initialized) {
                return;
            }
            AsalaeLoading.running = AsalaeLoading.running.filter(
                function (ajax) {
                    return ajax.readyState < 4;
                }
            );
            if (AsalaeLoading.running.length === 0) {
                clearInterval(AsalaeLoading.checkinterval);
                console.warn("ajax request was terminated without call complete()");
                AsalaeLoading.stop();
                alert(__("Une erreur inattendue a eu lieu"));
            }
        },
        1000
    );
};

/**
 * Termine l'animation de chargement
 */
AsalaeLoading.stop = function () {
    $('html').removeClass('ajax-loading');
    $('#loading-screen').stop(true, true).fadeOut(
        100,
        function () {
            $(this).addClass('paused');
        }
    );
    clearInterval(AsalaeLoading.checkinterval);
    AsalaeLoading.initialized = false;
    AsalaeLoading.running = [];
    AsalaeLoading.started = false;
};

/**
 * Effectue un appel ajax avec animation de chargement
 * @param {object} params
 * @param {function} start
 * @param {function} stop
 */
AsalaeLoading.ajax = function (params, start = AsalaeLoading.start, stop = AsalaeLoading.stop) {
    AsalaeLoading.initialized = false;
    start();
    try {
        if (!params.disable_error_callback) {
            if (params.error) {
                let fn = params.error;
                params.error = function () {
                    AsalaeGlobal.ajaxError.apply(params, arguments);
                    fn.apply(params, arguments);
                }
            } else {
                params.error = AsalaeGlobal.ajaxError;
            }
        }
        if (!params.disable_complete_callback) {
            if (params.complete) {
                let fn = params.complete;
                params.complete = function () {
                    stop();
                    fn.apply(params, arguments);
                }
            } else {
                params.complete = function () {
                    stop();
                };
            }
        }
        var ajax = $.ajax(params);
        AsalaeLoading.running.push(ajax);
        AsalaeLoading.initialized = true;
        return ajax;
    } catch (e) {
        stop();
        console.error(e);
    }
}

/**
 * Recharge les cookies enregistré en base
 */
if (typeof PHP === 'object' && PHP.cookies) {
    AsalaeCookies.init(PHP.cookies);
}

/**
 * Retire les accents (diacritics)
 * @returns {string}
 */
String.prototype.unaccents = function () {
    return this.normalize('NFD').replace(/[\u0300-\u036f]/g, "");
}

/**
 * Options surchargeable
 */
AsalaeGlobal.datepickerOpts = $.datepicker.regional['fr'];

/**
 * Ajoute un datepicker à un input
 * @param element
 * @returns {*|jQuery}
 */
AsalaeGlobal.datepicker = function (element) {
    return $(element).last()
        .datepicker(AsalaeGlobal.datepickerOpts)
        .siblings("span.input-group-addon")
        .last()
        .click(
            function () {
                $(this).siblings("input").focus();
            }
        );
}

/**
 * Ajoute un datetimepicker à un input
 * @param element
 * @returns {*|jQuery}
 */
AsalaeGlobal.datetimepicker = function (element) {
    return $(element).last()
        .datetimepicker(AsalaeGlobal.datepickerOpts)
        .siblings("span.input-group-addon")
        .last()
        .click(
            function () {
                $(this).siblings("input").focus();
            }
        );
}

/**
 * @link https://stackoverflow.com/questions/990904/remove-accents-diacritics-in-a-string-in-javascript
 */
AsalaeGlobal.defaultDiacriticsRemovalMap = [
    {'base':'A', 'letters':/[\u0041\u24B6\uFF21\u00C0\u00C1\u00C2\u1EA6\u1EA4\u1EAA\u1EA8\u00C3\u0100\u0102\u1EB0\u1EAE\u1EB4\u1EB2\u0226\u01E0\u00C4\u01DE\u1EA2\u00C5\u01FA\u01CD\u0200\u0202\u1EA0\u1EAC\u1EB6\u1E00\u0104\u023A\u2C6F]/g},
    {'base':'AA','letters':/\uA732/g},
    {'base':'AE','letters':/[\u00C6\u01FC\u01E2]/g},
    {'base':'AO','letters':/\uA734/g},
    {'base':'AU','letters':/\uA736/g},
    {'base':'AV','letters':/[\uA738\uA73A]/g},
    {'base':'AY','letters':/\uA73C/g},
    {'base':'B', 'letters':/[\u0042\u24B7\uFF22\u1E02\u1E04\u1E06\u0243\u0182\u0181]/g},
    {'base':'C', 'letters':/[\u0043\u24B8\uFF23\u0106\u0108\u010A\u010C\u00C7\u1E08\u0187\u023B\uA73E]/g},
    {'base':'D', 'letters':/[\u0044\u24B9\uFF24\u1E0A\u010E\u1E0C\u1E10\u1E12\u1E0E\u0110\u018B\u018A\u0189\uA779]/g},
    {'base':'DZ','letters':/[\u01F1\u01C4]/g},
    {'base':'Dz','letters':/[\u01F2\u01C5]/g},
    {'base':'E', 'letters':/[\u0045\u24BA\uFF25\u00C8\u00C9\u00CA\u1EC0\u1EBE\u1EC4\u1EC2\u1EBC\u0112\u1E14\u1E16\u0114\u0116\u00CB\u1EBA\u011A\u0204\u0206\u1EB8\u1EC6\u0228\u1E1C\u0118\u1E18\u1E1A\u0190\u018E]/g},
    {'base':'F', 'letters':/[\u0046\u24BB\uFF26\u1E1E\u0191\uA77B]/g},
    {'base':'G', 'letters':/[\u0047\u24BC\uFF27\u01F4\u011C\u1E20\u011E\u0120\u01E6\u0122\u01E4\u0193\uA7A0\uA77D\uA77E]/g},
    {'base':'H', 'letters':/[\u0048\u24BD\uFF28\u0124\u1E22\u1E26\u021E\u1E24\u1E28\u1E2A\u0126\u2C67\u2C75\uA78D]/g},
    {'base':'I', 'letters':/[\u0049\u24BE\uFF29\u00CC\u00CD\u00CE\u0128\u012A\u012C\u0130\u00CF\u1E2E\u1EC8\u01CF\u0208\u020A\u1ECA\u012E\u1E2C\u0197]/g},
    {'base':'J', 'letters':/[\u004A\u24BF\uFF2A\u0134\u0248]/g},
    {'base':'K', 'letters':/[\u004B\u24C0\uFF2B\u1E30\u01E8\u1E32\u0136\u1E34\u0198\u2C69\uA740\uA742\uA744\uA7A2]/g},
    {'base':'L', 'letters':/[\u004C\u24C1\uFF2C\u013F\u0139\u013D\u1E36\u1E38\u013B\u1E3C\u1E3A\u0141\u023D\u2C62\u2C60\uA748\uA746\uA780]/g},
    {'base':'LJ','letters':/\u01C7/g},
    {'base':'Lj','letters':/\u01C8/g},
    {'base':'M', 'letters':/[\u004D\u24C2\uFF2D\u1E3E\u1E40\u1E42\u2C6E\u019C]/g},
    {'base':'N', 'letters':/[\u004E\u24C3\uFF2E\u01F8\u0143\u00D1\u1E44\u0147\u1E46\u0145\u1E4A\u1E48\u0220\u019D\uA790\uA7A4]/g},
    {'base':'NJ','letters':/\u01CA/g},
    {'base':'Nj','letters':/\u01CB/g},
    {'base':'O', 'letters':/[\u004F\u24C4\uFF2F\u00D2\u00D3\u00D4\u1ED2\u1ED0\u1ED6\u1ED4\u00D5\u1E4C\u022C\u1E4E\u014C\u1E50\u1E52\u014E\u022E\u0230\u00D6\u022A\u1ECE\u0150\u01D1\u020C\u020E\u01A0\u1EDC\u1EDA\u1EE0\u1EDE\u1EE2\u1ECC\u1ED8\u01EA\u01EC\u00D8\u01FE\u0186\u019F\uA74A\uA74C]/g},
    {'base':'OI','letters':/\u01A2/g},
    {'base':'OO','letters':/\uA74E/g},
    {'base':'OU','letters':/\u0222/g},
    {'base':'P', 'letters':/[\u0050\u24C5\uFF30\u1E54\u1E56\u01A4\u2C63\uA750\uA752\uA754]/g},
    {'base':'Q', 'letters':/[\u0051\u24C6\uFF31\uA756\uA758\u024A]/g},
    {'base':'R', 'letters':/[\u0052\u24C7\uFF32\u0154\u1E58\u0158\u0210\u0212\u1E5A\u1E5C\u0156\u1E5E\u024C\u2C64\uA75A\uA7A6\uA782]/g},
    {'base':'S', 'letters':/[\u0053\u24C8\uFF33\u1E9E\u015A\u1E64\u015C\u1E60\u0160\u1E66\u1E62\u1E68\u0218\u015E\u2C7E\uA7A8\uA784]/g},
    {'base':'T', 'letters':/[\u0054\u24C9\uFF34\u1E6A\u0164\u1E6C\u021A\u0162\u1E70\u1E6E\u0166\u01AC\u01AE\u023E\uA786]/g},
    {'base':'TZ','letters':/\uA728/g},
    {'base':'U', 'letters':/[\u0055\u24CA\uFF35\u00D9\u00DA\u00DB\u0168\u1E78\u016A\u1E7A\u016C\u00DC\u01DB\u01D7\u01D5\u01D9\u1EE6\u016E\u0170\u01D3\u0214\u0216\u01AF\u1EEA\u1EE8\u1EEE\u1EEC\u1EF0\u1EE4\u1E72\u0172\u1E76\u1E74\u0244]/g},
    {'base':'V', 'letters':/[\u0056\u24CB\uFF36\u1E7C\u1E7E\u01B2\uA75E\u0245]/g},
    {'base':'VY','letters':/\uA760/g},
    {'base':'W', 'letters':/[\u0057\u24CC\uFF37\u1E80\u1E82\u0174\u1E86\u1E84\u1E88\u2C72]/g},
    {'base':'X', 'letters':/[\u0058\u24CD\uFF38\u1E8A\u1E8C]/g},
    {'base':'Y', 'letters':/[\u0059\u24CE\uFF39\u1EF2\u00DD\u0176\u1EF8\u0232\u1E8E\u0178\u1EF6\u1EF4\u01B3\u024E\u1EFE]/g},
    {'base':'Z', 'letters':/[\u005A\u24CF\uFF3A\u0179\u1E90\u017B\u017D\u1E92\u1E94\u01B5\u0224\u2C7F\u2C6B\uA762]/g},
    {'base':'a', 'letters':/[\u0061\u24D0\uFF41\u1E9A\u00E0\u00E1\u00E2\u1EA7\u1EA5\u1EAB\u1EA9\u00E3\u0101\u0103\u1EB1\u1EAF\u1EB5\u1EB3\u0227\u01E1\u00E4\u01DF\u1EA3\u00E5\u01FB\u01CE\u0201\u0203\u1EA1\u1EAD\u1EB7\u1E01\u0105\u2C65\u0250]/g},
    {'base':'aa','letters':/\uA733/g},
    {'base':'ae','letters':/[\u00E6\u01FD\u01E3]/g},
    {'base':'ao','letters':/\uA735/g},
    {'base':'au','letters':/\uA737/g},
    {'base':'av','letters':/[\uA739\uA73B]/g},
    {'base':'ay','letters':/\uA73D/g},
    {'base':'b', 'letters':/[\u0062\u24D1\uFF42\u1E03\u1E05\u1E07\u0180\u0183\u0253]/g},
    {'base':'c', 'letters':/[\u0063\u24D2\uFF43\u0107\u0109\u010B\u010D\u00E7\u1E09\u0188\u023C\uA73F\u2184]/g},
    {'base':'d', 'letters':/[\u0064\u24D3\uFF44\u1E0B\u010F\u1E0D\u1E11\u1E13\u1E0F\u0111\u018C\u0256\u0257\uA77A]/g},
    {'base':'dz','letters':/[\u01F3\u01C6]/g},
    {'base':'e', 'letters':/[\u0065\u24D4\uFF45\u00E8\u00E9\u00EA\u1EC1\u1EBF\u1EC5\u1EC3\u1EBD\u0113\u1E15\u1E17\u0115\u0117\u00EB\u1EBB\u011B\u0205\u0207\u1EB9\u1EC7\u0229\u1E1D\u0119\u1E19\u1E1B\u0247\u025B\u01DD]/g},
    {'base':'f', 'letters':/[\u0066\u24D5\uFF46\u1E1F\u0192\uA77C]/g},
    {'base':'g', 'letters':/[\u0067\u24D6\uFF47\u01F5\u011D\u1E21\u011F\u0121\u01E7\u0123\u01E5\u0260\uA7A1\u1D79\uA77F]/g},
    {'base':'h', 'letters':/[\u0068\u24D7\uFF48\u0125\u1E23\u1E27\u021F\u1E25\u1E29\u1E2B\u1E96\u0127\u2C68\u2C76\u0265]/g},
    {'base':'hv','letters':/\u0195/g},
    {'base':'i', 'letters':/[\u0069\u24D8\uFF49\u00EC\u00ED\u00EE\u0129\u012B\u012D\u00EF\u1E2F\u1EC9\u01D0\u0209\u020B\u1ECB\u012F\u1E2D\u0268\u0131]/g},
    {'base':'j', 'letters':/[\u006A\u24D9\uFF4A\u0135\u01F0\u0249]/g},
    {'base':'k', 'letters':/[\u006B\u24DA\uFF4B\u1E31\u01E9\u1E33\u0137\u1E35\u0199\u2C6A\uA741\uA743\uA745\uA7A3]/g},
    {'base':'l', 'letters':/[\u006C\u24DB\uFF4C\u0140\u013A\u013E\u1E37\u1E39\u013C\u1E3D\u1E3B\u017F\u0142\u019A\u026B\u2C61\uA749\uA781\uA747]/g},
    {'base':'lj','letters':/\u01C9/g},
    {'base':'m', 'letters':/[\u006D\u24DC\uFF4D\u1E3F\u1E41\u1E43\u0271\u026F]/g},
    {'base':'n', 'letters':/[\u006E\u24DD\uFF4E\u01F9\u0144\u00F1\u1E45\u0148\u1E47\u0146\u1E4B\u1E49\u019E\u0272\u0149\uA791\uA7A5]/g},
    {'base':'nj','letters':/\u01CC/g},
    {'base':'o', 'letters':/[\u006F\u24DE\uFF4F\u00F2\u00F3\u00F4\u1ED3\u1ED1\u1ED7\u1ED5\u00F5\u1E4D\u022D\u1E4F\u014D\u1E51\u1E53\u014F\u022F\u0231\u00F6\u022B\u1ECF\u0151\u01D2\u020D\u020F\u01A1\u1EDD\u1EDB\u1EE1\u1EDF\u1EE3\u1ECD\u1ED9\u01EB\u01ED\u00F8\u01FF\u0254\uA74B\uA74D\u0275]/g},
    {'base':'oi','letters':/\u01A3/g},
    {'base':'ou','letters':/\u0223/g},
    {'base':'oo','letters':/\uA74F/g},
    {'base':'p','letters':/[\u0070\u24DF\uFF50\u1E55\u1E57\u01A5\u1D7D\uA751\uA753\uA755]/g},
    {'base':'q','letters':/[\u0071\u24E0\uFF51\u024B\uA757\uA759]/g},
    {'base':'r','letters':/[\u0072\u24E1\uFF52\u0155\u1E59\u0159\u0211\u0213\u1E5B\u1E5D\u0157\u1E5F\u024D\u027D\uA75B\uA7A7\uA783]/g},
    {'base':'s','letters':/[\u0073\u24E2\uFF53\u00DF\u015B\u1E65\u015D\u1E61\u0161\u1E67\u1E63\u1E69\u0219\u015F\u023F\uA7A9\uA785\u1E9B]/g},
    {'base':'t','letters':/[\u0074\u24E3\uFF54\u1E6B\u1E97\u0165\u1E6D\u021B\u0163\u1E71\u1E6F\u0167\u01AD\u0288\u2C66\uA787]/g},
    {'base':'tz','letters':/\uA729/g},
    {'base':'u','letters':/[\u0075\u24E4\uFF55\u00F9\u00FA\u00FB\u0169\u1E79\u016B\u1E7B\u016D\u00FC\u01DC\u01D8\u01D6\u01DA\u1EE7\u016F\u0171\u01D4\u0215\u0217\u01B0\u1EEB\u1EE9\u1EEF\u1EED\u1EF1\u1EE5\u1E73\u0173\u1E77\u1E75\u0289]/g},
    {'base':'v','letters':/[\u0076\u24E5\uFF56\u1E7D\u1E7F\u028B\uA75F\u028C]/g},
    {'base':'vy','letters':/\uA761/g},
    {'base':'w','letters':/[\u0077\u24E6\uFF57\u1E81\u1E83\u0175\u1E87\u1E85\u1E98\u1E89\u2C73]/g},
    {'base':'x','letters':/[\u0078\u24E7\uFF58\u1E8B\u1E8D]/g},
    {'base':'y','letters':/[\u0079\u24E8\uFF59\u1EF3\u00FD\u0177\u1EF9\u0233\u1E8F\u00FF\u1EF7\u1E99\u1EF5\u01B4\u024F\u1EFF]/g},
    {'base':'z','letters':/[\u007A\u24E9\uFF5A\u017A\u1E91\u017C\u017E\u1E93\u1E95\u01B6\u0225\u0240\u2C6C\uA763]/g}
];
AsalaeGlobal.removeDiacritics = function (str) {
    for (var i = 0; i < AsalaeGlobal.defaultDiacriticsRemovalMap.length; i++) {
        str = str.replace(
            AsalaeGlobal.defaultDiacriticsRemovalMap[i].letters,
            AsalaeGlobal.defaultDiacriticsRemovalMap[i].base
        );
    }
    return str;
}

/**
 * Manipule la casse d'une chaine selon certains standards
 */
class AsalaeInflector {
    /**
     * 1ere lettre en majuscule
     * @param word
     * @returns {string}
     */
    static ucfirst(word)
    {
        return word[0].toUpperCase() + word.substr(1);
    }

    /**
     * Returns the input lower_case_delimited_string as a CamelCasedString.
     * @param text
     * @param separator
     * @returns {string}
     */
    static camelize(text, separator = '_')
    {
        var words = text.split(separator);
        var result = [words[0]];
        words.slice(1).forEach((word) => result.push(AsalaeInflector.ucfirst(word)));
        var output = result.join('');
        return AsalaeInflector.ucfirst(result.join(''));
    }

    /**
     * Returns camelBacked version of an underscored string.
     * @param text
     * @param separator
     * @returns {string}
     */
    static variable(text, separator = '_')
    {
        var words = text.split(separator);
        var result = [words[0]];
        words.slice(1).forEach((word) => result.push(AsalaeInflector.ucfirst(word)));
        var output = result.join('');
        return result.join('');
    }

    /**
     * Expects a CamelCasedInputString, and produces a lower_case_delimited_string
     * @param str
     * @param separator
     * @returns {string}
     */
    static delimit(str, separator)
    {
        separator = typeof separator === 'undefined' ? '_' : separator;
        return str
            .replace(/([a-z\d])([A-Z])/g, '$1' + separator + '$2')
            .replace(/([A-Z]+)([A-Z][a-z\d]+)/g, '$1' + separator + '$2')
            .toLowerCase();
    }

    /**
     * Returns the input CamelCasedString as an underscored_string.
     * Also replaces dashes with underscores
     * @param str
     * @returns {string}
     */
    static underscore(str)
    {
        return AsalaeInflector.delimit(str.replace('-', '_'), '_');
    }

    /**
     * Returns the input CamelCasedString as an dashed-string.
     * Also replaces underscores with dashes
     * @param str
     * @returns {string}
     */
    static dasherize(str)
    {
        return AsalaeInflector.delimit(str.replace('_', '-'), '-');
    }
}

/**
 * Note: surchargé dans le head (html)
 * @type {string}
 */
var tinymceDefaultLanguage = 'fr_FR'

/**
 * Editeur HTML
 */
class AsalaeMce
{
    constructor(options = {}, colorChanger = null)
    {
        var that = this;
        this.tiny = {};
        options = $.extend(
            true,
            {
                selector: 'textarea.mce-small',
                menubar: false,
                plugins: [
                    'advlist autolink lists link image charmap print preview anchor',
                    'searchreplace visualblocks code fullscreen',
                    'insertdatetime media table paste help wordcount'
                ],
                // @see https://www.tiny.cloud/docs/advanced/editor-control-identifiers
                toolbar: 'undo redo | bold italic underline forecolor backcolor | fontsizeselect | bullist numlist | code',
                contextmenu: 'link | cut copy paste | formats removeformat | align | hr',
                branding: false,
                language: tinymceDefaultLanguage,
                setup: function (editor) {
                    editor.on(
                        'keyup',
                        function (e) {
                            editor.save();
                        }
                    );
                    editor.on(
                        'change',
                        function (e) {
                            editor.save();
                            $(editor.getElement()).trigger('change');
                        }
                    );
                }
            },
            options
        );

        $(options.selector).each(
            function () {
                var noptions = Object.assign({}, options);
                var id = $(this).attr('id');
                if (!id) {
                    id = 'as-mce-'+AsalaeMce.id_given;
                    $(this).attr('id', id);
                    AsalaeMce.id_given++;
                }
                noptions.selector = '#'+id;
                that.tinymce(noptions, colorChanger);
            }
        );

        $(document).off('.tinymodal').on(
            'focusin.tinymodal',
            function (e) {
                var dialog = $(e.target).closest(".tox-dialog");
                var modal = $(".modal:visible").last();
                if (dialog.length && modal.length && modal.find(dialog).length === 0) {
                    var wrapper = $('.tox-tinymce-aux');
                    modal.append(wrapper);
                }
            }
        );
    }

    tinymce(options, colorChanger)
    {
        var that = this;
        var id = options.selector;

        tinymce.init(options).then(
            function (result) {
                that.tiny[id] = result[0];
                if (colorChanger) {
                    $(colorChanger).change();
                }
            }
        );
        if (colorChanger) {
            $(colorChanger).change(
                function () {
                    var css = that.applyStyle($(colorChanger).val());
                    if (that.tiny[id]) {
                        that.tiny[id].getBody().setAttribute('style', css);
                    }
                }
            ).change();
        }
        var modal = $(id).closest('.modal');
        if (modal.length) {
            modal.one(
                'hidden.bs.modal',
                function () {
                    tinymce.remove(options.selector);
                    that.tiny[id] = null;
                }
            );
        }
    }

    applyStyle(option)
    {
        var css = {
            "color": "#333"
        };
        switch (option) {
            case 'alert-info':
                css["background-color"] = '#c8e5f3';
                break;
            case 'alert-warning':
                css["background-color"] = '#faf5d7';
                break;
            case 'alert-danger':
                css["background-color"] = '#eed5d5';
                break;
            case 'alert-success':
                css["background-color"] = '#d4eacb';
                break;
        }
        var output = [];
        for (var key in css) {
            output.push(key+': '+css[key]);
        }
        return output.join('; ');
    }
}
AsalaeMce.id_given = 0;

/**
 * Repliement des debug (importé de cakephp)
 */
AsalaeGlobal.collapseDebug = function () {
    $('.cake-dbg-collapse, .cake-dbg-copy').remove();
    $('.cake-dbg-array-items, .cake-dbg-object-props').each(
        function () {
            var node = this;
            // Hide the childnode container if it is not
            // a direct parent of the container.
            // noinspection JSUnresolvedVariable
            if (!node.parentNode.parentNode.classList.contains('cake-dbg')) {
                node.dataset.hidden = true;
            }

            // Don't show toggles for empty arrays/objects
            if (node.childNodes.length === 0) {
                return;
            }

            var collapser = document.createElement('a');
            collapser.classList.add('cake-dbg-collapse');
            // noinspection JSValidateTypes
            collapser.dataset.open = !node.dataset.hidden;
            collapser.setAttribute('href', '#')
            collapser.setAttribute('title', 'Toggle items');

            // Add open/close behavior
            collapser.addEventListener(
                'click',
                function (event) {
                    event.preventDefault();
                    event.stopPropagation();
                    node.dataset.hidden = node.dataset.hidden === 'true' ? 'false' : 'true';
                    collapser.dataset.open = collapser.dataset.open === 'true' ? 'false' : 'true';
                }
            );

            node.parentNode.insertBefore(collapser, node);
        }
    );

    $('.cake-dbg').each(
        function () {
            var container = this;
            var refLinks = container.querySelectorAll('.cake-dbg-ref');
            refLinks.forEach(
                function (ref) {
                    ref.addEventListener(
                        'click',
                        function (event) {
                            event.preventDefault();
                            event.stopPropagation();
                            var target = document.getElementById(ref.getAttribute('href').substr(1));

                            // Open the target element
                            var expander = target.querySelector('.cake-dbg-collapse');
                            if (expander.dataset.open === 'false') {
                                expander.click();
                            }
                            container.querySelectorAll('.cake-dbg-object').forEach(
                                function (el) {
                                    el.dataset.highlighted = 'false';
                                }
                            );
                            target.dataset.highlighted = 'true';

                            var current = target;
                            // Traverse up the tree opening all closed containers.
                            while (true) {
                                var parent = current.parentNode;
                                if (parent === container) {
                                    break;
                                }
                                if (parent.classList.contains('cake-dbg-object') || parent.classList.contains('cake-dbg-array')) {
                                    expander = parent.querySelector('.cake-dbg-collapse');
                                    if (expander.dataset.open === 'false') {
                                        expander.click();
                                    }
                                }
                                current = parent;
                            }
                        }
                    );
                }
            );
            var copy = document.createElement('a');
            copy.classList.add('cake-dbg-copy');
            copy.setAttribute('href', '#');
            copy.setAttribute('title', 'Copy contents of debug output');
            copy.appendChild(document.createTextNode('Copy'));

            // Add copy behavior
            copy.addEventListener(
                'click',
                function (event) {
                    event.preventDefault();
                    event.stopPropagation();
                    var lineNo = '';
                    if (container.parentNode && container.parentNode.classList.contains('cake-debug')) {
                        var line = container.parentNode.querySelector('span');
                        lineNo = line.textContent + "\n";
                    }
                    var value = lineNo + container.textContent.substring(0, container.textContent.length - 4);
                    navigator.clipboard.writeText(value);
                }
            );

            container.appendChild(copy);
        }
    );

    $('.cake-debug[data-open-all="true"]').each(
        function () {
            var block = this;
            block.querySelectorAll('.cake-dbg-collapse[data-open="false"]').forEach(
                function (el) {
                    el.click();
                }
            );
        }
    );
};

/**
 * Téléchargements avec feedback
 */
class AsalaeDownloads
{
    constructor(ratchetUrl) {
        this.ratchetUrl = ratchetUrl;
        this.downloads = {};
        this.debug = false;
        var that = this;
        AsalaeWebsocket.connect(
            ratchetUrl,
            function (session) {
                that.session = session;
                session.subscribe(
                    'download_file',
                    function (topic, data) {
                        if (Object.keys(that.downloads).indexOf(data.message.uuid) !== -1) {
                            that.updateState(data.message);
                        }
                    }
                );
            },
            function () {
            },
            {'skipSubprotocolCheck': true}
        );
    }

    updateState(data) {
        var uuid = data.uuid;
        this.downloads[uuid].state = data.state;
        this.downloads[uuid].last_update = data.last_update;
        this.downloads[uuid].integrity = data.integrity;

        if (this.debug) {
            console.log(this.downloads[uuid]);
        }
        if (data.state === 'finished' && data.integrity === false) {
            alert(
                __(
                    "Attention, le fichier téléchargé a été détecté non intègre.\n"
                    +"Ne tentez pas de l'utiliser et supprimez le des téléchargements.\n"
                    +"Merci de signaler cette anomalie à votre administrateur."
                )
            );
        }
        if (data.state === 'error') {
            alert(data.message);
        }
    }

    appendDownload(uuid, filename) {
        this.downloads[uuid] = {
            "uuid": uuid,
            "filename": filename,
            "state": 'pending',
            "date": Date.now()
        };
    }

    doDownload(element, uuid) {
        var d = document.createElement("a");
        var url = element.attr('href');
        url += url.indexOf('?') !== -1 ? '&' : '?';
        url += 'sessionFile=' + uuid;
        d.href = url;
        d.target = '=_blank';
        d.download = element.attr('download');
        document.body.appendChild(d);
        d.click();
        d.parentElement.removeChild(d);
    }

    handleDownloads() {
        var that = this;
        $('a[download]').off('.js-downloads').on('click.js-downloads', function(e) {
            e.preventDefault();
            var downloadUuid = uuid.v4();
            var element = $(this);
            that.appendDownload(downloadUuid, $(this).attr('download'));
            setTimeout(function() {
                that.doDownload(element, downloadUuid);
            }, 0);
        });
    }

    static getInstance(ratchetUrl = null) {
        ratchetUrl = ratchetUrl ? ratchetUrl : (window.PHP ? PHP.urls.ratchet : null);
        if (!AsalaeDownloads.instance) {
            AsalaeDownloads.instance = new AsalaeDownloads(ratchetUrl);
        }
        return AsalaeDownloads.instance;
    }

    static last() {
        var instance = AsalaeDownloads.getInstance();
        if (!instance) {
            return null;
        }
        return instance.downloads[Object.keys(instance.downloads).pop()];
    }

    static debug() {
        var instance = AsalaeDownloads.getInstance();
        instance.debug = true;
    }
}

/**
 * Gestion simplifiée des websockets
 * @type {object}
 */
AsalaeWebsocket = {
    session: null,
    connect: function(url, fn, params) {
        var that = this;
        if (this.session && AsalaeWebsocket.debug) {
            let currentSubscriptions = Object.keys(AsalaeWebsocket.session._subscriptions);
            let result = fn(this.session); // certainement vide
            let diff = Object.keys(AsalaeWebsocket.session._subscriptions).filter(
                x => currentSubscriptions.indexOf(x) < 0
            );
            for (let i = 0; i < diff.length; i++) {
                console.info('ws: subscribing on ' + diff[i]);
                this.session.subscribe(
                    diff[i],
                    function (topic, data) {
                        console.info('ws: ' + topic, data.message);
                    }
                );
            }
            return result
        } else if (this.session) {
            return fn(this.session);
        } else {
            if (AsalaeWebsocket.debug) {
                console.info('ws: connecting on ' + url);
            }
            ab.connect(
                url,
                function (newSession) {
                    that.session = newSession;
                    fn(newSession);
                    if (AsalaeWebsocket.debug) {
                        let subscriptions = Object.keys(AsalaeWebsocket.session._subscriptions);
                        for (let i = 0; i < subscriptions.length; i++) {
                            console.info('ws: subscribing on ' + subscriptions[i]);
                            that.session.subscribe(
                                subscriptions[i],
                                function (topic, data) {
                                    console.info('ws: ' + topic, data.message);
                                }
                            );
                        }
                    }
                }
            );
        }
    },
    subscribe: function(chanel, fn) {
        if (!this.session) {
            throw 'session not found';
        } else {
            this.session.subscribe(chanel, fn);
            if (AsalaeWebsocket.debug) {
                console.info('ws: subscribing on ' + chanel);
                this.session.subscribe(
                    chanel,
                    function (topic, data) {
                        console.info('ws: ' + topic, data.message);
                    }
                );
            }
        }
    },
    unsubscribe: function(chanel) {
        if (!this.session) {
            throw 'session not found';
        } else {
            if (Array.isArray(chanel)) {
                for (let i = 0; i < chanel.length; i++) {
                    this.unsubscribe(chanel[i]);
                }
                return;
            }
            try {
                this.session.unsubscribe(chanel);
                if (AsalaeWebsocket.debug) {
                    console.info('ws: unsubscribed ' + topic);
                }
            } catch (e) {
            }
        }
    },
    publish: function(chanel, data) {
        if (!this.session) {
            throw 'session not found';
        } else {
            this.session.publish(chanel, data);
            if (AsalaeWebsocket.debug) {
                console.info('ws: published ' + chanel, data);
            }
        }
    }
};

/**
 * Gestionnaire mercure
 */
class AsalaeMercure {
    static instance;
    debug = false;
    baseUrl;
    publishUrl;
    subscribed = {};
    eventSource;
    failedCount = 0;
    timeout;
    constructor(baseUrl, publishUrl = false)
    {
        this.baseUrl = baseUrl;
        this.publishUrl = publishUrl;
        AsalaeMercure.instance = this;
        window.addEventListener('beforeunload', () => {
            if (this.eventSource instanceof EventSource) {
                this.eventSource.close();
            }
        });
    }
    subscribe(chanel, fn)
    {
        if (this.debug) {
            console.debug('subscribed to: ' + chanel);
        }
        if (this.subscribed[chanel]) {
            this.unsubscribe(chanel);
        }
        this.subscribed[chanel] = fn;
        this.initEventSource(chanel);
    }
    unsubscribe(chanel, fn)
    {
        if (this.debug) {
            console.debug('unsubscribed to: ' + chanel);
        }
        if (this.timeout !== undefined) {
            clearTimeout(this.timeout);
        }
        if (this.subscribed[chanel]) {
            this.eventSource.close();
            delete this.subscribed[chanel];
            if (Object.keys(this.subscribed).length > 0) {
                this.initEventSource()
            }
        }
    }
    publish(chanel, data)
    {
        if (this.debug) {
            console.debug('published to: ' + chanel);
        }
        if (this.publishUrl) {
            fetch(
                this.publishUrl,
                {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json',
                    },
                    body: JSON.stringify({chanel: chanel, data: data}),
                }
            );
        }
    }
    onmessage(chanel, data, event)
    {
        if (this.debug) {
            console.debug('onmessage event to: ' + chanel + ' with data:', event.data);
        }
        if (this.subscribed[chanel] !== undefined) {
            this.subscribed[chanel](chanel, data);
        }
    }
    onerror(event)
    {
        if (this.debug) {
            console.debug('onerror event');
        }
        if (event.target.readyState === EventSource.CONNECTING) {
            this.eventSource.close();
            this.initEventSource();
        } else if (event.target.readyState === EventSource.CLOSED) {
            this.failedCount++;
            if (this.failedCount < 6) {
                this.timeout = setTimeout(() => this.initEventSource(), 5000);
            }
        }
    }
    initEventSource(chanel = null)
    {
        if (this.eventSource instanceof EventSource) {
            this.eventSource.close();
        }
        let topics = Object.keys(this.subscribed);
        if (chanel) {
            topics.push(chanel);
        }
        let queryParams = topics
            .map(chanel => 'topic=' + encodeURIComponent(chanel))
            .join('&');
        this.eventSource = new EventSource(
            this.baseUrl + '?' + queryParams,
            {withCredentials: true}
        );
        this.eventSource.onmessage = (event, d) => {
            let data = JSON.parse(event.data);
            this.onmessage(data.category, data, event);
        };
        this.eventSource.onerror = (event) => {
            this.onerror(event);
        };
    }
};

/**
 * Autofocus les champs date au clic du calendrier
 */
$(document).on(
    'click.datepicker',
    '.input-group-addon:has(.fa-calendar)',
    function () {
        $(this).closest('.form-group').find('input').focus();
    }
);

/**
 * Envoi du formulaire par CTRL+Enter
 */
$(document).on(
    'keypress',
    'form input, form textarea, form select',
    function (event) {
        if (event.originalEvent.ctrlKey && event.originalEvent.charCode) {
            var form = $(this).closest('form').first();
            form.trigger('beforeSubmit.modalForm', event);
            setTimeout(
                function () {
                    form.trigger('submit')
                        .trigger('afterSubmit.modalForm', event);
                },
                0
            );
        }
    }
);

if (typeof module !== 'undefined') {
    module.exports = {
        AsalaeDownloads: AsalaeDownloads,
        AsalaeGlobal: AsalaeGlobal,
        AsalaeInflector: AsalaeInflector,
        AsalaeLoading: AsalaeLoading,
        AsalaeMce: AsalaeMce,
        AsalaeNotifications: AsalaeNotifications,
        AsalaeWebsocket: AsalaeWebsocket,
        CustomMenu: CustomMenu
    };
}
