/*global Form, Infinity, TableGenerator*/

class XsdForm
{
    /**
     * Initialise la classe
     *
     * @param {object} xsdJson
     * @param {string|jquery|Element} jstreeDiv
     * @param {string|jquery|Element} formDiv
     * @param {string|jquery|Element} docDiv
     * @param {array} blackList
     */
    constructor(xsdJson, jstreeDiv, formDiv, docDiv, blackList = [])
    {
        this.xsd = xsdJson;
        this.icons = {
            attribute: '<i aria-hidden="true" class="fa fa-bookmark"></i>',
            element: '<i aria-hidden="true" class="fa fa-file-o"></i>',
            close: '<span class="pull-right"><i aria-hidden="true" class="fa fa-times slide-icon"></i></span>' // event click sur "fa-times"
        };
        this.jstree = $(jstreeDiv);
        this.form = $(formDiv);
        this.doc = $(docDiv);
        this.blackList = blackList;
        this.data = null;
        this.auto_open_count = 1;
        this.TableGenerator = null;
        this.tableGeneratorActions = {
            "action_0": {
                "href": "javascript:XsdForm.removeCallback('{0}', {1})",
                role: "button",
                "label": "<i aria-hidden=\"true\" class=\"fa fa-times text-danger\"><\/i>",
                "title": XsdForm.texts.removeTitle,
                "params": [
                    'target', 'instance'
                ]
            },
            "action_1": {
                "href": "javascript:XsdForm.openCallback('{0}', {1})",
                role: "button",
                "label": "<i aria-hidden=\"true\" class=\"fa fa-folder-open-o\"><\/i>",
                "title": XsdForm.texts.openTitle,
                "params": [
                    'target', 'instance'
                ]
            },
            "action_2": {
                "href": "javascript:XsdForm.viewdocCallback('{0}')",
                role: "button",
                "label": "<i aria-hidden=\"true\" class=\"fa fa-info-circle\"><\/i>",
                "title": XsdForm.texts.viewTitle,
                "params": [
                    'target'
                ]
            }
        };
        var thead = [
        {
            "_id": "key",
            "label": XsdForm.texts.element,
            "title": XsdForm.texts.element,
            "link": false,
            "type": "th",
            "data-view": "default",
            "display": true,
            "colspan": 1,
            "target": "key",
            "thead": null,
            "callback": null,
            "style": null
        },
        {
            "_id": "value",
            "label": XsdForm.texts.value,
            "title": XsdForm.texts.value,
            "link": false,
            "type": "th",
            "data-view": "default",
            "display": true,
            "colspan": 1,
            "target": "value",
            "thead": null,
            "callback": XsdForm.getValue,
            "style": null
        },
        {
            "_id": "elements",
            "label": XsdForm.texts.elements,
            "title": XsdForm.texts.elements,
            "link": false,
            "type": "th",
            "data-view": "default",
            "display": true,
            "colspan": 1,
            "target": "value",
            "thead": null,
            "callback": XsdForm.getElements,
            "style": null
        },
        {
            "_id": "args",
            "label": XsdForm.texts.args,
            "title": XsdForm.texts.args,
            "link": false,
            "type": "th",
            "data-view": "default",
            "display": true,
            "colspan": 1,
            "target": "value",
            "thead": null,
            "callback": XsdForm.getArgs,
            "style": null
        },
        {
            "_id": "actions",
            "label": "Actions",
            "title": "Actions",
            "link": false,
            "type": "th",
            "class": "action",
            "data-view": "column",
            "display": true,
            "colspan": 1,
            "target": "actions",
            "thead": {
                "action_0": {
                    "_id": "action_0",
                    "label": false,
                    "title": XsdForm.texts.removeTitle,
                    "link": false,
                    "type": "th",
                    "data-view": "default",
                    "display": true,
                    "colspan": 1,
                    "target": "action_0",
                    "thead": null,
                    "callback": null
                },
                "action_1": {
                    "_id": "action_1",
                    "label": false,
                    "title": XsdForm.texts.openTitle,
                    "link": false,
                    "type": "th",
                    "data-view": "default",
                    "display": true,
                    "colspan": 1,
                    "target": "action_1",
                    "thead": null,
                    "callback": null
                },
                "action_2": {
                    "_id": "action_2",
                    "label": false,
                    "title": XsdForm.texts.viewTitle,
                    "link": false,
                    "type": "th",
                    "data-view": "default",
                    "display": true,
                    "colspan": 1,
                    "target": "action_2",
                    "thead": null,
                    "callback": null
                }
            }
        }
        ];
        this.tableGeneratorParams = {
            "thead": [
                thead
            ],
            "tbody": {
                "identifier": "id",
                "checkbox": false
            }
        };
        if (typeof XsdForm.instance === 'undefined') {
            XsdForm.instance = [];
        }
        let i = 0;
        do {
            this.instanceIndex = i;
            i++;
        } while (typeof XsdForm.instance[this.instanceIndex] !== 'undefined');
        XsdForm.instance[this.instanceIndex] = this;
        XsdForm.lastInstance = this;
    }

    /**
     * Transforme les {"Foo": {"bar": "baz"}} en {"Foo": {"bar": {"@": "baz"}}}
     * Afin que toutes les valeurs se situe dans un objet sous '@', même lorsque
     * il n'y a pas d'autres attributs
     * @param {object} data
     * @returns {object} data
     * @private
     */
    static _format(data)
    {
        if (typeof data === 'object') {
            for (let key in data) {
                if (typeof data[key] === 'object') {
                    data[key] = XsdForm._format(data[key]);
                } else if (typeof data[key] === 'string' && key[0] !== '@') {
                    data[key] = {'@': data[key]};
                }
            }
        }
        return data;
    }

    /**
     * Permet de visualiser l'ordre d'execution des nombreux évenements de jstree
     * @param {jQuery} element
     * @return {jQuery} element
     */
    static debugJstreeEvents(element)
    {
        const test = [
            'init', 'loading', 'loaded', 'changed', 'ready', 'load_node', 'model',
            'redraw', 'before_open', 'after_open', 'open_node', 'after_close',
            'close_node', 'open_all', 'close_all', 'enable_node', 'disable_node',
            'activate_node', 'hover_node', 'dehover_node', 'select_node', 'deselect_node',
            'select_all', 'deselect_all', 'set_table', 'refresh', 'refresh_state',
            'set_next', 'create_node', 'rename_node', 'delete_node', 'move_node',
            'copy_node', 'cut', 'copy', 'paste', 'set_theme', 'show_contextmenu',
            'show', 'hide', 'mousedown', 'scroll', 'start', 'move', 'stop', 'search',
            'clear_search', 'restore_search', 'click', 'dblclick', 'keydown', 'focus',
            'mouseenter', 'mouseleave'
        ];
        for (let i = 0; i < test.length; i++) {
            $(element).on(
                test[i],
                function () {
                    console.info(test[i]);
                }
            );
            $(element).on(
                test[i]+'.jstree',
                function () {
                    console.info(test[i]+'.jstree');
                }
            );
        }
        return element;
    }

    /**
     * Construit/Reconstruit l'arbre du xml
     *
     * @param {object} data
     * @returns {XsdForm}
     */
    update(data = null)
    {
        var that = this;
        $(this).trigger('data_changed');
        if (!data) {
            data = this.data;
        } else {
            this.data = XsdForm._format(data);
        }
        var tree = $('<div class="jstree_tree"></div>').jstree(
            {
                core: {data: this._buildTree(data)},
                plugins : [
                "contextmenu", // menu clic droit
                "search", // Recherche par nom de noeud
                "wholerow" // sélection sur la ligne (clic facilité et affichage + moderne)
                ],
                contextmenu: {
                    select_node: false,
                    show_at_node: true,
                    items: function (node) {
                        if (tree.hasClass('disabled')) {
                            return {};
                        }
                        var items = {};
                        var treeInstance = tree.jstree(true);

                        /**
                         * Action add
                         */
                        var submenu = {};
                        var name;

                        var rules = that._getRulesById(node.original.id),
                        options = {},
                        select,
                        data = that._getDataById(node.original.id);

                        if (rules) {
                            let possibleOptions = rules.elements;
                            for (let key in possibleOptions) {
                                let max = possibleOptions[key].maxOccurs,
                                count = XsdForm._countSpecificChildNodes(data, key);
                                max = AsalaeGlobal.is_numeric(max) ? parseInt(max, 10) : Infinity;
                                if (count < max
                                    && XsdForm._canAddChoosableElement(data, key, rules)
                                    && that.blackList.indexOf(key) === -1
                                ) {
                                    options[key] = key;
                                }
                            }
                        }
                        for (var key in options) {
                            name = options[key].indexOf('/') !== -1
                            ? options[key].substr(options[key].lastIndexOf('/') +1)
                            : options[key];
                            submenu[name] = (function (name) {
                                return {
                                    "separator_before": false,
                                    "separator_after": false,
                                    "label": name,
                                    "icon": "fa fa-angle-right",
                                    "action": function (obj) {
                                        var rules = that._getRulesById(node.original.id).elements;
                                        var max;
                                        for (let skey in rules) {
                                            if (skey === name || skey.indexOf('/'+name) >= 0) {
                                                max = rules[skey].maxOccurs;
                                                break;
                                            }
                                        }
                                        var data = that._getDataById(node.original.id);
                                        var count = XsdForm._countSpecificChildNodes(data, name);
                                        var id = node.original.id + '_' + name;
                                        if (max !== '1' && count) {
                                            id += '_' + count;
                                        }
                                        that._createNode(id);
                                        that._buildForm(id);
                                    }
                                };
                            })(name);
                        }
                        if (Object.keys(submenu).length) {
                            items.Add = {
                                "separator_before": false,
                                "separator_after": false,
                                "label": __("Ajouter"),
                                "icon": "fa fa-plus text-success",
                                "submenu": submenu
                            };
                        }

                        /**
                         * Action delete
                         */
                        items.Remove = {
                            "separator_before": true,
                            "separator_after": false,
                            "label": __("Supprimer"),
                            "icon": "fa fa-trash text-danger",
                            "action": function (obj) {
                                if (confirm(__("Vous êtes sur le point de supprimer un nœud et tout ce qu'il contient. Cette action ne pourra pas être annulée. Voulez-vous continuer ?"))) {
                                    let targetData = that._getDataById(node.original.id);
                                    let parentId = node.original.id.substr(0, node.original.id.lastIndexOf('_'));
                                    let targetParent = that._getDataById(parentId);
                                    for (let key in targetParent) {
                                        if (targetParent[key] === targetData) {
                                            if (Array.isArray(targetParent)) {
                                                targetParent.splice(key, 1);
                                            } else {
                                                delete targetParent[key];
                                            }
                                            break;
                                        }
                                    }
                                    that.update();
                                    that.jstree.find('> .jstree').one(
                                        'loaded.jstree',
                                        function () {
                                            if (Array.isArray(targetParent)) {
                                                that.openNode(parentId.substr(0, parentId.lastIndexOf('_')));
                                            } else {
                                                that.openNode(parentId);
                                            }
                                        }
                                    );
                                }
                            }
                        }
                        return items;
                    }
                }
            }
        ).on(
            'select_node.jstree',
            function (event, data, foo) {
                if (data.event === 'ignore') {
                    return;
                }
                if (data && data.selected && data.selected.length) {
                    that._buildForm(data.node.id);
                    that.openNode(data.node.id);
                }
            }
        ).on(
            'loaded.jstree',
            function () {
                    that.jstree.trigger('loaded.jstree');
                    $(that.jstree).find('.jstree-anchor').on(
                        'click',
                        function (event, data) {
                            let modal = $(that.jstree).closest('.modal').get(0);
                            modal.style.opacity = 0.8;
                        // Donne le temps à l'animation de se lancer
                            event.stopPropagation();
                            var anchor = $(this);
                            setTimeout(
                                function () {
                                    anchor.trigger('click.jstree');
                                    modal.style.opacity = 1;
                                },
                                0
                            );
                        }
                    );
                    XsdForm.accessibleContextMenu(that)();
            }
        ).on(
            'activate_node.jstree',
            function () {
                    $(that.jstree).closest('.modal').fadeTo(200, 1).css('cursor', '');
            }
        ).on('after_open.jstree', XsdForm.accessibleContextMenu(this));
        this.jstree.empty().append(tree);

        return this;
    }

    /**
     * Rend le context_menu (clic droit) accéssible au clavier (Ctrl, Alt, + ou ²)
     */
    static accessibleContextMenu(XsdFormObject)
    {
        return function () {
            XsdFormObject.jstree.find('.jstree-anchor').off('.accessibility').on(
                'keydown.accessibility',
                function (e) {
                /// [Ctrl], [Alt], [+], [²]
                    if ([17, 18, 107, 192].indexOf(e.keyCode) >= 0) {
                        e.preventDefault();
                        e.stopPropagation();
                        $(this).trigger('contextmenu.jstree');
                    }
                }
            ).on(
                'contextmenu.jstree.accessibility',
                function () {
                        var contextMenu = $('ul.vakata-context');
                        var modal = $(XsdFormObject.jstree.closest('.modal'));

                    // Rend le context-menu "focusable" car il n'est plus derriere la modale
                        setTimeout(
                            function () {
                            // Repositionne le menu par rapport à la modale
                                contextMenu.css('top', contextMenu.position().top - modal.offset().top);
                                modal.append(contextMenu);
                            // Focus le 1er element du menu
                                contextMenu.find('> li.vakata-context-separator > a').attr('tabindex', '-1');
                                contextMenu.find('> li:not(.vakata-context-separator) > a').first().focus();

                            // Corrige le problème du menu hors du cadre
                                var enableMove = true;
                                contextMenu.on(
                                    'mousemove.fixtop',
                                    function () {
                                        if (enableMove === false) {
                                            return;
                                        }
                                        enableMove = false;
                                        setTimeout(
                                            function () {
                                                $(contextMenu).find('> li > ul').each(
                                                    function () {
                                                        var current = $(this).offset().top - $(document).scrollTop();
                                                        if (current < 0) {
                                                            $(this).css('top', '').css('bottom', '');
                                                        }
                                                    }
                                                );
                                            },
                                            0
                                        );
                                        setTimeout(
                                            function () {
                                                enableMove = true;
                                            },
                                            10
                                        );
                                    }
                                );
                            },
                            0
                        );
                }
            );
        }
    }

    /**
     * @see http://jsfiddle.net/53cvtbv9/1/
     * @param {object} data
     * @param {string} id
     * @return {object}
     */
    _buildTree(data, id = '')
    {
        var obj = false;
        if (typeof data === 'object') {
            let rules = id.length > 0 ? this._getRulesById(id) : {};
            obj = [];
            for (let key in data) {
                if (this.blackList.indexOf(key) !== -1 || key === '@') {
                    continue;
                }
                let count = 0;
                if (Array.isArray(data[key])) {
                    for (let i = 0; i < data[key].length; i++) {
                        obj.push(
                            {
                                "id": (id ? id+"_"+key+'_'+i : key+'_'+i).replace(/@/g, ':at:'),
                                "text": key,
                                "icon": key.indexOf('@') === 0 ? "fa fa-tags text-info" : "fa fa-folder",
                                "state": {
                                    "opened": (id.match(/_/g) || []).length < this.auto_open_count, // Ouvre les premiers dossiers
                                    "disabled": false,
                                    "selected": false
                                },
                                "children": this._buildTree(data[key][i], id ? id+"_"+key+'_'+i : key+'_'+i)
                            }
                        );
                        count++;
                    }
                } else {
                    obj.push(
                        {
                            "id": (id ? id+"_"+key : key).replace(/@/g, ':at:'),
                            "text": key,
                            "icon": key.indexOf('@') === 0 ? "fa fa-tags text-info" : "fa fa-folder",
                            "state": {
                                "opened": (id.match(/_/g) || []).length < this.auto_open_count, // Ouvre les premiers dossiers
                                "disabled": false,
                                "selected": false
                            },
                            "children": this._buildTree(data[key], id ? id+"_"+key : key)
                        }
                    );
                    count++;
                }
            }
        }
        return obj;
    }

    /**
     * Permet d'obtenir un lien (info) pour afficher la doc
     * @param {string} id
     * @returns {*|void|jQuery}
     * @private
     */
    static _getInfoLink(id)
    {
        var filteredPath = id.replace(/:at:/g, '@').split('_').filter(
            function (a) {
                return !AsalaeGlobal.is_numeric(a) && a.length > 0;
            }
        );
        var targetDoc = filteredPath.pop();
        if (targetDoc === '@') {
            targetDoc = filteredPath.pop();
        }
        if ($('[data-id="'+targetDoc+'"]').length === 0) {
            return '';
        }
        return XsdForm._getDocById(id) ?
            $('<a href="javascript:" title="'+XsdForm.texts.doc+'" role="button"><i aria-hidden="true" class="fa fa-info-circle"></i> </a>')
                .append('<span class="sr-only">'+XsdForm.texts.doc+'</span>')
                .click(
                    function () {
                        var focused = $('*:focus');
                        $(this).closest('.modal').append(XsdForm.viewdocCallback(id));
                        $('.doc-tooltip .close').focus().click(
                            function () {
                                focused.focus();
                            }
                        );
                    }
                )
            : ''
            ;
    }

    /**
     * Permet de créer le formulaire de modification d'un noeud
     *
     * @param {string} id
     */
    _buildForm(id)
    {
        var that = this,
            path = id.replace(/:at:/g, '@').split('_'),
            title = (!AsalaeGlobal.is_numeric(path[path.length -1]) ? path[path.length -1] : path[path.length -2]).replace(/:at:/g, '@'),
            table = $('<table class="table table-striped table-hover minifiable-target"></table>'),
            data = XsdForm._getDataByPath(this.data, path),
            rules = id.indexOf('_:at:') > 0
                ? this._getRulesById(id.substr(0, id.indexOf('_:at:')))
                : this._getRulesById(id),
            filteredPath = id.replace(/:at:/g, '@').split('_').filter(
                function (a) {
                    return !AsalaeGlobal.is_numeric(a) && a.length > 0;
                }
            );
        this.form.empty().append($('<h2>'+title+'</h2>'));
        this.form.append(this._appendAriane(id));

        // Si attribut
        if (path[path.length -1][0] === '@') {
            let dataName = '@',
                value = typeof data[dataName] !== 'undefined' ? data[dataName] : '',
                input = Form.input(
                    dataName,
                    {
                        id: id+'_value',
                        name: id+'_value',
                        label: filteredPath[filteredPath.length -1],
                        value: value,
                        fullname: id
                    }
                );
            input.find('input').change(
                function () {
                    that._setDataAndSelectParent(id, $(this).val());
                }
            );
            this.form.append(input);
            $(XsdForm._getInfoLink(id)).insertBefore(input.find('label'), null);
            return;
        }

        if (!rules || rules.content !== 'element-only') {
            let input = this._renderValue(id, data, rules);

            this.form.append(input).append('<hr>');
            $(XsdForm._getInfoLink(id)).insertBefore(input.find('label'), null);
        } else if (rules.attributes) {
            let list = $('<div class="attributesList"></div>');
            for (let key in rules.attributes) {
                // m.id/localType => @localType || xml:id => @xml:id
                let baseName = key.substr(key.lastIndexOf('/') +1),
                    dataName = '@' + baseName,
                    idName = id + '_' + ':at:' + baseName,
                    value = typeof data[dataName] !== 'undefined' ? data[dataName] : '',
                    input = Form.input(
                        dataName,
                        {
                            id: idName+'_value',
                            name: idName+'_value',
                            value: value,
                            fullname: idName
                        }
                    );
                input.find('input').change(
                    function () {
                        that._setDataAndSelectParent(idName, $(this).val());
                    }
                );
                $(XsdForm._getInfoLink(idName)).insertBefore(input.find('label'), null);
                list.append(input);
            }
            this.form.append(list).append('<hr>');
        }

        if (rules) {
            for (let key in rules.elements) {
                let subId = id + '_' + key,
                    subRules = this._getRulesById(subId);
                if (subRules.content !== 'element-only') {
                    let subPath = path.slice();
                    subPath.push(key);
                    let input = this._renderValue(subId, XsdForm._getDataByPath(this.data, subPath), subRules);
                    this.form.append(input);
                    $(XsdForm._getInfoLink(subId)).insertBefore(input.find('label'), null);
                }
            }
        } else {
            for (let key in data) {
                if (key[0] !== '@') {
                    let subId = id + '_' + key,
                        subPath = path.slice();
                    subPath.push(key);
                    let input = this._renderValue(subId, XsdForm._getDataByPath(this.data, subPath), false);
                    this.form.append(input);
                }
            }
        }

        this.doc.html(XsdForm._getDocById(id));
    }

    /**
     * Handler permettant de charger/afficher/cacher la liste d'attributs
     * @param {object} event
     * @param {string} id
     * @param {string} attributes
     */
    toggleAttributes(event, id, attributes)
    {
        event.preventDefault()
        var container = $(event.target).closest('div.form-group'),
            path = id.replace(/:at:/g, '@').split('_'),
            data = XsdForm._getDataByPath(this.data, path),
            list = container.find('.attributesList'),
            that = this
        ;
        attributes = JSON.parse(attributes)
        if (list.length > 0) {
            list.slideToggle();
        } else {
            list = $('<div class="attributesList" style="display: none;"></div>');
            for (let key in attributes) {
                // m.id/localType => @localType || xml:id => @xml:id
                let baseName = key.substr(key.lastIndexOf('/') +1),
                    dataName = '@' + baseName,
                    idName = id + '_' + ':at:' + baseName,
                    value = data && typeof data[dataName] !== 'undefined' ? data[dataName] : '',
                    input = Form.input(
                        dataName,
                        {
                            id: idName+'_value',
                            name: idName+'_value',
                            value: value,
                            fullname: idName
                        }
                    );
                input.find('input').change(
                    function () {
                        that._setDataAndSelectParent(idName, $(this).val());
                    }
                );
                list.append(input);
            }
            container.append(list);
            list.slideDown();
        }
    }

    /**
     * Permet d'obtenir le label, qui comporte les attributs du champ si besoin
     * @param {string} id
     * @param {object} data
     * @param {object} rules
     * @private
     */
    _getAttributesIcon(id, data, rules)
    {
        if (rules.attributes && Object.keys(rules.attributes).length > 0) {
            var instance = 'XsdForm.instance['+this.instanceIndex+']',
                title = XsdForm.texts.toggleAttributesTitle,
                dataString = JSON.stringify(data).replace(/(")/g, '\\$1').replace(/'/g, '&quote;').replace(/\\([ntr])/g, '\\\\$1'),
                ruleAttrString = JSON.stringify(rules.attributes).replace(/(")/g, '\\$1');

            return '<a href="javascript:" onclick=\''+instance+'.toggleAttributes(event, \"'+id+'\", \"'+ruleAttrString+'\")\' title="'+title+'" role="button"> <i class="fa fa-tags" aria-hidden="true"></i><span class="sr-only">'+title+'</span></a>';
        } else {
            return '';
        }
    }

    /**
     * Effectue le rendu HTML de la valeur
     * @param {string} id
     * @param {object} data
     * @param {object} rules
     * @private
     */
    _renderValue(id, data, rules)
    {
        var that = this,
            output
        ;
        if (typeof data === 'undefined' || (Array.isArray(data) && data.length === 0)) {
            data = {'@': ''};
        }
        if (Array.isArray(data)) {
            let group = $('<div class="multi-value-group"></div>');
            for (let i = 0; i < data.length; i++) {
                group.append(this._renderValue(id+'_'+i, data[i], rules));
            }
            return group;
        }

        if (!rules) {
            let filteredId = id.replace(/:at:/g, '@').split('_').filter(
                function (a) {
                    return !AsalaeGlobal.is_numeric(a) && a.length > 0;
                }
            ).join('_');
            rules = {
                attributes: {},
                traduction: filteredId.substr(filteredId.lastIndexOf('_') +1),
                enumeration: []
            };
            for (let key in data) {
                if (key[0] === '@' && key.length > 1) {
                    rules.attributes[key.substr(1)] = {use: 'optional'};
                }
            }
        }

        // Select de la valeur
        if (rules.enumeration.length > 0) {
            let enumeration = {};
            for (let i=0; i<rules.enumeration.length; i++) {
                enumeration[rules.enumeration[i]] = rules.enumeration[i];
            }
            let select = Form.select(
                rules.traduction,
                {
                    id: id+'_value',
                    name: id+'_value',
                    label: rules.traduction,
                    value: data['@'],
                    fullname: id,
                    options: enumeration
                }
            );
            select.find('select').change(
                function () {
                    that._setDataAndSelectParent(id+'_@', $(this).val());
                }
            );
            $(this._getAttributesIcon(id, data, rules)).insertAfter(select.find('label'));
            output = select;

            // Saisie manuelle de la valeur
        } else {
            let textarea = Form.textarea(
                rules.traduction,
                {
                    id: id+'_value',
                    name: id+'_value',
                    label: rules.traduction,
                    value: data && data['@'] ? data['@'] : '',
                    fullname: id
                }
            );
            textarea.find('textarea').change(
                function () {
                    that._setDataAndSelectParent(id+'_@', $(this).val());
                }
            );
            $(this._getAttributesIcon(id, data, rules)).insertAfter(textarea.find('label'));
            output = textarea;
        }

        return output;
    }

    /**
     * Défini la valeur d'un noeud et selectionne le noeud parent
     * @param id
     * @param value
     * @private
     */
    _setDataAndSelectParent(id, value)
    {
        if (this._setDataById(id, value)) {
            this.jstree.find('> .jstree').one('loaded.jstree', () => this.openNode(XsdForm._getParentId(id), 'ignore'));
        }
    }

    /**
     * Donne l'id du parent
     * @param id
     * @return {string}
     * @private
     */
    static _getParentId(id)
    {
        var path = id.split('_');
        do {
            let key = path.pop();
            if (!AsalaeGlobal.is_numeric(key)) {
                break;
            }
        } while (path.length);
        return path.join('_');
    }

    /**
     * Récupère la documentation dans la page si elle existe sous le terme
     * data-id="elementName"
     * @param {string} id
     * @returns {jQuery}
     * @private
     */
    static _getDocById(id)
    {
        var filteredPath = id.replace(/:at:/g, '@').split('_').filter(
            function (a) {
                return !AsalaeGlobal.is_numeric(a) && a.length > 0;
            }
        );
        var targetDoc = filteredPath.pop();
        if (targetDoc === '@') {
            targetDoc = filteredPath.pop();
        }
        return $('[data-id="'+targetDoc+'"]').html();
    }

    /**
     * Permet d'obenir le contenu de data à partir d'un chemin sous forme d'array
     * Ex : si path == ['foo', 'bar'], alors on retourne data.foo.bar
     *
     * @param {object} data
     * @param {Array} path
     * @returns {object|string}
     */
    static _getDataByPath(data, path)
    {
        var newData = eval("data['"+path.join("']['")+"']");
        if (typeof newData === 'string') {
            newData = {"@": newData};
        }
        return newData;
    }

    /**
     * Permet d'obtenir la/les valeur(s) selon un id donné
     *
     * @param {string} id
     * @param {string} mask
     * @returns {object|string}
     */
    _getDataById(id, mask = '_')
    {
        return XsdForm._getDataByPath(this.data, id.split(mask));
    }

    /**
     * Permet de modifier la valeur selon un id donné
     *
     * @param {string} id
     * @param {string|object} value
     * @returns {XsdForm}
     */
    _setDataById(id, value)
    {
        var that = this,
            path = id.replace(/:at:/g, '@').split('_'),
            evalString = 'that.data',
            hasChanged = false,
            testedVar = this.data,
            evaluated = undefined
        ;

        // Assure l'existance de la donné parente
        for (let i = 0; i < path.length; i++) {
            let defaultValue = path[i][0] === '@' ? '' : {};
            if (evaluated === '') {
                eval(evalString + " = {}");
            }
            evalString += '["'+path[i]+'"]';
            evaluated = eval(evalString);
            if (typeof evaluated === "undefined" || (Array.isArray(evaluated) && evaluated.length === 0)) {
                eval(evalString + " = defaultValue");
                hasChanged = true;
            }
        }
        if (value === '') {
            this._unsetById(id);
            hasChanged = true;
        } else {
            eval(evalString + " = value");
        }
        if (hasChanged) {
            this.update();
        }
        $(this).trigger('data_changed');
        return hasChanged;
    }

    /**
     * Supprime une valeur en fonction de son id
     * @param {string} id
     */
    _unsetById(id)
    {
        var that = this,
            path = id.replace(/:at:/g, '@').split('_'),
            fullPath = path.slice(),
            last = path.pop(),
            beforeLast = path.pop(),
            data = eval('that.data["'+path.join('"]["')+'"]');

        // Cas de suppression d'un argument/valeur dans element multiple (array)
        if (last[0] === '@' && Array.isArray(data)
            && typeof data[beforeLast] === 'object'
            && Object.keys(data[beforeLast]).length === 1
        ) {
            eval('that.data["'+path.join('"]["')+'"].splice('+beforeLast+', 1)');
        } else if (typeof data[beforeLast] === 'object' && Object.keys(data[beforeLast]).length === 1) {
            eval('delete that.data["' + path.join('"]["') + '"]["'+beforeLast+'"]');
        } else {
            eval('delete that.data["' + fullPath.join('"]["') + '"]');
        }
    }

    /**
     * Initialize un noeud
     * @param {string} id
     * @private
     */
    _createNode(id)
    {
        var that = this,
            match = id.match(/^(.*)_[\d]+$/);
        if (!match) {
            match = [id, id, '0'];
        }
        let data = this._getDataById(match[1]),
            evalString = match[1].replace(/_/g, "']['");
        if (Array.isArray(data)) {
            let existings = eval("that.data['"+evalString+"']");
            for (let key in existings) {
                if (existings[key] === id) {
                    break;
                }
                // sélectionne un noeud vide (si existant) plutôt qu'ajouter un autre noeud
                if (Object.keys(existings[key]).length === 0
                    || (Object.keys(existings[key]).length === 1
                    && typeof existings[key]['@'] !== 'undefined'
                    && !existings[key]['@'])
                ) {
                    that.openNode(match[1]+'_'+key);
                    this.jstree.find('> .jstree').jstree('deselect_node', id);
                    return;
                }
            }
        }
        this._addElementToTargetId(match[1].substr(match[1].lastIndexOf('_') +1), XsdForm._getParentId(match[1]));
        this.jstree.one(
            'loaded.jstree',
            function () {
                that.openNode(id);
            }
        );
        this.update();
    }

    /**
     * Ouvre un noeud avec tout ses parents
     * @param {array} path
     * @param event
     */
    openNode(path, event = undefined)
    {
        var targetPath = [],
            id = '',
            jstree = this.jstree.find('> .jstree');
        if (typeof path === 'string') {
            path = path.split('_');
        }
        for (let i = 0; i < path.length; i++) {
            targetPath.push(path[i]);
            id = '#'+targetPath.join('_');
            if ($(id).hasClass('jstree-closed')) {
                jstree.jstree('open_node', id);
            }
        }
        jstree.jstree('select_node', id, false, true, event);
    }

    /**
     * Action qui permet de retirer une valeur
     *
     * @param {string} id Identifiant de l'element à retirer
     * @param {number} instance Numéro de l'instance de classe (XsdForm.instance[instance])
     */
    static removeCallback(id, instance)
    {
        var mainId = id.substr(0, id.lastIndexOf('_')),
            index = id.substr(id.lastIndexOf('_') +1),
            targetData = "XsdForm.instance["+instance+"].data['"+mainId.replace(/_/g, "']['")+"']",
            data = eval(targetData);
        if (confirm(XsdForm.texts.removeConfirm)) {
            if (data.length > 1 && Array.isArray(data)) {
                eval(targetData+'.splice('+index+', 1)');
            } else {
                eval('delete '+targetData);
            }
            XsdForm.instance[instance].jstree.one(
                "loaded.jstree",
                function () {
                    XsdForm.openCallback(mainId.substr(0, mainId.lastIndexOf('_')), instance);
                }
            );
            XsdForm.instance[instance].update();
        }
    }

    /**
     * Action qui permet de consulter une valeur
     *
     * @param {string} id Identifiant de l'element à consulter
     * @param {number} instance Numéro de l'instance de classe (XsdForm.instance[instance])
     */
    static openCallback(id, instance)
    {
        var jstree = $('.jstree');
        id = id.replace(/@/g, ':at:');
        jstree.jstree('deselect_all');
        if (AsalaeGlobal.is_numeric(id.substr(id.lastIndexOf('_') +1))) {
            jstree.jstree('select_node', id.substr(0, id.lastIndexOf('_')));
        }
        jstree.jstree('select_node', id);
    }

    /**
     * Affiche une fenetre avec la documentation de l'element choisi
     * @param {string} id
     */
    static viewdocCallback(id)
    {
        var docWindow = $('<div class="bg-white doc-tooltip"></div>')
            .append(
                $('<div class="handle"></div>').append(
                    $('<a class="close" href="#"><span aria-hidden="true">&times;</span><span class="sr-only">'+XsdForm.texts.close+'</span></a>').click(
                        function (event) {
                            event.preventDefault();
                            $('.doc-tooltip').remove();
                        }
                    )
                )
            )
            .append(
                $('<div class="content"></div>').append(XsdForm._getDocById(id))
            )
            .append($('<div class="bottom-handle"></div>').css('cursor', 'move'))
            .draggableCustom(
                {
                    handle: ".handle, .bottom-handle"
                }
            )
            .css({position: 'fixed', 'z-index': 1000000, 'max-height': 600, top: 50, left: 50, right: 50, bottom: 50});
        $(window).off('keydown.tooltip').on(
            'keydown.tooltip',
            function (event) {
                if (event.originalEvent.keyCode === 27) { // Escape
                    $('.doc-tooltip').remove();
                }
            }
        );
        $('.doc-tooltip').remove();
        return docWindow;
    }

    /**
     * Génère le bouton qui permet d'ajouter une valeur
     *
     * @returns {jQuery}
     */
    _genAddBtn()
    {
        var that = this;
        return $('<button type="button">'+XsdForm.texts.add+'</button>').click(
            function () {
                var select = $(this).parent().find('select'),
                values = select.val(),
                target = select.attr('data-fullname');
                if (!Array.isArray(values) || values.length < 1) {
                    return;
                }
                for (let i = 0; i < values.length; i++) {
                    that._addElementToTargetId(values[i], target);
                }
                that.update();
                $('.jstree').one(
                    "loaded.jstree",
                    function () {
                        $(this).jstree('select_node', target);
                    }
                );
            }
        );
    }

    _genDocBtn()
    {
        var that = this;
        return $('<button type="button" title="'+XsdForm.texts.doc+'"><i aria-hidden="true" class="fa fa-info-circle"></i></button>').click(
            function () {
                var select = $(this).parent().find('select'),
                values = select.val(),
                target = select.attr('data-fullname');
                if (!Array.isArray(values) || values.length < 1) {
                    return;
                }
                XsdForm.viewdocCallback(values[0]);
            }
        );
    }

    /**
     * Permet d'obtenir les règles d'un element en fonction de son identifiant
     * ex: eac-cpf.cpfDescription.identity.nameEntry renverra this.xsd.elements['identity/nameEntry'] (cas spécial)
     * ex: eac-cpf.cpfDescription renverra this.xsd.elements['cpfDescription'] (cas normal)
     * @param {string} id
     * @returns {object|bool}
     */
    _getRulesById(id)
    {
        var pointer = 0,
            rules = false,
            that = this,
            path = id.replace(/:at:|@/g, '').split('_').filter(
                function (a) {
                    return !AsalaeGlobal.is_numeric(a) && a.length > 0;
                }
            );
        for (let i = 0; i < path.length; i++) {
            let t = [];
            for (let j = i; j < path.length; j++) {
                t.push(path[j]);
            }
            try {
                rules = eval("that.xsd.elements['"+t.join('/')+"']");
            } catch (e) {
                rules = false;
            }
            if (rules) {
                break;
            }
        }
        return rules;
    }

    /**
     * Génère le select qui permet d'ajouter des elements dand un element
     *
     * @param {string} id
     * @returns {jQuery}
     */
    _selectAppendChild(id)
    {
        var rules = this._getRulesById(id),
            options = {},
            select,
            data = this._getDataById(id);

        if (rules) {
            let possibleOptions = rules.elements;
            for (let key in possibleOptions) {
                let max = possibleOptions[key].maxOccurs,
                    count = XsdForm._countSpecificChildNodes(data, key);
                if (!AsalaeGlobal.is_numeric(max)) {
                    max = Infinity;
                }
                if (count < max
                    && XsdForm._canAddChoosableElement(data, key, rules)
                    && this.blackList.indexOf(key) === -1
                ) {
                    options[key] = key;
                }
            }
            possibleOptions = rules.attributes;
            for (let key in possibleOptions) {
                let count = XsdForm._countSpecificChildNodes(data, key);
                if (count === 0) {
                    options['@'+key] = '@'+key;
                }
            }
        }

        select = Form.select(
            XsdForm.texts.add,
            {
                id: id+'_select_add',
                fullname: id,
                options: options,
                multiple: true
            }
        );
        select.append(this._genDocBtn());
        select.append(this._genAddBtn());
        return select;
    }

    /**
     * Donne le nombre de nodeName que contient data selon les deux possibilités
     * de structure :
     * data = [{nodeName: 'foo'}, {nodeName: 'bar'}] -> count 2 (data[i].nodeName)
     * data = {nodeName: 'foo', bar: 'baz'} -> count 1 (data.nodeName)
     *
     * @param {Array|object} data
     * @param {string} nodeName
     * @returns {Number}
     */
    static _countSpecificChildNodes(data, nodeName)
    {
        var count = 0;
        nodeName = nodeName.indexOf('/') ? nodeName.substr(nodeName.lastIndexOf('/') +1) : nodeName;
        for (let key in data) {
            if (key === nodeName && Array.isArray(data[key])) {
                count += data[key].length;
            } else if (key.replace('@', '') === nodeName || typeof data[key][nodeName] !== 'undefined' || data[key] === nodeName) {
                count++;
            }
        }
        return count;
    }

    /**
     * Si l'element fait parti d'un choix, le choix ne doit pas avoir été fait
     * au préalable.
     * Ex: si on a le choix entre foo et bar, et qu'on veut ajouter bar, il faut
     * que ni foo, ni bar ne soit présent
     *
     * @param {object} data
     * @param {string} nodeName
     * @param {object} rules
     * @returns {boolean}
     */
    static _canAddChoosableElement(data, nodeName, rules)
    {
        var compatible = true;
        if (typeof rules.choice === 'undefined') {
            return true;
        }
        for (let i = 0, c = rules.choice.length; i < c; i++) {
            if (rules.choice[i].indexOf(nodeName) === -1) {
                continue;
            }
            for (let j = 0; j < rules.choice[i].length; j++) {
                if (XsdForm._countSpecificChildNodes(data, rules.choice[i][j]) > 0) {
                    compatible = false;
                    break;
                }
            }
            if (!compatible) {
                break;
            }
        }
        return compatible;
    }

    /**
     * Permet d'insérer précisement une valeur après une autre
     * Ex: on veux insérer la clef bar entre foo et baz :
     * XsdForm._insertAfter({foo: 1, baz: 1}, 'foo', 'bar', 1)
     *          -> résultat : {foo: 1, bar: 1, baz: 1}
     *
     * @param {object} data
     * @param {string} insertAfterThisKey
     * @param {string} newKey
     * @param {string|object} newValue
     * @returns {object}
     */
    static _insertAfter(data, insertAfterThisKey, newKey, newValue)
    {
        var newData = {};
        for (let key in data) {
            newData[key] = data[key];
            if (key === insertAfterThisKey) {
                newData[newKey] = newValue;
            }
        }
        return newData;
    }

    /**
     * Ajoute une valeur au début d'un objet
     *
     * @param {object} data
     * @param {string} newKey
     * @param {string|object} newValue
     * @returns {object}
     */
    static _prependValue(data, newKey, newValue)
    {
        var newData = {};
        newData[newKey] = newValue;
        for (let key in data) {
            newData[key] = data[key];
        }
        return newData;
    }

    /**
     * Ajoute un element (value) dans le chemin (target) de this.data
     * Se base sur les règles xsd pour la position.
     *
     * @param {string} value
     * @param {string} target
     */
    _addElementToTargetId(value, target)
    {
        var rules = this._getRulesById(target),
            data = this._getDataById(target),
            newData = data,
            dataKeys = Object.keys(data),
            sequence = rules.sequence.length > 0 ? rules.sequence : null,
            index = sequence ? sequence.indexOf(value) : -1,
            insertAfter = dataKeys ? dataKeys[dataKeys.length -1] : '',
            nameToInsert = value.lastIndexOf('/') ? value.substr(value.lastIndexOf('/') +1) : value;
        if (value[0] === '@' && nameToInsert[0] !== '@') {
            nameToInsert = '@'+nameToInsert;
        }
        index--;
        if (typeof data === 'string') {
            data = data ? {'@': data} : {};
            dataKeys = Object.keys(data);
            insertAfter = dataKeys ? dataKeys[dataKeys.length -1] : '';
        }
        if (typeof data[nameToInsert] !== 'undefined') {
            if (!Array.isArray(data[nameToInsert])) {
                data[nameToInsert] = [data[nameToInsert], ''];
            } else {
                data[nameToInsert].push('');
            }
        } else if (index === -1) {
            data = XsdForm._prependValue(data, nameToInsert, "");
        } else {
            while (index > -1) {
                if (dataKeys.indexOf(sequence[index]) !== -1) {
                    insertAfter = sequence[index];
                    break;
                }
                index--;
            }
            if (insertAfter) {
                data = XsdForm._insertAfter(data, insertAfter, nameToInsert, '');
            } else {
                data[value] = '';
            }
        }
        this._insertDataById(data, target);
    }

    /**
     * Remplace les données dans le chemin id par data
     * @param {object|string} data
     * @param {string} id
     */
    _insertDataById(data, id)
    {
        var that = this;
        eval("that.data['"+id.replace(/_/g, "']['")+"'] = data");
    }

    /**
     * Génère un fil d'ariane en fonction de id
     * @param {string} id
     * @returns {jQuery}
     */
    _appendAriane(id)
    {
        var path = id.replace(/([a-z0-9]+)_([\d]+)/gi, '$1:i:$2').split('_'),
            units = [];
        for (let i = 0; i < path.length -1; i++) {
            let target = [];
            for (let j = 0; j <= i; j++) {
                target.push(path[j]);
            }
            let label = path[i].replace(/:at:/g, '@').replace(/([\w]+):i:([\d]+)/g, '$1_$2'),
                match = label.match(/_[\d]+$/g);
            if (match) {
                match[0] = match[0].substr(1);
                label = label.substr(0, label.length - match[0].length -1) + ' ' + (parseInt(match[0], 10) +1);
            }

            units.push(
                {
                    href: "javascript:XsdForm.openCallback('"+target.join('_').replace(/([\w]+):i:([\d]+)/g, '$1_$2')+"', "+this.instanceIndex+")",
                    role: "button",
                    label: label
                }
            );
        }
        let label = path[path.length -1].replace(/:at:/g, '@').replace(/([\w]+):i:([\d]+)/g, '$1_$2'),
            match = label.match(/_[\d]+$/g);
        if (match) {
            match[0] = match[0].substr(1);
            label = label.substr(0, label.length - match[0].length -1) + ' ' + (parseInt(match[0], 10) +1);
        }
        units.push({label: label});
        return Form.ariane(units);
    }

    /**
     * Permet d'obtenir la valeur d'un element pour affichage dans le tableau
     * @param {string|object} data
     * @returns {string}
     */
    static getValue(data)
    {
        if (typeof data === 'string') {
            return data;
        }
        if (data['@']) {
            return data['@'];
        }
    }

    /**
     * Permet d'obtenir la valeur d'un element pour affichage dans le tableau
     * @param {string|object} data
     * @returns {string|void}
     */
    static getElements(data)
    {
        var ul = $('<ul></ul>');
        if (typeof data === 'string') {
            return;
        }
        for (let key in data) {
            if (key.indexOf('@') !== 0) {
                ul.append('<li>'+key+'</li>');
            }
        }
        return ul;
    }

    /**
     * Permet d'obtenir la valeur d'un element pour affichage dans le tableau
     * @param {string|object} data
     * @returns {string}
     */
    static getArgs(data)
    {
        var ul = $('<ul></ul>');
        if (typeof data === 'string') {
            return;
        }
        for (let key in data) {
            if (key.indexOf('@') === 0 && key !== '@') {
                ul.append('<li>'+key.substr(1)+' : '+data[key]+'</li>');
            }
        }
        return ul;
    }
}
XsdForm.texts = {
    value: 'Valeur',
    attribute: 'Attribut',
    element: 'Element',
    elements: 'Elements',
    args: 'Arguments',
    add: 'Ajouter',
    viewTitle: 'Voir documentation',
    doc: 'Documentation',
    openTitle: 'Ouvrir',
    removeTitle: 'Retirer valeur',
    removeConfirm: "Cette action va retirer les valeurs contenu dans l'element, Voulez-vous continuer ?",
    toggleAttributesTitle: "Afficher/Cacher les attributs",
    close: 'Fermer'
};
